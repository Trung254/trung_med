//
//  PregDaily.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/8/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class PregDaily: Object {
    @objc dynamic var id = 0
    
    /*
        start auto generated by AutoFixModel Tool @ Lantd
    */

	var daily_type_id: Int? {

		return LinkingObjects(fromType: PregDailyType.self, property: "pregDaily").first?.id

	}

//	let daily_type_id = LinkingObjects(fromType: PregDailyType.self, property: "id")

    /*
        end auto generated by AutoFixModel Tool @ Lantd
    */

    @objc dynamic var title = ""
    @objc dynamic var hingline_image = ""
    @objc dynamic var short_description = ""
    @objc dynamic var daily_description = ""
    @objc dynamic var daily_relation = ""
    @objc dynamic var like = 0
    @objc dynamic var todo_id = 0


    /*
        start auto generated by AutoFixModel Tool @ Lantd
    */

	var pregDailyLike = List<PregDailyLike>()

    /*
        end auto generated by AutoFixModel Tool @ Lantd
    */

	
}
