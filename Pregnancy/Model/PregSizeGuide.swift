//
//  PregSizeGuide.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/8/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class PregSizeGuide: Object {
    @objc dynamic var id = 0
    @objc dynamic var image = ""
    @objc dynamic var title = ""
    @objc dynamic var length = ""
    @objc dynamic var weight = ""
    @objc dynamic var size_guide_description = ""
    @objc dynamic var week_id = 0
}
