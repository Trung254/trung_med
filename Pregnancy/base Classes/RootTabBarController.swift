//
//  RootTabBarController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class RootTabBarController: UITabBarController {

    private var mIsSelectMenuItem: Bool = false
    private var mNewMenuIndex: Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let activeImage : [Int:String] = [0:"Home_active",1:"favorites_active",3:"user_active"]
        let images = ["tabbar_today","tabbar_video","tabbar_baby","tabbar_me","tabbar_more"]
        let titles = ["Trang chủ","Video", "Em bé", "Mẹ", "Xem thêm"]
        self.tabBar.tintColor = UIColor(red: 0/255.0, green: 150/255.0, blue: 255/255.0, alpha: 1.0)
        if let tabbarItems = self.tabBar.items {
            for i in 0..<tabbarItems.count {
                let tabbar = tabbarItems[i]
                let imageName:String = images[i]
                let activeImageName:String = "\(imageName)_active"
                tabbar.image = UIImage(named: imageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
                tabbar.selectedImage = UIImage(named: activeImageName)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
                
                tabbar.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -0, right: 0)
                tabbar.title = titles[i]
                tabbar.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
                
            }
        }
        ViewManager.setCurrentViewController(self.selectedViewController as? BaseViewController)
    }
    
    func selectTab(_ index: Int, menuIndex:Int) -> Void {
        mIsSelectMenuItem = true
        self.mNewMenuIndex = menuIndex
        self.selectedIndex = index
        self.navigationController?.popToRootViewController(animated: true)
//        self.selectedViewController?.navigationController?.popToRootViewController(animated: true)
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showViewController(viewController vc: BaseViewController, at desTabBar: Int, backTo backTabbar: Int?) -> Void {
        if let vcs = self.viewControllers, vcs.count > desTabBar {
            if let navC = vcs[desTabBar] as? RootNavigationController {
                var desVcs = navC.viewControllers
                desVcs.append(vc)
                navC.setViewControllers(desVcs, animated: false)
                self.selectedIndex = desTabBar
                if let backDes = backTabbar {
                    vc.backToTab = backDes
                }
            }
            
            
            
        }
    }

}

extension RootTabBarController: UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let tabBar = self.tabBar.items?[self.selectedIndex]  {
            if let vc = (self.selectedViewController as! UINavigationController).visibleViewController {
                if let index = vc.navigationController?.viewControllers.firstIndex(of: vc) {
                    if (index > 0) {
                        let vcs = vc.navigationController!.viewControllers
                        vc.navigationController?.setViewControllers([vcs.first!], animated: false)
                        
//                        vc.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
            
            
            
        }
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
    }
}
