//
//  ViewManager.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 1/21/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class ViewManager {

    var rootViewcontroller: RootViewController!
    var currentViewController: BaseViewController?
    var rootTabbarController: RootTabBarController?
    var welcomeViewController: WelcomeViewController?
    var currentMenuView: BaseViewController?
    var mIsShowingMenu: Bool?
    var countRepeat: Int = 0
    
    class var shareInstance : ViewManager {
        struct Static {
            static let instance : ViewManager = ViewManager()
        }
        return Static.instance
    }
    
    class func getRootViewController() -> RootViewController {
        return self.shareInstance.rootViewcontroller
    }
    
    class func setRootViewController(_ rootViewController: RootViewController) -> Void {
        self.shareInstance.rootViewcontroller = rootViewController
    }
    
    class func getCurrentViewController() -> BaseViewController? {
        return self.shareInstance.currentViewController
    }
    
    class func setCurrentViewController(_ currentViewController: BaseViewController?) -> Void {
        self.shareInstance.currentViewController = currentViewController
    }
    
    class func getRootTabbarController() -> RootTabBarController? {
        return self.shareInstance.rootTabbarController
    }
    
    class func setRootTabbarController(_ rootTabbarController: RootTabBarController?) -> Void {
        self.shareInstance.rootTabbarController = rootTabbarController
    }
    
    class func getWelcomeViewController() -> WelcomeViewController? {
        return self.shareInstance.welcomeViewController
    }
    
    class func setWelcomeViewController(_ welcomeViewController: WelcomeViewController?) -> Void {
        self.shareInstance.welcomeViewController = welcomeViewController
    }
    
    class func instantiateWelcomeViewController() -> WelcomeViewController {
        let menuStoryboard: UIStoryboard = UIStoryboard.init(name: "menu", bundle: nil)
        let welcomeView: WelcomeViewController = menuStoryboard.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
        return welcomeView
    }
    
    class func logout() -> Void {
        if let tabbar = self.getRootTabbarController() {
            
            let rootVC = self.getRootViewController()
            if let navController = rootVC.navigationController {
                var vcs = navController.viewControllers
                if let welcome = self.getWelcomeViewController() {
                    if (vcs.contains(welcome)) {
                        tabbar.navigationController?.popViewController(animated: true)
                        self.setRootTabbarController(nil)
                        return
                    }
                }
                
                let welcome = self.instantiateWelcomeViewController()
                vcs = vcs.filter({ (vc) -> Bool in
                    return vc.isEqual(rootVC)
                })
                vcs.append(welcome)
                self.setWelcomeViewController(welcome)
                navController.setViewControllers(vcs, animated: true)
            }
            
        }
    }
    
    class func showTabbarController () -> Void {
        if let tabbar = ViewManager.getRootTabbarController() {
            if let currentView = ViewManager.getCurrentViewController() {
                if let navigationController = currentView.navigationController {
                    navigationController.pushViewController(tabbar, animated: true)
                    
                    Timer.scheduledTimer(timeInterval: 0.1, target: ViewManager.self, selector: #selector(checkTabbar(_:)), userInfo:nil, repeats: false)
                }
            }
        }
    }
    
    class func showWellcomeViewController() -> Void {
        if let currentView = ViewManager.getCurrentViewController(), let navigationController = currentView.navigationController {
            navigationController.popToRootViewController(animated: false)
            
        }
        if let tabBar = ViewManager.getRootTabbarController() {
            tabBar.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    @objc class func checkTabbar(_ count: Any) {
        if(ViewManager.shareInstance.countRepeat > 10) {
            ViewManager.shareInstance.countRepeat = 0
            return
        }
        ViewManager.shareInstance.countRepeat = ViewManager.shareInstance.countRepeat + 1
        DispatchQueue.global(qos: .background).async {
            let currentView = ViewManager.getRootViewController()
            if let tabbar = ViewManager.getRootTabbarController() {
                if let navigationController = currentView.navigationController {
                    var viewControllers = navigationController.viewControllers
                    if viewControllers.lastIndex(of: tabbar) != nil {
                        viewControllers = viewControllers.filter({ (vc) -> Bool in
                            if(vc.isEqual(ViewManager.getRootViewController()) ||
                                vc.isEqual(ViewManager.getRootTabbarController()) ||
                                vc.isEqual(ViewManager.getWelcomeViewController())
                                ) {
                                return true
                            }
                            return false
                            
                        })
                        DispatchQueue.main.async {
                            navigationController.viewControllers = viewControllers
                        }
                    }
                    
                }
            } else {
                Timer.scheduledTimer(timeInterval: 0.1, target: ViewManager.self, selector: #selector(checkTabbar(_:)), userInfo: nil, repeats: false)
            }
        }
    }
}
