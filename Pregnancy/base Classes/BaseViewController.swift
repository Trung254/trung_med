//
//  BaseViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/7/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import AVFoundation
import MBProgressHUD
import SwiftyJSON

let screenSizeWidth = UIScreen.main.bounds.width
let screenSizeHeight = UIScreen.main.bounds.height
struct ScreenSize
{
    static let SCREEN_WIDTH = screenSizeWidth
    static let SCREEN_HEIGHT = screenSizeHeight
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
let IS_IPHONE_8 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
let IS_IPHONE_XS = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

public class Constant {
    public static let totalItem: CGFloat = 6
    public static let column: CGFloat = 3
    public static let minLineSpacing: CGFloat = 5.0
    public static let minItemSpacing: CGFloat = 5.0
    public static let offset: CGFloat = 5.0 // TODO: for each side, define its offset
    public static let MAX_ROW: Int = 10000000
}

class BaseViewController: UIViewController {
    
    var mIsShowBackButton: Bool = true
    var mIsShowRightButton: Bool = true
    var hud : MBProgressHUD?
    var hudShowCount : Int = 0
    var gestureRecognizer = UIGestureRecognizer()
    var weeks: Int?
    var days: Int?
    var backToTab: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        gestureRecognizer.delegate = self
        self.view.addGestureRecognizer(gestureRecognizer)
        setupNotifications()
        self.setColorNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        ViewManager.setCurrentViewController(self)
        super.viewWillAppear(animated)
        if(self.mIsShowBackButton) {
            self.setBackButton()
        }
        
        if(self.mIsShowRightButton) {
            self.setupRightBarButton()
        }

        self.handleTabBar(animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc func showHud() {
        
        if Thread.isMainThread != true {
            self.perform(#selector(showHud), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        
        if(self.hudShowCount == 0) {
            hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud?.mode = .indeterminate
        }
        
        self.hudShowCount = self.hudShowCount + 1
//        let checkHub = self.hudShowCount
        
//        if !Connectivity.isConnectedToInternet() {
//            self.alert("Vui lòng kiểm tra kết nối mạng")
//            self.hideHud()
//        } else {
//              self.hideHud()
////            DispatchQueue.main.asyncAfter(deadline: .now() + 15.0) {
//////                if checkHub == self.hudShowCount {
//////
//////                    self.alert("Vui lòng kiểm tra kết nối mạng")
//////                    self.hideHud()
//////                }
////
////
////            }
//        }
    }
    
    @objc func hideHud() {
        if Thread.isMainThread != true {
            self.perform(#selector(hideHud), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        if(self.hudShowCount > 0) {
            self.hudShowCount = self.hudShowCount - 1
            if (self.hudShowCount == 0) {
                hud?.hide(animated: false)
            }
        }
    }
    /////
    
    func alert(_ message: String, title : String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let action = UIAlertAction(title: "OK", style: .default, handler:handler)
        self.prompt(title ?? "", message: message, actions: [action])
    }
    
    func prompt(_ title: String, message: String, okHandler: ((UIAlertAction) -> Void)? = nil, cancelHandler: ((UIAlertAction) -> Void)? = nil) -> Void {
        prompt(title, message: message, okTitle: "Ok", okHandler: okHandler, cancelTitle: "Cancel", cancelHandler: cancelHandler)
        
    }
    
    func prompt(_ title: String, message: String, okTitle: String? = "Ok" , okHandler: ((UIAlertAction) -> Void)? = nil, cancelTitle: String? = "Cancel", cancelHandler: ((UIAlertAction) -> Void)? = nil) -> Void {
        let okAction: UIAlertAction = UIAlertAction.init(title: okTitle, style: .default, handler: okHandler)
        
        let cancelAction: UIAlertAction = UIAlertAction.init(title: cancelTitle, style: .default, handler: cancelHandler)
        
        let actions: Array<UIAlertAction> = [okAction, cancelAction ]
        self.prompt(title, message: message, actions:actions)
    }
    
    func prompt(_ title: String, message: String, actions:Array<UIAlertAction>, preferredStyle: UIAlertController.Style = .alert ) -> Void {
        if(Thread.isMainThread != true) {
            DispatchQueue.main.async {
                self.prompt(title, message: message, actions: actions, preferredStyle: preferredStyle)
            }
            return
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showMainMenu() -> Void {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let menu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.definesPresentationContext = true
        menu.modalPresentationStyle = .overFullScreen
        menu.currVC = ViewManager.getCurrentViewController()
        menu.viewController = self
        
        
        self.presentMenu(menu)
        
    }
    
    @objc func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        if(self.extendedLayoutIncludesOpaqueBars ||
//            self.edgesForExtendedLayout.contains(UIRectEdge.bottom)) {
//            if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
//                    self.changeTabBar(hidden: true, animated: true)
//
//            }
//            else{
//                    self.changeTabBar(hidden: false, animated: true)
//            }
//        }
    }
    
    @objc func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    @objc func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if(self.extendedLayoutIncludesOpaqueBars ||
            self.edgesForExtendedLayout.contains(UIRectEdge.bottom)) {
            if isUpdateTabBarWhenScrolling() == true {
                if scrollView.panGestureRecognizer.translation(in: scrollView).y < 0{
                    self.hideTabBar(hidden: true, animated: true)
                }
                else {
                    self.hideTabBar(hidden: false, animated: true)
                }
            }
        }
    }
    
    func tabBar(_ tabbar: UITabBar, rect:CGRect, hidden: Bool) {
        
    }
    
   
    
    func hideTabBar(hidden:Bool, animated: Bool){
        guard let tabBar = self.tabBarController?.tabBar else { return; }
        if tabBar.isHidden == hidden{ return }
        let frame = tabBar.frame
        let windowFrame = UIScreen.main.bounds
        let offset = hidden ? 0 : -frame.size.height
        let duration:TimeInterval = (animated ? 0.3 : 0.0)
        tabBar.isHidden = false
        
        UIView.animate(withDuration: duration, animations: {
            tabBar.frame.origin = CGPoint(x: 0, y: windowFrame.size.height + offset) // frame.offsetBy(dx: 0, dy: offset)
        }, completion: { (true) in
            tabBar.isHidden = hidden
            self.tabBar(tabBar, rect: tabBar.frame, hidden: hidden)
        })
    }
    
    func handleTabBar(animated: Bool) {
        let hidden = self.isHiddenTabBar()
        self.hideTabBar(hidden: hidden, animated: animated)
    }
    
    func setLeftMainMenu() {
        self.mIsShowBackButton = false
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "menu-button-of-three-lines"), for: .normal)
        button.addTarget(self, action: #selector(handleLeftMenu), for: .touchUpInside)
        button.frame = CGRect(x: -2, y: -2, width: 34, height: 31.3)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    func setBackButton() {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "LeftArrow"), for: .normal)
        button.addTarget(self, action: #selector(doDefaultBack), for: .touchUpInside)
        button.frame = CGRect(x: -2, y: 0, width: 30, height: 27.7)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
//    func setRightButtonInfo() {
//        let infoButton = UIButton(type: .custom)
//        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
//        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
//        infoButton.addTarget(self, action: #selector(doDefaultInfo), for: .touchUpInside)
//        let barButton = UIBarButtonItem(customView: infoButton)
//        self.navigationItem.rightBarButtonItem = nil
//        self.navigationItem.rightBarButtonItems = nil
//        self.navigationItem.rightBarButtonItem = barButton
//    }
//
//    func getNavigationRightButton() -> Array<UIBarButtonItem> {
//        return [/*info,*//*contractionlog*/]
//    }
//
//    func setupRightBarButton(_ barItems:[UIBarButtonItem]) {
//        self.navigationItem.rightBarButtonItem = nil
//        self.navigationItem.rightBarButtonItems = nil
//        self.navigationItem.rightBarButtonItems = self.getNavigationRightButton()
//    }

    private func setupRightBarButton() {
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = self.createRightBarButtonItems()
    }
    
    //MARK: this method maybe override by child classes
    func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        infoButton.addTarget(self, action: #selector(doDefaultInfo), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: infoButton)
        return [barButton]
    }
    
    //MARK: this method should be override by child classes if the class want to hide tabBar
    func isHiddenTabBar() -> Bool {
        return false
    }

    func isUpdateTabBarWhenScrolling() -> Bool {
        return false
    }
    
    func addPadLockButton() {
        let button1 = UIButton(type: .custom)
//        button1.setImage(UIImage(named: "padlock_white"), for: .normal)
//        button1.addTarget(self, action: #selector(handlePadLockButton), for: .touchUpInside)
        button1.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button1)
        
        self.navigationItem.rightBarButtonItems = [barButton]
    }
    
    
    
    @objc func handleLeftMenu(){
        fatalError("Must Override to handle left menu")
    }
    
    @objc func handlePadLockButton() {
        let infoStoryBoard = UIStoryboard.init(name: "PremiumUpgrade", bundle: nil)
        let info = infoStoryBoard.instantiateViewController(withIdentifier: "PremiumUpgradeViewController") as! PremiumUpgradeViewController
        self.navigationController?.pushViewController(info, animated: true)
    }
    
    @objc func doDefaultBack(_ sender : Any?) -> Void {
        
        if let backDes = self.backToTab {
            self.tabBarController?.selectedIndex = backDes
            self.backToTab = nil
            // Thêm phần pop về root view để khi có bấm vào tab thì trở về root
            self.navigationController?.popToRootViewController(animated: false)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func doDefaultInfo(_ sender : Any?) -> Void {
        let infoStoryBoard = UIStoryboard.init(name: "Info", bundle: nil)
        let info = infoStoryBoard.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        self.navigationController?.show(info, sender: nil)
//        let viewcontroller = UIViewController()
//        viewcontroller.view.backgroundColor = UIColor.red
//        self.navigationController?.show(viewcontroller, sender: nil)
    }
    
    func setTitle(title: String){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.8, height: 44))
        lbl.text = title
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(20.0)
        lbl.sizeToFit()
        self.navigationItem.titleView = lbl
    }
    
//    func setThumbImageUISlider(slider: UISlider, text: String, color: UIColor) {
//
//        slider.thumbTintColor = color
//
//
//        let thumbView = UIView(frame: CGRect(x: 0, y: 0, width: 31, height: 31))
//        thumbView.backgroundColor = color
//        thumbView.layer.cornerRadius = thumbView.layer.frame.width / 2
//        thumbView.clipsToBounds = true
//
//        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 31, height: 31))
//        lbl.textColor = .white
//        lbl.textAlignment = .center
//        lbl.font = UIFont.systemFont(ofSize: 12)
//        lbl.text = text
//
//        thumbView.addSubview(lbl)
//
////        imgView.contentMode = .center
////        imgView.image = UIImage.init(view: thumbView)
//        slider.setThumbImage(UIImage.init(view: thumbView), for: .normal)
//        slider.setThumbImage(UIImage.init(view: thumbView), for: .highlighted)
////        thumbView.layer.bringToFront()
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func convertStringToDate(_ dataDate: String) -> Date{
//        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        if let date = dateFormatter.date(from: dataDate) {
            return date
        }
        
        return Date()
    }
    
    func transferStringToDate(_ dataDate: String, _ typeFormat: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = typeFormat
        if let date = dateFormatter.date(from: dataDate) {
            return date
        }
        
        return Date()
    }
    
    func caculatorEndDate(_ dateString : String) -> Date{
        let date = convertStringToDate(dateString)
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let periodMonth = calendar!.component(.month, from: date)
        var timefinnal: Date!
        if (4 <= periodMonth) && (periodMonth <= 12) {
            let time1 = (calendar?.date(byAdding: .day, value: 7, to: date, options: .init(rawValue: 0)))!
            let time2 = (calendar?.date(byAdding: .month, value: -3, to: time1, options: .init(rawValue: 0)))!
            timefinnal = (calendar?.date(byAdding: .year, value: 1, to: time2, options: .init(rawValue: 0)))!
            
        } else {
            let time1 = (calendar?.date(byAdding: .day, value: 7, to: date, options: .init(rawValue: 0)))!
            timefinnal = (calendar?.date(byAdding: .month, value: 9, to: time1, options: .init(rawValue: 0)))!
        }
        
        return timefinnal
    }
    
    func caculatorStartDate(_ dateString : String) -> Date{
        let date = convertStringToDate(dateString)
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
//        let periodMonth = calendar!.component(.month, from: date)
        var timefinnal: Date!
         timefinnal = (calendar?.date(byAdding: .day, value: -280, to: date, options: .init(rawValue: 0)))!
//        if (1 <= periodMonth) && (periodMonth <= 9) {
//            let time1 = (calendar?.date(byAdding: .day, value: -7, to: date, options: .init(rawValue: 0)))!
//            let time2 = (calendar?.date(byAdding: .month, value: 3, to: time1, options: .init(rawValue: 0)))!
//            timefinnal = (calendar?.date(byAdding: .year, value: -1, to: time2, options: .init(rawValue: 0)))!
//
//        } else {
//            let time1 = (calendar?.date(byAdding: .day, value: -7, to: date, options: .init(rawValue: 0)))!
//            timefinnal = (calendar?.date(byAdding: .month, value: -9, to: time1, options: .init(rawValue: 0)))!
//        }
        
        return timefinnal
    }

    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onDidUpdateUserInformations(_:)), name: .didUpdatedCurrentUserInformations, object: nil)
    }
    
    @objc func onDidUpdateUserInformations(_ notification: Notification) {
        //do nothing
        //this method will be override by sub class
    }
    
    func setColorNavigation() {
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func subStringAppointment(_ str: String) -> String{
        let startIndex = str.index(str.startIndex, offsetBy: 3)
        
        if let endIndex = str.range(of: "</p>")?.lowerBound {
            return (String(str[startIndex..<endIndex]))
        }
        else {
            return str
        }
    }
    
    func addBoxShadow(uiview: UIView) {
        uiview.layer.cornerRadius = 3
        
        // border
        uiview.layer.borderWidth = 0.3
        uiview.layer.borderColor = UIColor.lightGray.cgColor
        
        // shadow
        uiview.layer.shadowColor = UIColor.lightGray.cgColor
        uiview.layer.shadowOffset = CGSize(width: 1, height: 1)
        uiview.layer.shadowOpacity = 0.5
        uiview.layer.shadowRadius = 1.0
    }
}

extension UIView {
    
    var width: CGFloat {
        get{return size.width}
        set(width){size = CGSize(width: width, height: height)}
    }
    
    
    
    var height: CGFloat {
        get{return size.height}
        set(height){size = CGSize(width: width, height: height)}
    }
    
    
    
    var size: CGSize {
        
        get {
            return self.frame.size
            
        }
        
        set(size) {
            frame = CGRect.init(origin: origin, size: size)
        }
        
    }
    
    var x: CGFloat {
        
        get {
            return origin.x
        }
        
        set(x) {
            origin = CGPoint(x: x, y: y)
        }
        
    }
    
    
    
    var y: CGFloat {
        
        get {
            return origin.y
        }
        
        set(y) {
            origin = CGPoint(x: x, y: y)
        }
        
    }
    
    var origin: CGPoint {
        
        get {
            return frame.origin
        }
        
        set(origin) {
            frame = CGRect.init(origin: origin, size: size)
        }
    }
    
    
}

extension String {
    
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
            ], documentAttributes: nil).string
        
        return decoded ?? self
    }

    
    var isNumber: Bool {
        let characters = CharacterSet.decimalDigits.inverted
        return !self.isEmpty && rangeOfCharacter(from: characters) == nil
    }
    
    var length: Int {
        return count
    }
    
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}

extension UIDevice {
    
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}

extension UIViewController {
    
    func presentMenu(_ viewControllerToPresent: UIViewController) {
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = CATransitionType.moveIn
//        transition.subtype = CATransitionSubtype.fromLeft
//        self.view.layer.add(transition, forKey: kCATransition)
        
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissMenu(completion: (() -> Void)? = nil) {
//        let transition = CATransition()
//        transition.duration = 0.25
//        transition.type = CATransitionType.moveIn
//        transition.subtype = CATransitionSubtype.fromRight
//        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: completion)
    }
    
    func dismissPopUpEmail(completion: (() -> Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: completion)
    }
    
    func addBackgroundImage (_ imageName: String) {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: imageName)?.draw(in: self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.view.backgroundColor = UIColor(patternImage: image)
    }
}

// set background image
extension UIView {
    func addBackground(imageName: String) {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: height)))
        imageViewBackground.image = UIImage(named: imageName)
        
        // set content mode
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
        imageViewBackground.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview()
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
        }
    }
}


extension UIImage {
    // Convert UiView to Image
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}

extension String {
    
    func convertVietnamPhoneNumer(_ convertCountryCode:Bool = false) -> String? {
        
        if(self.contains(" ")) {
            let spaceremoved = self.replacingOccurrences(of: " ", with: "")
            return spaceremoved.convertVietnamPhoneNumer(convertCountryCode)
        }
        
        if(!self.isNumber ) {
            return nil
        }
        
        if (self.length > 11 || self.length < 10) {
            return nil
        }
        
        let codes = [
            "016":"03",
            "0120":"070",
            "0121":"079",
            "0122":"077",
            "0126":"076",
            "0128":"078",
            "0123":"083",
            "0124":"084",
            "0125":"085",
            "0127":"081",
            "0129":"082",
            "0186":"056",
            "0188":"058"
            ] as Dictionary
        
        if(self.length == 11) {
            for (oldCode, newCode) in codes {
                if(self.hasPrefix(oldCode)) {
                    return self.replacingOccurrences(of: oldCode, with: newCode).convertVietnamPhoneNumer(convertCountryCode)
                }
            }
            return nil
        }
        
        if(self.length == 10) {
            let prefix = [
                "032",
                "033",
                "034",
                "035",
                "036",
                "037",
                "038",
                "039",
                "056",
                "058",
                "059",
                "070",
                "076",
                "077",
                "078",
                "079",
                "081",
                "082",
                "083",
                "084",
                "085",
                "086",
                "088",
                "089",
                "090",
                "091",
                "092",
                "093",
                "094",
                "096",
                "097",
                "098",
                "099"
            ]
            
            if (prefix.contains(String(self.prefix(3)))) {
                if(convertCountryCode ) {
                    return self.replacingCharacters(in: ...self.startIndex, with: "+84")
//                    return self.replacingOccurrences(of: "0", with: "84")
                }
                return self
            }
        }
        return nil
    }
}



extension BaseViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = (touch.view == gestureRecognizer.view)
        if(should) {
            self.view.endEditing(true)
        }
        return should
    }
}

extension Notification.Name {
    static let didUpdatedCurrentUserInformations = Notification.Name("didUpdatedCurrentUserInformations")
}

// Delay cho DispatchQueue tinh bang milisecond


extension Date {
    func getDayMonthYearHourMinuteSecond() -> (day: Int, month: Int, year: Int, hour: Int, minute: Int, second: Int) {
        let calendar = Calendar.current
        let day = calendar.component(.day, from: self)
        let month = calendar.component(.month, from: self)
        let year = calendar.component(.year, from: self)
        let hour = calendar.component(.hour, from: self)
        let minute = calendar.component(.minute, from: self)
        let second = calendar.component(.second, from: self)
        return (day, month, year, hour, minute, second)
    }
}

//extension UIFont {
//    
//    public class func fontAwesome(ofSize fontSize: CGFloat) -> UIFont {
//        return UIFont.fontAwesome(ofSize: fontSize, style: .solid)
//        
//    }
//    
//}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
