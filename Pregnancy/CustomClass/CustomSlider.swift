//
//  Custom.swift
//  uislider test
//
//  Created by Nguyễn Thanh on 3/21/19.
//  Copyright © 2019 TunBeo. All rights reserved.
//

import UIKit
import FontAwesomeKit

class CustomSlider : UISlider {
    
    // Tăng kích thước của phần track UiSlider
    @IBInspectable open var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }
    
    override open func trackRect(forBounds bounds: CGRect) -> CGRect {
        let defaultBounds = super.trackRect(forBounds: bounds)
        return CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
    }
    
    // Thêm label vào thumbimage UISlider
    var thumbTextLabel: UILabel = UILabel()
    
    private var thumbFrame: CGRect {
        return thumbRect(forBounds: bounds, trackRect: trackRect(forBounds: bounds), value: value)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbTextLabel.frame = thumbFrame
        thumbTextLabel.font = UIFont.boldSystemFont(ofSize: 11)
        thumbTextLabel.textColor = .white
        thumbTextLabel.text = String(Int(value))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(thumbTextLabel)
        thumbTextLabel.textAlignment = .center
        thumbTextLabel.layer.zPosition = layer.zPosition + 1
    }
    
    // Giới hạn lại 2 phía của Uislider
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        var bounds: CGRect = self.bounds
//        bounds = bounds.insetBy(dx: -10, dy: -15)
//        return bounds.contains(point)
//    }
    
}
