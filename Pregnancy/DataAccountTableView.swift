//
//  DataAccountTableView.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import Foundation

class dataAccount {
    public var imgAccount: String
    public var lblAccount: String
    
    init(hinh:String, lb:String) {
        self.imgAccount = hinh
        self.lblAccount = lb
    }
}
