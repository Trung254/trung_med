//
//  NewPasswordViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/21/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class NewPasswordViewController: BaseViewController {

    var mPhoneNumber: String?
    @IBOutlet weak var mRegisterBtn: UIButton!
    @IBOutlet weak var mPasswordTF: UITextField!
    var isShowPass:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Nhập mật khẩu"
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        //hiden navigationbarhidden
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        self.mIsShowBackButton = false
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = nil
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    @IBAction func didSelectContinue(_ sender: UIButton) {
        sender.isEnabled = false
        if(dataUser.isSignup) {
            if let rawPassword = self.mPasswordTF.text {
                let count = rawPassword.count
                let password = rawPassword.trimmingCharacters(in: .whitespacesAndNewlines)
                let trimcount = password.count
                if (count != trimcount && trimcount == 0) {
                    self.alert("Không được phép nhập ký tự trống vào trường")
                    sender.isEnabled = true
                    return
                }
                if(trimcount < 6) {
                    self.alert("Mật khẩu phải dài hơn 6 ký tự")
                    sender.isEnabled = true
                    return
                }
                self.showHud()
                DispatchQueue.global(qos: .background).async {
                    NetworkManager.shareInstance.apiPostUser(password: password, phone: dataUser.phone, socialTypeId: dataUser.socialTypeId, firstName: dataUser.firstName, lastName: nil, youAreThe: dataUser.youAreThe, location: dataUser.location, status: nil, avarta: nil, email: nil) { (data, message, isSuccess) in
                        
                        if(isSuccess) {
                            DatabaseManager.sharedManager.login(in: self, user: dataUser.phone, password: password, token: nil, provider: nil
                                , callBack: { (user, isTempData, isGetInfoSuccess, isLoginSuccess) in
                                    if (isLoginSuccess) {
                                        NetworkManager.shareInstance.apiPostPregnacys(babyGender: dataUser.dataGender, dueDate: dataUser.dueDate, startDate: dataUser.startDate, showWeek: 1) {
                                            (data, message, isSuccess) in
                                            DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTempData, isSuccess) in
                                                if(!isTempData && isSuccess) {
                                                    DatabaseManager.sharedManager.getPregnancys(callBack: { (pregnancy, isTemp, isSuccess, rawData) in
                                                        NetworkManager.shareInstance.apiPostAppSetting(reminders: true, length_units: dataUser.lengthUnit, weight_unit: dataUser.weightUnit) {
                                                            (data, message, isSuccess) in
                                                            DispatchQueue.main.async {
                                                                self.hideHud()
                                                                self.showMainView()
                                                            }
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    }
                            })
                            NetworkManager.shareInstance.apiToken(username: dataUser.phone, password: password, provider: nil, accessToken: nil, callBack: { (isSuccess) in
                                
                                
                                //                            NetworkManager.shareInstance.apiPostAppSetting(reminders: true, length_units: dataUser.lengthUnit, weight_unit: dataUser.weightUnit) {
                                //                                (data, message, isSuccess) in
                                //
                                //                            }
                            })
                        } else {
                            if let data = data as? JSON {
                                let mess = data["Message"].stringValue
                                if (mess.isEmpty != false ) {
                                    self.alert(mess)
                                } else {
                                    self.alert("Vui lòng thử lại sau")
                                }
                                
                            } else {
                                self.alert("Vui lòng thử lại sau")
                            }
                            self.mRegisterBtn.isEnabled = true
                        }
                    }
                }
            } else {
                self.alert("Vui lòng nhập mật khẩu")
                self.mRegisterBtn.isEnabled = true
            }
        } else {
            if let rawPassword = self.mPasswordTF.text {
                let count = rawPassword.count
                let password = rawPassword.trimmingCharacters(in: .whitespacesAndNewlines)
                let trimcount = password.count
                if (count != trimcount && trimcount == 0) {
                    self.alert("Không được phép nhập ký tự trống vào trường")
                    sender.isEnabled = true
                    return
                }
                if(trimcount < 6) {
                    self.alert("Mật khẩu phải dài hơn 6 ký tự")
                    self.mRegisterBtn.isEnabled = true
                    return
                }
                self.showHud()
//                let phone = AuthManager.shared.currentUser?.phone
                let phone = dataUser.phone
                DispatchQueue.global(qos: .background).async {
                    DatabaseManager.sharedManager.forgotPassword(phone: phone, password: password) { (data, isTempdata, isSuccess) in
                        //                        print("chinh sua password")
                        
                        self.prompt("Thông báo", message: "Thay đổi thành công", okHandler: { (alert) in
                            //                            self.login(phone, password: password)
                            DatabaseManager.sharedManager.logout {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }, cancelHandler: nil)
                        self.hideHud()
                    }
                }
            } else {
                self.alert("Vui lòng nhập mật khẩu")
                self.mRegisterBtn.isEnabled = true
                
            }
        }
    }
    
    @IBAction func pressToShowPass(_ sender: Any) {
        if isShowPass == true {
            mPasswordTF.isSecureTextEntry = false
        }
        else {
            mPasswordTF.isSecureTextEntry = true
        }
        isShowPass = !isShowPass
    }
    
    private func login (_ user: String?, password: String?, token: String? = nil, provider: SocialProvider? = nil ) -> Void {
        
        DatabaseManager.sharedManager.login(in: self, user: user, password: password, token: token, provider: provider) { (user, isTempData, isGetInfoSuccess, isLoginSuccess) in
            //login failed
            if(!isLoginSuccess){
                self.hideHud()
                self.alert("Sai số điện thoại hoặc mật khẩu.")
                self.mRegisterBtn.isEnabled = true
                return
            }
            // login succeed but failed to get info
            if(!isGetInfoSuccess) {
                self.hideHud()
                self.alert("Vui lòng thử lại sau!")
                self.mRegisterBtn.isEnabled = true
                return
            }
            
            // check if data on cloud is empty ( name ) then let's user put their info if need
            if(!isTempData) {
                self.hideHud()
                if provider != nil {
                    if let user = user {
                        if user.first_name.isEmpty || user.last_name.isEmpty {
                            
                        }
                    }
                }
                
                // show main view
                self.showMainView()
            }
        }
    }
    
    @objc func showMainView() {
        if(Thread.isMainThread != true) {
            self.perform(#selector(showMainView), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        
        if(ViewManager.getRootTabbarController() == nil) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            let rootVC = ViewManager.getRootViewController()
            rootVC.createRootTabbarControllerIfNeed()
            
            ViewManager.showTabbarController()
        } else {
            self.doDefaultBack(nil)
        }
    }
}
