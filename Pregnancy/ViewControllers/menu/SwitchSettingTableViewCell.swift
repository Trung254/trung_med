//
//  SwitchSettingTableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SwitchSettingTableViewCell: UITableViewCell {
    @IBOutlet weak var mImage: UIImageView!
    @IBOutlet weak var mSwitchLabel: UILabel!
    
    @IBOutlet weak var mWeightButton: UIButton!
    @IBOutlet weak var mView2: UIView!
    @IBOutlet weak var mView1: UIView!
    @IBOutlet weak var mSwitch: UISwitch!
    @IBOutlet weak var mLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
