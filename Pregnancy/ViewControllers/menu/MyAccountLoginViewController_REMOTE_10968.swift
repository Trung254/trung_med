//
//  MyAccountLoginViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit


class MyAccountLoginViewController: BaseViewController {
    
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mBtnYouAreThe: UIButton!
    @IBOutlet weak var mBtnLocation: UIButton!
    @IBOutlet weak var mProfileTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var mLogoutBtn: UIButton!
    @IBOutlet weak var mPickerTopLayout: NSLayoutConstraint!
    @IBOutlet weak var mLblAccountEmail: UILabel!
    
    @IBOutlet weak var mFirstNameTF: UITextField!
    @IBOutlet weak var mLastNameTF: UITextField!
    
    var pickerItemYouAreThe : [String] = ["Mother", "Father", "Single mother", "Grandparent", "Uncle or Aunt", "Partner", "Friend"]
    var pickerItemLocation : [String] = ["Hà Nội", "TP Hồ Chí Minh", "Hải Phòng", "Nam Định", "Hà Nam", "Nghệ An", "Hà Tĩnh"]
    var selectedPicker : String = ""
    var passedTitle = ""
    var authType: AuthType = .SignUp
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mPickerView.isHidden = true
        //mPickerViewLocation.isHidden = true
        showMessageIfLogin()
    }
    
    func showMessageIfLogin() {
        guard DatabaseManager.sharedManager.isGuest() != true else {
            print("failed")
            self.mLblAccountEmail.isHidden = true
            return
        }
        guard DatabaseManager.sharedManager.isLogin() != true else {
            print("failed")
            self.mLblAccountEmail.isHidden = false
            return
            
        }

//        if DatabaseManager.sharedManager.isLogin() == true {
//            self.mLblLastBackUp.isHidden = false
//            self.mLblAccountEmail.isHidden = false
//        } else {
//            if DatabaseManager.sharedManager.isGuest() == true {
//                print(123)
//            self.mLblLastBackUp.isHidden = true
//            self.mLblAccountEmail.isHidden = true
//            }
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.mIsShowRightButton = true
        self.addPadLockButton()
        super.viewWillAppear(animated)
        
        if(DatabaseManager.sharedManager.isGuest()) {
            self.mProfileTopContraint.constant = 150
            self.mLogoutBtn.isHidden = true
        } else {
            self.mProfileTopContraint.constant = 0
            self.mLogoutBtn.isHidden = false
        }
        self.showHud()
        DatabaseManager.sharedManager.getUserInfo(nil, nil) { (user, isTempData, isSuccess) in
            self.setData(user)
            self.hideHud()
        }
        
    }
    
    func setData(_ user: PregUser?) -> Void {
        
        if let user = user {
            self.mFirstNameTF.text = user.first_name
            self.mLastNameTF.text = user.last_name
            self.mLblAccountEmail.text = "Tài khoản: \(user.phone)"
            self.mBtnYouAreThe.setTitle(user.you_are_the, for: .normal)
            self.mBtnLocation.setTitle(user.location, for: .normal)
        }
        
    }

    override func isHiddenTabBar() -> Bool {
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myAccount_to_Login" {
            let viewController = segue.destination as! AuthViewController
            viewController.authType = self.authType
        }
    }
    
    
    @IBAction func mBtnLogIn(_ sender: Any) {
        self.authType = .Login
        performSegue(withIdentifier: "myAccount_to_Login", sender: self)
    }
    
    @IBAction func mBtnSignUp(_ sender: Any) {
        self.authType = .SignUp
        performSegue(withIdentifier: "myAccount_to_Login", sender: self)
    }
    
    @IBAction func mBtnYouAreTheTouchUpInside(_ sender: UIButton) {
        mPickerView.isHidden = false
        mPickerView.tag = 1
        self.mPickerTopLayout.constant = self.mProfileTopContraint.constant + sender.y
        mPickerView.reloadAllComponents()
        
    }
    
    @IBAction func mBtnLocationTouchUpInside(_ sender: UIButton) {
        mPickerView.isHidden = false
        mPickerView.tag = 2
        self.mPickerTopLayout.constant = self.mProfileTopContraint.constant + sender.y 
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnLogOutEveryWhere(_ sender: Any) {
        
        DatabaseManager.sharedManager.logout {
            
        }
    }
    
    @IBAction func mBtnDone(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}

extension MyAccountLoginViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if mPickerView.tag == 1 {
            return pickerItemYouAreThe.count
        }
        else {
            return pickerItemLocation.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if mPickerView.tag == 1 {
            return pickerItemYouAreThe[row]
        }
        else {
            return pickerItemLocation[row]
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if mPickerView.tag == 1 {
                mBtnYouAreThe.setTitle(pickerItemYouAreThe[row], for: .normal)
                mPickerView.isHidden = true
        }
        else if mPickerView.tag == 2 {
            mBtnLocation.setTitle(pickerItemLocation[row], for: .normal)
            mPickerView.isHidden = true
        }

    }
}
