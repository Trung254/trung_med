//
//  DueDateViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/5/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

public struct MenuData {
    public var name : String
    public var image : String
    public var type : String
}

class DueDateViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var mTopPickerView: NSLayoutConstraint!
    @IBOutlet weak var mLblWeekPregnancy: UILabel!
    @IBOutlet weak var mSubLblWeekPregnancy: UILabel!
    @IBOutlet weak var mAvatarView: UIView!
    @IBOutlet weak var mAvatarButton: UIButton!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mBtnBabyGender: UIButton!
    @IBOutlet weak var mBtnDueDate: UIButton!
    @IBOutlet weak var mBtnShowWeek: UIButton!
    @IBOutlet weak var mSwBabyAlreadyBorn: UISwitch!
    @IBOutlet weak var mLbBabyAlreadyBorn: UILabel!
    @IBOutlet weak var mBtnDateOfBirth: UIButton!
    @IBOutlet weak var mViewWeek: UIView!
    
    
    var dataPickerBabyGender : [String] = ["Trai", "Gái", "Sinh đôi", "Chưa xác định"]
    var dataPickerShowWeek : [Int:String] = [1:"Tuần hiện tại", 2:"Hoàn thành"]
    var checkDatePicker :Int?
    var checkBabyGender : String = ""
    var checkBabyGenderInt : Int?
    var checkSW : Int = 0
    var photoUpload: UIImage?
    var mSelectedShowWeek: Int = 1
    var myPregnancy : [PregPregnancy] = [PregPregnancy]()
    let dateFormatter = DateFormatter()
    var dateStringCurrent = ""
    var selectedDate:Date?
    var mShowingPickerDateView: PickerDateView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mIsShowRightButton = false
        self.mIsShowBackButton = false
        self.setLeftMainMenu()
        self.setTitle(title: "Thời gian dự sinh")
        
        mPickerView.delegate = self
        mPickerView.dataSource = self
        mPickerView.isHidden = true
        
        self.dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        
        
        //mAvatarView.layer.cornerRadius = mAvatarView.frame.size.height / 2
        mAvatarView.layer.cornerRadius = 45
        mAvatarView.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
        mAvatarView.layer.borderWidth = 5
        mAvatarButton.layer.cornerRadius = mAvatarButton.frame.width / 2
        mAvatarButton.layer.masksToBounds = true
        
        self.updateAvatar()
        self.view.bringSubviewToFront(mPickerView)
        mSwBabyAlreadyBorn.onTintColor = #colorLiteral(red: 0, green: 0.6632037759, blue: 1, alpha: 0.3878692209)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        self.showHud()
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            self.setData(pregnancy)
            self.hideHud()
        }
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideTabBar(hidden: true, animated: false)
        var user = DatabaseManager.sharedManager.getLocalPregnancys()
        
        let realm = try! Realm()
        try! realm.write {
            
            user?.check_screen_calculator = 0
            realm.add(user!)
        }
        if ( user?.baby_already_born == 1 ){
            mSwBabyAlreadyBorn.isOn = true
        } else {
            mSwBabyAlreadyBorn.isOn = false
        }
        
    }
    
    fileprivate func calculateTimes(_ deadlineDate: Date) {
        var components = Calendar.current.dateComponents([ .day], from: Date(), to:deadlineDate.addingTimeInterval(86399))
        
        if var day = components.day {
            if (day < 0 ) {
                day = 280
            } else {
                day = 280 - day
            }
            
            self.weeks = day / 7
            self.days = day - (self.weeks! * 7 )
        } else {
            self.days = 0
            self.weeks = 0
        }
        
        changeTitleDate()
    }
    
    fileprivate func changeTitleDate() {
        
        if let week = weeks , let day = days {
            self.mLblWeekPregnancy.text = "\(week)"
            if mSelectedShowWeek == 1 {
                self.mLblWeekPregnancy.text = "\(week + 1)"
            }
            
            if day == 0 {
                self.mSubLblWeekPregnancy.text = "Mang thai \(week) tuần "
            }else {
                self.mSubLblWeekPregnancy.text = "Mang thai \(week) tuần và \(day) ngày"
            }
        }
    }
    
    func setData(_ pregnancy: PregPregnancy?) -> Void {
        
        if let pregnancy = pregnancy {
            
            mSelectedShowWeek = pregnancy.show_week
            switch pregnancy.baby_gender {
            case 1 :
                checkBabyGender = "Trai"
            case 2 :
                checkBabyGender = "Gái"
            case 3 :
                checkBabyGender = "Sinh đôi"
            case 4 :
                checkBabyGender = "Chưa xác định"
            default:
                checkBabyGender = ""
            }
            
            calculateTimes(pregnancy.due_date)
            
            let dueDateString = dateFormatter.string(from: pregnancy.due_date)
            self.selectedDate = pregnancy.due_date
            self.mBtnBabyGender.setTitle(checkBabyGender, for: .normal)
            let dateOfBirth = dateFormatter.string(from: pregnancy.date_of_birth)
            
            switch mSelectedShowWeek {
            case 2 :
                self.mBtnShowWeek.setTitle("Hoàn Thành", for: .normal)
            case 1 :
                self.mBtnShowWeek.setTitle("Tuần Hiện Tại", for: .normal)
            default:
                break
            }
            
            switch pregnancy.baby_already_born {
            case 1 :
                mSwBabyAlreadyBorn.isOn = true
                mLbBabyAlreadyBorn.text = "Đã sinh"
                self.mBtnDueDate.setTitle("", for: .normal)
                self.mBtnDateOfBirth.setTitle(dateOfBirth, for: .normal)
                self.mBtnDateOfBirth.isEnabled = true
                self.mSubLblWeekPregnancy.isHidden = true
                self.mViewWeek.isHidden = true
            default :
                mSwBabyAlreadyBorn.isOn = false
                mLbBabyAlreadyBorn.text = "Chưa sinh"
                self.mBtnDueDate.setTitle(dueDateString, for: .normal)
                self.mBtnDateOfBirth.setTitle("Chọn...", for: .normal)
                self.mBtnDateOfBirth.isEnabled = false
                self.mSubLblWeekPregnancy.isHidden = false
                self.mViewWeek.isHidden = false
            }
        }
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    // Button upload avatar
    @IBAction func AvatarTouchUpInside(_ sender: Any) {
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
    }
    
    @objc func updateAvatar(){
        DispatchQueue.main.async {
            DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTemp, isSuccess) in
                if let user = user {
                    var avatar = NetworkManager.rootDomain
                    if user.avatar.hasPrefix("http") {
                        avatar = user.avatar
                    } else {
                        avatar.append(user.avatar)
                    }
                    self.mAvatarButton.sd_setImage(with: URL(string:avatar), for: .normal, completed: { (image, error, cacheType, url) in
                        if let image = image {
                            self.mAvatarButton.setImage(image, for: .normal)
                        } else {
                            self.mAvatarButton.setImage(UIImage(named: "user_placeholder"), for: .normal)
                        }
                    })
                } else {
                    self.mAvatarButton.setImage(UIImage(named: "user_placeholder"), for: .normal)
                }
            })
        }
    }
    
    @IBAction func mBtnBabyGenderTouchUpInside(_ sender: Any) {
        if let pickerDateView = self.mShowingPickerDateView {
            pickerDateView.btnDoneTouchUpInside(self)
        }
        mPickerView.isHidden = false
        mPickerView.tag = 1
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnDuaDateTouchUpInside(_ sender: Any) {
        mPickerView.isHidden = true
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        checkDatePicker = 1
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let dayMax = calendar?.date(byAdding: .day, value: 280, to: Date(), options: .init(rawValue: 0))
        let dayMin = calendar?.date(byAdding: .day, value: 0, to: Date(), options: .init(rawValue: 0))
        self.navigationController?.view.addSubview(pickerDate)
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.maximumDate = dayMax
        pickerDate.datePicker.minimumDate = dayMin
        if let date = self.selectedDate {
            var user = DatabaseManager.sharedManager.getLocalPregnancys()
            let dateStr1 = user?["due_date"]
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            //        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            let strDate1 = dateFormatter.string(from: dateStr1 as! Date)
            let date = dateFormatter.date(from:strDate1)!
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day], from: date)
            let finalDate = calendar.date(from:components)
            
            pickerDate.datePicker.date = finalDate!
        }
        
        
        pickerDate.datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        self.mShowingPickerDateView = pickerDate
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        self.selectedDate  = sender.date
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
        }
    }
    
    @IBAction func mBtnHelpMeCalculator(_ sender: Any) {
        mPickerView.isHidden = true
        if let pickerDateView = self.mShowingPickerDateView {
            //            pickerDateView.btnDoneTouchUpInside(self)
        }
        let storyBoardCalculator = UIStoryboard(name: "menu", bundle: nil)
        let calculatorViewController = storyBoardCalculator.instantiateViewController(withIdentifier: "CalculatorDueDate") as! CalculatorDueDateViewController
        calculatorViewController.delegate = self
        let startDateNew = self.caculatorStartDate(mBtnDueDate.title(for: .normal)!)
        let startDateNewStr = dateFormatter.string(from: startDateNew)
        calculatorViewController.datePushFromSetting = startDateNewStr
        self.navigationController?.pushViewController(calculatorViewController, animated: true)
        let realm = try! Realm()
        try! realm.write {
            
            var user = DatabaseManager.sharedManager.getLocalPregnancys()
            user?.check_screen_calculator = 1
            realm.add(user!)
        }
        
        //let calculatorViewController = CalculatorDueDateViewController()
        //calculatorViewController.dateFromString = lblEndCaculator.text!
    }
    
    @IBAction func mBtnShowWeekTouchUpInside(_ sender: Any) {
        if let pickerDateView = self.mShowingPickerDateView {
            pickerDateView.btnDoneTouchUpInside(self)
        }
        mPickerView.selectRow(mSelectedShowWeek - 1, inComponent:0, animated:true)
        mPickerView.isHidden = false
        mPickerView.tag = 2
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mSwBabyAlreadyBornValueChanged(_ sender: Any) {
        
        if mSwBabyAlreadyBorn.isOn == true {
            mLbBabyAlreadyBorn.text = "Đã sinh"
            self.prompt("Thông báo", message: "Khi bật chức năng này lên, bạn sẽ không còn theo dõi thai kỳ nữa. Bạn có chắc không?", okHandler: { (alert) in
                self.mBtnDateOfBirth.setTitle(self.mBtnDueDate.title(for: .normal), for: .normal)
                self.mBtnDueDate.setTitle("", for: .normal)
                self.mBtnDateOfBirth.isEnabled = true
                self.mSubLblWeekPregnancy.isHidden = true
                self.mViewWeek.isHidden = true
                let realm = try! Realm()
                try! realm.write {
                    
                    var user = DatabaseManager.sharedManager.getLocalPregnancys()
                    user?.baby_already_born = 1
                    realm.add(user!)
                }
                
            },cancelTitle: "Không") { (alert) in
                self.mLbBabyAlreadyBorn.text = "Chưa sinh"
                self.mSwBabyAlreadyBorn.isOn = false
            }
        } else {
            // check ngay du sinh
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: self.mBtnDateOfBirth.title(for: .normal) ?? "")
            self.calculateTimes(date ?? Date())
            //
            mLbBabyAlreadyBorn.text = "Chưa sinh"
            self.mBtnDueDate.setTitle(self.mBtnDateOfBirth.title(for: .normal), for: .normal)
            self.mBtnDateOfBirth.setTitle("Chọn...", for: .normal)
            self.mBtnDateOfBirth.isEnabled = false
            self.mSubLblWeekPregnancy.isHidden = false
            self.mViewWeek.isHidden = false
            let realm = try! Realm()
            try! realm.write {
                
                var user = DatabaseManager.sharedManager.getLocalPregnancys()
                user?.baby_already_born = 0
                realm.add(user!)
            }
            
        }
    }
    
    @IBAction func mBtnDateOfBirthTouchUpInside(_ sender: Any) {
        mPickerView.isHidden = true
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let dayMax = calendar?.date(byAdding: .day, value: 280, to: Date(), options: .init(rawValue: 0))
        let dayMin = calendar?.date(byAdding: .day, value: 0, to: Date(), options: .init(rawValue: 0))
        pickerDate.datePicker.maximumDate = dayMax
        pickerDate.datePicker.minimumDate = dayMin
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        checkDatePicker = 2
        if let date = self.selectedDate {
            pickerDate.datePicker.date = date
        }
        self.navigationController?.view.addSubview(pickerDate)
        self.mShowingPickerDateView = pickerDate
    }
    
    @IBAction func mBtnDoneTouchUpInside(_ sender: Any) {
        self.showHud()
        switch mBtnBabyGender.title(for: .normal) {
        case "Trai" :
            checkBabyGenderInt = 1
        case "Gái" :
            checkBabyGenderInt = 2
        case "Sinh đôi" :
            checkBabyGenderInt = 3
        case "Chưa xác định" :
            checkBabyGenderInt = 4
        default:
            checkBabyGenderInt = 0
        }
        
        switch mSwBabyAlreadyBorn.isOn {
        case true:
            checkSW = 1
        default:
            checkSW = 0
        }
        
        //        let dateFromString = mBtnDueDate.title(for: .normal)! + "T00:00:00"
        //        let dateFromStringDOB = mBtnDateOfBirth.title(for: .normal)! + "T00:00:00"
        //        let startDateNew = self.caculatorStartDate(mBtnDueDate.title(for: .normal)!)
        //        let startDateNewStr = dateFormatter.string(from: startDateNew)
        
        self.showHud()
        
        if checkSW == 1 { // Đã sinh
            
            self.dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            let dateFromString = dateFormatter.string(from: Date())
            let dateFromStringDOB = mBtnDateOfBirth.title(for: .normal)! + "T00:00:00"
            
            self.dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateCalNew = dateFormatter.string(from: Date())
            
            let startDateNew = self.caculatorStartDate(dateCalNew)
            let startDateNewStr = dateFormatter.string(from: startDateNew) + "T00:00:00"
            
            
            
            DatabaseManager.sharedManager.putPregnancy(showWeek: self.mSelectedShowWeek, babyGender: checkBabyGenderInt!, dueDate: dateFromString, startDate: startDateNewStr, dateOfBirth: dateFromStringDOB, babyAlreadyBorn: checkSW, weightBeforePregnant: nil) { (pregUser, isTemp, isSuccess) in
                if (isSuccess == false) {
                    let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        DatabaseManager.sharedManager.logout {
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                        self.hideHud()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                if(!isTemp) {
                    self.hideHud()
                    NotificationManager().scheduleNotification()
                    if let currentViewController = ViewManager.getCurrentViewController() {
                        if let navigationController = currentViewController.navigationController {
                            navigationController.popToRootViewController(animated: true)
                        }
                    }
                    //                    NetworkManager.shareInstance.apiDeleteAllMyWeights(callBack: { (data, message, isSuccess) in
                    //                        self.hideHud()
                    //                        NotificationManager().scheduleNotification()
                    //                        if let currentViewController = ViewManager.getCurrentViewController() {
                    //                            if let navigationController = currentViewController.navigationController {
                    //                                navigationController.popToRootViewController(animated: true)
                    //                            }
                    //                        }
                    //                    })
                }
            }
        } else {
            
            let dateFromString = mBtnDueDate.title(for: .normal)! + "T00:00:00"
            let dateFromStringDOB = mBtnDateOfBirth.title(for: .normal)! + "T00:00:00"
            let startDateNew = self.caculatorStartDate(mBtnDueDate.title(for: .normal)!)
            
            let startDateNewStr = dateFormatter.string(from: startDateNew) + "T00:00:00"
            
            DatabaseManager.sharedManager.putPregnancy(showWeek: self.mSelectedShowWeek, babyGender: checkBabyGenderInt!, dueDate: dateFromString, startDate: startDateNewStr, dateOfBirth: dateFromStringDOB, babyAlreadyBorn: checkSW, weightBeforePregnant: nil) { (pregUser, isTemp, isSuccess) in
                if (isSuccess == false) {
                    let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        DatabaseManager.sharedManager.logout {
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                        self.hideHud()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }
                if(!isTemp) {
                    self.hideHud()
                    NotificationManager().scheduleNotification()
                    if let currentViewController = ViewManager.getCurrentViewController() {
                        if let navigationController = currentViewController.navigationController {
                            navigationController.popToRootViewController(animated: true)
                        }
                    }
                    //                    NetworkManager.shareInstance.apiDeleteAllMyWeights(callBack: { (data, message, isSuccess) in
                    //                        self.hideHud()
                    //                        NotificationManager().scheduleNotification()
                    //                        if let currentViewController = ViewManager.getCurrentViewController() {
                    //                            if let navigationController = currentViewController.navigationController {
                    //                                navigationController.popToRootViewController(animated: true)
                    //                            }
                    //                        }
                    //                    })
                }
            }
        }
    }
}

extension DueDateViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch mPickerView.tag {
        case 1:
            return dataPickerBabyGender.count
        default:
            return dataPickerShowWeek.count
        }
        //return dataPickerShowWeek.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch mPickerView.tag {
        case 1:
            if let selected = self.mBtnBabyGender.title(for: .normal) {
                if selected == dataPickerBabyGender[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return dataPickerBabyGender[row]
        default:
            if let selected = self.mBtnShowWeek.title(for: .normal) {
                if selected == dataPickerShowWeek[row+1] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return dataPickerShowWeek[row+1]
        }
        
        //return dataPickerShowWeek[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch mPickerView.tag {
        case 1:
            mBtnBabyGender.setTitle(dataPickerBabyGender[row], for: .normal)
            mPickerView.isHidden = true
            break
        default:
            mBtnShowWeek.setTitle(dataPickerShowWeek[row+1], for: .normal)
            mPickerView.isHidden = true
            self.mSelectedShowWeek = row + 1
            changeTitleDate()
            
            break
        }
    }
}

extension DueDateViewController: PickerDateViewDelegate {
    func doneTapped(_ sender: PickerDateView) {
        print("tapped ben setting")
    }
    
    
    func didSelectedDateString(dateString: String) {
        if self.checkDatePicker == 1 {
            if (mSwBabyAlreadyBorn.isOn == true) {
                mSwBabyAlreadyBorn.isOn = false
                self.mSwBabyAlreadyBornValueChanged(mSwBabyAlreadyBorn)
                
            }
            
            
            self.mBtnDueDate.setTitle(dateString, for: .normal)
            
            let deadlineDate = convertStringToDate(dateString)
            calculateTimes(deadlineDate)
            
            
            //            if let week = weeks {
            //                self.mLblWeekPregnancy.text = "\(week)"
            //                if mSelectedShowWeek == 2 {
            //                    self.mLblWeekPregnancy.text = "\(week + 1)"
            //                }
            //
            //                let day = self.days!
            //
            //                if day == 0 {
            //                    self.mSubLblWeekPregnancy.text = "Mang thai \(week) tuần "
            //                }
            //                else if day == 7{
            //                    self.mSubLblWeekPregnancy.text = "Mang thai \(week + 1) tuần"
            //                }
            //                else {
            //                    self.mSubLblWeekPregnancy.text = "Mang thai \(week) tuần và \(day) ngày"
            //                }
            //            }
            
        } else if self.checkDatePicker == 2 {
            self.mBtnDateOfBirth.setTitle(dateString, for: .normal)
        }
    }
}


extension DueDateViewController: delegateCaculatorDueDate {
    func dateCalculatorSend(with data: String) {
        mBtnDueDate.setTitle(data, for: .normal)
        self.dateStringCurrent = data
        
        let dueDate = self.convertStringToDate(data)
        calculateTimes(dueDate)
        
        //        if let week = weeks {
        //            self.mLblWeekPregnancy.text = "\(week)"
        //            if mSelectedShowWeek == 2 {
        //                self.mLblWeekPregnancy.text = "\(week + 1)"
        //            }
        //
        //            var day: Int = 0
        //            if let d = self.days {
        //                day = d
        //            }
        //
        //            self.mSubLblWeekPregnancy.text = "Mang thai \(week) tuần và \(day) ngày"
        //        }
    }
    
    func switchWasBorn(with data: Bool) {
        if data == true ,  mSwBabyAlreadyBorn.isOn == true {
            mLbBabyAlreadyBorn.text = "Chưa sinh"
            self.mBtnDateOfBirth.isEnabled = false
            self.mSubLblWeekPregnancy.isHidden = false
            self.mViewWeek.isHidden = false
            mSwBabyAlreadyBorn.isOn = false
            self.mBtnDateOfBirth.setTitle("Chọn...", for: .normal)
        }
    }
}
