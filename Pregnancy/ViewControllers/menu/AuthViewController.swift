//
//  AuthViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/7/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import RealmSwift
import FirebaseAuth
import SwiftyJSON
import Alamofire

enum AuthType : Int {
    case SignUp = 0
    case Login
}

enum AuthCellType: String {
    case google
    case facebook
    case border
    case noImage = "no image"
}

struct Connectivity {
//    static let sharedInstance = NetworkReachabilityManager()!
//    static var isConnectedToInternet:Bool {
//        return self.sharedInstance.isReachable
//    }
    static func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class AuthViewController: BaseViewController {
    
    var authType : AuthType = .SignUp
    var isShowPass:Bool = false
    var mNavigationHidden: Bool = false
    var idGuest: Int = 0
    
    
    @IBOutlet weak var mPasswordView: UIView!
    @IBOutlet weak var mForgotPasswordBtn: UIButton!
    @IBOutlet weak var mTableTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var mScrollView: UIScrollView!
    
    let cellItems: Dictionary<AuthType, Array<MenuData>> = [
        AuthType.Login:[
            MenuData(name: "Đăng nhập ", image: "key", type:AuthCellType.noImage.rawValue),
            MenuData(name: "", image: "", type:AuthCellType.border.rawValue),
            MenuData(name: "Đăng nhập bằng Google", image: "google", type:AuthCellType.google.rawValue),
            MenuData(name: "Đăng nhập bằng Facebook", image: "facebook", type:AuthCellType.facebook.rawValue)
        ],
        
        AuthType.SignUp:[
            MenuData(name: "Đăng ký", image: "key",type:AuthCellType.noImage.rawValue),
            MenuData(name: "", image: "",type:"border"),
            MenuData(name: "Đăng nhập bằng Google", image: "google",type:AuthCellType.google.rawValue),
            MenuData(name: "Đăng nhập bằng Facebook", image: "facebook",type:AuthCellType.facebook.rawValue)
        ]
    ]
    
    // phan biet 2 truong hop login
    // 0 la truong hop login binh thuong
    // 1 la truong hop login khi login viewcontroller duoc push sau khi dang nhap bang tai khoan khach
    var loginType: Int = 0
    
   
    
    @IBOutlet weak var mUserTF: UITextField!
    @IBOutlet weak var mPassTF: UITextField!
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var mLinkLabel: UILabel!
    @IBOutlet weak var mTextView: UITextView!
    @IBOutlet weak var heightOfTextView: NSLayoutConstraint!
    var activeTextField : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTableView.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        mTableView.register(UINib.init(nibName: "BorderTableViewCell", bundle: nil), forCellReuseIdentifier: "BorderTableViewCell")
        mTableView.alwaysBounceVertical = false
        mNavigationHidden = self.navigationController?.navigationBar.isHidden ?? true
        
        self.setNavigationTitle()
        self.mTableView.endEditing(true)
        mTableView.tableFooterView = UIView()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        self.setupShowHideKeyboard()
    }   
    func setupShowHideKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        
        var userInfo = aNotification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = self.mScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.mScrollView.contentInset = contentInset
        self.mScrollView.scrollIndicatorInsets = contentInset
        let rect = mTableView.frame
        let test = view.bounds.height - (rect.size.height + rect.origin.y + self.mScrollView.contentInset.top) + 8
        let different = keyboardFrame.size.height - test
        if different > 0 {
            self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = self.mScrollView.contentInset
        contentInset.bottom = 0
        self.mScrollView.contentInset = contentInset
        self.mScrollView.scrollIndicatorInsets = contentInset
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        mForgotPasswordBtn.isHidden = (authType == .SignUp)
        mForgotPasswordBtn.isEnabled = (authType != .SignUp)
        mPasswordView.isHidden = (authType == .SignUp)
        mTableTopContraint.priority = UILayoutPriority(rawValue: (authType == .SignUp) ? 750:250)
        
    }

    override func doDefaultBack(_ sender: Any?) {
        super.doDefaultBack(sender)
        self.navigationController?.setNavigationBarHidden(mNavigationHidden, animated: true)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func setNavigationTitle() {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = (self.authType == .SignUp) ? "Đăng ký" : "Đăng nhập"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.heightOfTextView?.constant = self.mTextView.contentSize.height
    }
    
    @IBAction func pressToShowPass(_ sender: Any) {
        if isShowPass == true {
            mPassTF.isSecureTextEntry = false
        }
        else {
            mPassTF.isSecureTextEntry = true
        }
        isShowPass = !isShowPass
    }
    
    
    @IBAction func didTapOutside(_ sender: Any?) {
        if(mPassTF.isFirstResponder) {
            mPassTF.resignFirstResponder()
        }
        if(mUserTF.isFirstResponder) {
            mUserTF.resignFirstResponder()
        }
    }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        return touch.view == gestureRecognizer.view
//    }
    
}

extension AuthViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.didTapOutside(nil)
        let type = self.cellItems[self.authType]?[indexPath.row].type
        
        switch type {
            
        case AuthCellType.noImage.rawValue:
            if self.authType == .Login {
                if let user = mUserTF.text {
                    
                    if user.isEmpty {
                        self.alert("Vui lòng nhập số điện thoại!")
                        return
                    }
                    
                    if(user.convertVietnamPhoneNumer() == nil) {
                        self.alert("Vui lòng nhập đúng số điện thoại")
                        return
                    }
                    if let password = mPassTF.text {
                        if password.isEmpty {
                            self.alert("Vui lòng nhập mật khẩu!")
                            return
                        }
                        if (password.length < 6) {
                            self.alert("Mật khẩu phải có độ dài tối thiểu 6 ký tự")
                            return
                        }
                        if let vnphone = user.convertVietnamPhoneNumer(){
                            self.showHud()
                            if Connectivity.isConnectedToInternet() {
                                NetworkManager.shareInstance.apiCheckPhoneNumber(phone: vnphone) { (data, message, isSuccess) in
                                    if(isSuccess ) {
                                        if (message == "Data not found.") {
                                            let actionSheetController = UIAlertController (title: "Thông báo", message: "SĐT chưa đăng ký tài khoản trên hệ thống, đăng ký ngay!", preferredStyle: .alert)
                                            
                                            //Add Save-Action
                                            actionSheetController.addAction(UIAlertAction(title: "Đăng ký", style: UIAlertAction.Style.default, handler: { (actionSheetController) -> Void in
                                                let vc = UIStoryboard.init(name: "menu", bundle: Bundle.main).instantiateViewController(withIdentifier: "AuthViewController") as? AuthViewController
                                                self.navigationController?.pushViewController(vc!, animated: true)
                                                
                                            }))
                                            
                                            self.present(actionSheetController, animated: true, completion: nil)
                                            
                                        } else {
                                            self.login(user.convertVietnamPhoneNumer(), password: password, token: nil, provider: nil)
                                        }
                                        
                                        self.hideHud()
                                    } else {
                                        self.hideHud()
                                    }
                                }
                            }
                            else {
                                self.hideHud()
                                self.alert("Không có kết nối. Vui lòng kiểm tra lại!")
                            }
                        }
                    } else {
                        self.alert("Vui lòng nhập mật khẩu!")
                    }
                    
                } else {
                    self.alert("Vui lòng nhập số điện thoại!")
                }
                
            } else {
                self.signup()
                AuthManager.shared.authPhoneNumber = mUserTF.text
            }
        case AuthCellType.google.rawValue:
            GIDSignIn.sharedInstance()?.signOut()
            GIDSignIn.sharedInstance().signIn()
        case AuthCellType.facebook.rawValue:
            let loginManager = FBSDKLoginManager.init()
            if UIApplication.shared.canOpenURL(URL(string: "fb://")!){
                loginManager.loginBehavior = FBSDKLoginBehavior.systemAccount
            }
            loginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
                self.handleFacebookLogin(result, error: error)
            }
        default:
            print(type as Any)
        }
    }
    
    func signup() {
        
        if let phone = mUserTF.text {
            if phone.isEmpty {
                self.alert("Vui lòng nhập số điện thoại!")
                return
            }
            UserDefaults.standard.set(nil, forKey: "authVerificationID")
            UserDefaults.standard.synchronize()
            if let vnphone = phone.convertVietnamPhoneNumer(), let externalVNPhone = vnphone.convertVietnamPhoneNumer(true) {
                self.showHud()
                if Connectivity.isConnectedToInternet() {
                    NetworkManager.shareInstance.apiCheckPhoneNumber(phone: vnphone) { (data, message, isSuccess) in
                    if(isSuccess) {
                        if (message == "Data not found.") {
                            Auth.auth().useAppLanguage()
                            PhoneAuthProvider.provider().verifyPhoneNumber(externalVNPhone, uiDelegate: self) { (verificationID, error) in
                                if let error = error {
                                    self.alert(error.localizedDescription)
                                    self.hideHud()
                                    return
                                }
                                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                                UserDefaults.standard.synchronize()
                                dataUser.isSignup = true
                                dataUser.phone = vnphone
                                self.performSegue(withIdentifier: "AuthviewControllerToOTPViewController", sender: nil)
                                self.hideHud()
//                                self.alert("Mã xác nhận đã gửi thành công")
                                let alert = UIAlertController(title: "", message: "Mã xác nhận đã gửi thành công", preferredStyle: .alert)
                                self.present(alert, animated: true, completion: nil)
                                let when = DispatchTime.now() + 3
                                DispatchQueue.main.asyncAfter(deadline: when){
                                    alert.dismiss(animated: true, completion: nil)
                                }
                            }
                            
                        }
                        else {
                        self.alert("Số điện thoại đã tồn tại")
                        self.hideHud()
                        }
                    }
                   
                    
                    
                    
                    else {
                        Auth.auth().useAppLanguage()
                        PhoneAuthProvider.provider().verifyPhoneNumber(externalVNPhone, uiDelegate: self) { (verificationID, error) in
                            if let error = error {
                                self.alert(error.localizedDescription)
                                self.hideHud()
                                return
                            }
                            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                            UserDefaults.standard.synchronize()
                            dataUser.isSignup = true
                            dataUser.phone = vnphone
                            self.performSegue(withIdentifier: "AuthviewControllerToOTPViewController", sender: nil)
                            self.hideHud()
//                            self.alert("Mã xác nhận đã gửi thành công")
                            let alert = UIAlertController(title: "", message: "Mã xác nhận đã gửi thành công", preferredStyle: .alert)
                            self.present(alert, animated: true, completion: nil)
                            let when = DispatchTime.now() + 3
                            DispatchQueue.main.asyncAfter(deadline: when){
                                alert.dismiss(animated: true, completion: nil)
                            }
                            
                        }
                    }
                }
                }
                else {
                    self.hideHud()
                    self.alert("Không có kết nối. Vui lòng kiểm tra lại!")
                }
            } else {
                self.alert("Vui lòng nhập đúng số điện thoại")
            }
        } else {
            self.alert("Vui lòng nhập số điện thoại")
        }
    }
    
    func login (_ user: String?, password: String?, token: String?, provider: SocialProvider? ) -> Void {
        
        DatabaseManager.sharedManager.login(in: self, user: user, password: password, token: token, provider: provider, loginType: loginType) { (user, isTempData, isGetInfoSuccess, isLoginSuccess) in
            //login failed
  
            if(!isLoginSuccess){
                
                
                self.hideHud()
//                self.alert("Sai số điện thoại hoặc mật khẩu.")
                self.alert("Thông tin đăng nhập không đúng, vui lòng nhập lại")
                return
            }
            // login succeed but failed to get info
            if(!isGetInfoSuccess) {
                self.hideHud()
                self.alert("Vui lòng thử lại sau!")
                return
            }
            
            // check if data on cloud is empty ( name ) then let's user put their info if need
            if(!isTempData) {
                DatabaseManager.sharedManager.getPregnancys(callBack: { (pregnancy, isTemp, isSuccess, rawData) in
                    self.hideHud()
                    NotificationManager().scheduleNotification()
                    if let provider = provider {
                        if let user = user, let rawData = rawData {
                            let firstData = rawData.arrayValue.first
                            if user.first_name.isEmpty || firstData == nil || (firstData != nil && firstData?["due_date"].stringValue.count == 0 )  {
                                
                                dataUser.socialTypeId = Int(user.social_type_id)!
                                dataUser.isSignup = false
                                self.performSegue(withIdentifier: "auth_view_controller_to_setting_account_view", sender: nil)
                                return
                            }
                            
                        }
                    }
                    else {
                        if let user = user, let rawData = rawData {
                            let firstData = rawData.arrayValue.first
                            if user.first_name.isEmpty || firstData == nil || (firstData != nil && firstData?["due_date"].stringValue.count == 0 )  {
                                dataUser.isSignup = false
                                self.performSegue(withIdentifier: "auth_view_controller_to_setting_account_view", sender: nil)
                                return
                            }
                            
                        }
                    }
                    
                    // show main view
                    if self.idGuest != 0 {
                        NetworkManager.shareInstance.apiDeleteUser(userId: self.idGuest, callBack: { (data, message, isSuccess) in
                            if isSuccess {
                                self.idGuest = 0
                            }
                        })
                    }
                    self.showMainView()
                })
                
            }
        }
    }
    
    func handleFacebookLogin(_ result: FBSDKLoginManagerLoginResult?, error: Error?) -> Void {
        if let error = error as NSError? {
            self.alert(error.userInfo[FBSDKErrorLocalizedDescriptionKey] as? String ?? "Lỗi không xác định")
        } else {
            if let result = result as FBSDKLoginManagerLoginResult? {
                if(result.token != nil) {
                    self.login(nil, password: nil, token: result.token.tokenString, provider: .facebook)
                }
            }
        }
    }
    
    @objc func showMainView() {
        if(Thread.isMainThread != true) {
            self.perform(#selector(showMainView), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        
        if(ViewManager.getRootTabbarController() == nil) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            let rootVC = ViewManager.getRootViewController()
            rootVC.createRootTabbarControllerIfNeed()
            ViewManager.showTabbarController()
            
            // Check kickcounter khi đăng nhập thành công và show mainview
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.checkDataKickCounter()
            
        } else if loginType == 1 {
            // Check kickcounter khi đăng nhập thành công và show mainview
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.checkDataKickCounter()
            
            
            // Neu login type == 1 thi nghia la rootTabbarcontroller da co roi
            // chi can pop back ve tabbar la dc
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            self.doDefaultBack(nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if(segue.identifier == "AuthviewControllerToOTPViewController") {
            if let vc = segue.destination as? OTPViewController {
                if(authType == .SignUp) {
                    vc.mIsSignup = true
//                    vc.mPhoneNumber =  mUserTF.text!.convertVietnamPhoneNumer()
                }

            }
        }
    }
    
}

extension AuthViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellItems[self.authType]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellArray = self.cellItems[self.authType]!
        
        if cellArray[indexPath.row].type == "border"{
            let cell:BorderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BorderTableViewCell", for: indexPath) as! BorderTableViewCell
            cell.isUserInteractionEnabled = false
            return cell
        }
        else {
            let cell:GuideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
                        cell.selectionStyle = .none
            let data = cellArray[indexPath.row]
            cell.configureCell(data)
        
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let verticalPadding: CGFloat = 14
        
        let maskLayer = CALayer()
        maskLayer.backgroundColor = UIColor.black.cgColor
        maskLayer.frame = CGRect(x: cell.bounds.origin.x, y: cell.bounds.origin.y, width: cell.bounds.width, height: cell.bounds.height).insetBy(dx: 8, dy: verticalPadding/2)
        cell.layer.mask = maskLayer
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


extension AuthViewController: GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension AuthViewController: GIDSignInDelegate {
    // Present a view that prompts the user to sign in with Google
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("\(error.localizedDescription)")
        }
        else {
            self.login(nil, password: nil, token:user.authentication.accessToken, provider: .google)
            
        }
    }
    
}

extension AuthViewController: AuthUIDelegate {
    
}
