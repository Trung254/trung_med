//
//  SubAboutUsViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/2/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SubAboutUsViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var mWebView: UIWebView!
    var passedTitle:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getWebViewContent()
        self.setTitle(title: passedTitle)
//        self.setNavigationTitle()
//        self.setNavigationIcon()
    }
    
//    func setNavigationTitle(){
//
//        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
//        lbl.text = passedTitle
//        lbl.textColor = .white
//        lbl.textAlignment = .center
//        lbl.font = lbl.font.withSize(25.0)
//        navigationItem.titleView = lbl
//    }
//
//    func setNavigationIcon(){
//
//        let backButton = UIButton(type: .custom)
//        backButton.setImage(UIImage(named: "Back"), for: .normal)
//        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
//        backButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
//        let barButton = UIBarButtonItem(customView: backButton)
//
//        self.navigationItem.leftBarButtonItem = barButton
//    }
    
//    @objc func handleNavigationIcon(){
//        self.navigationController?.popViewController(animated: true)
//    }
    
    func getWebViewContent(){
        let htmlPath = Bundle.main.path(forResource: "dieukhoansudung", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebView.loadRequest(req)
//        mWebView.scrollView.isScrollEnabled = false
    }
    
    
    

    

}
