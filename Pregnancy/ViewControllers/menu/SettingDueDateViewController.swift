//
//  SettingCreatAccountViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/22/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SettingDueDateViewController: BaseViewController {

    var dateGetFromDueDate : String?
    var periodSaveFomCalDueDate = ""
    var checkDueDate = false
    
    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var btnDueDateLbl: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Ngày dự sinh")
        
        let textAttr : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 17),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
//        btnDueDateLbl.setAttributedTitle(NSMutableAttributedString(string: "phân tích",
//                                                                 attributes: textAttr), for: .normal)
        
        let image = UIImage(named:"calendar")
        btnDueDate.setImage(image, for: .normal)
        btnDueDate.backgroundColor = .white
        btnDueDate.setTitleColor(.black, for: .normal)
        btnDueDate.imageView?.contentMode = .scaleAspectFit
        btnDueDate.contentHorizontalAlignment = .left
        btnDueDate.imageEdgeInsets = UIEdgeInsets(top:0, left:btnDueDate.frame.width - 40, bottom:0, right:0)
        btnDueDate.titleEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dueDate = Calendar.current.date(byAdding: .day, value: 280, to: Date())!
        if dataUser.dueDate.isEmpty {
            self.btnDueDate.setTitle(dateFormatter.string(from: dueDate), for: .normal)
            dataUser.dueDate = dateFormatter.string(from: dueDate)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        if dataUser.dueDate.isEmpty == false {
            self.btnDueDate.setTitle(dataUser.dueDate, for: .normal)
        }
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    @IBAction func mBtnDueDateTouchUpInside(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        
        pickerDate.delegate = self
        let calendar = NSCalendar(calendarIdentifier: .gregorian)
        let dayMax = calendar?.date(byAdding: .day, value: 280, to: Date(), options: .init(rawValue: 0))
        let dayMin = calendar?.date(byAdding: .day, value: 0, to: Date(), options: .init(rawValue: 0))
        self.navigationController?.view.addSubview(pickerDate)
        pickerDate.datePicker.maximumDate = dayMax
        pickerDate.datePicker.minimumDate = dayMin
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        if (self.btnDueDate.title(for: .normal)?.isEmpty)! == false {
            pickerDate.datePicker.date = self.convertStringToDate(self.btnDueDate.title(for: .normal)!)
        }
    }
    
    @IBAction func showDueDateCal(_ sender: Any) {
        let dueDateSB = UIStoryboard.init(name: "menu", bundle: nil)
        let dueDateVC = dueDateSB.instantiateViewController(withIdentifier: "CalculatorDueDate") as! CalculatorDueDateViewController
        dueDateVC.mSettingCreateAccount = self
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startDateNew = self.caculatorStartDate(btnDueDate.title(for: .normal)!)
        let startDateNewStr = dateFormatter.string(from: startDateNew)
        dueDateVC.datePushFromSetting = startDateNewStr
        
        self.navigationController?.pushViewController(dueDateVC, animated: true)
    }
    
    @IBAction func mBtnAgreeTouchUpInside(_ sender: Any) {
        if self.btnDueDate.title(for: .normal) == "" {
            self.alert("Bạn cần nhập ngày dự sinh")
        }
        else if(dataUser.isSignup) {
            performSegue(withIdentifier: "SettingDueDateToNewPassword", sender: sender)
        } else {
            self.showHud()
            if let user = DatabaseManager.sharedManager.getLocalUserInfo(){
                DatabaseManager.sharedManager.updateUserInformations(in: self, userId: user.id, firstName: dataUser.firstName, lastname: "", youAreThe: dataUser.youAreThe, location: dataUser.location) { (user, isTemp, isSuccess) in
                    if (!isTemp && isSuccess) {
                        if let pregnancy = DatabaseManager.sharedManager.getLocalPregnancys() {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            DatabaseManager.sharedManager.putPregnancy(showWeek: pregnancy.show_week, babyGender: pregnancy.baby_gender, dueDate: dataUser.dueDate, startDate: dataUser.startDate, dateOfBirth:dateFormatter.string(from: pregnancy.date_of_birth) , babyAlreadyBorn: pregnancy.baby_already_born, weightBeforePregnant: nil, callBack: { (user, isTemp, isSuccess) in
                                if(!isTemp && isSuccess) {
                                    self.getUserInfo()
                                }
                                
                            })
                        } else {
                            NetworkManager.shareInstance.apiPostPregnacys(babyGender: dataUser.dataGender, dueDate: dataUser.dueDate, startDate: dataUser.startDate, showWeek: 1) {
                                (data, message, isSuccess) in
                                if(!isTemp && isSuccess) {
                                    self.getUserInfo()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getUserInfo() {
        DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTempData, isSuccess) in
            if(!isTempData && isSuccess) {
                DatabaseManager.sharedManager.getPregnancys(callBack: { (pregnancy, isTemp, isSuccess, rawData) in
                    if (!isTempData && isSuccess) {
                        NetworkManager.shareInstance.apiPostAppSetting(reminders: true, length_units: true, weight_unit: 2) {
                            (data, message, isSuccess) in
                            DispatchQueue.main.async {
                                self.hideHud()
                                self.showMainView()
                            }
                        }
                    }
                    
                })
            }
        })
    }
    
    @objc func showMainView() {
        if(Thread.isMainThread != true) {
            self.perform(#selector(showMainView), on: Thread.main, with: nil, waitUntilDone: true)
            return
        }
        
        if(ViewManager.getRootTabbarController() == nil) {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            let rootVC = ViewManager.getRootViewController()
            rootVC.createRootTabbarControllerIfNeed()
            
            ViewManager.showTabbarController()
        } else {
            self.doDefaultBack(nil)
        }
    }
    
    private func convertDateToString(_ dataDate: Date,_ typeFormat: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = typeFormat
        let dateString = dateFormatter.string(from: dataDate)
        
        return dateString
    }
}

extension SettingDueDateViewController: PickerDateViewDelegate {
    func doneTapped(_ sender: PickerDateView) {
        print("tapped ben setting")
    }
    
    func didSelectedDateString(dateString: String) {
        dataUser.startDate = self.convertDateToString(self.caculatorStartDate(dateString),"yyyy-MM-dd")
        dataUser.dueDate = dateString
        //checkDueDate = true
        self.btnDueDate.setTitle(dateString, for: .normal)
    }
}
