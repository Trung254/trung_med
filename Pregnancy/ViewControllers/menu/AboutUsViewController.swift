//
//  AboutUsViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 1/2/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

struct Menu {
    public var name : String
    public var image : String
}

class AboutUsViewController: BaseViewController, UIWebViewDelegate {
    

    @IBOutlet weak var mWebView: UIWebView!
    @IBOutlet weak var mTableView: UITableView!
    @IBOutlet weak var heightOfWebView: NSLayoutConstraint!
    @IBOutlet weak var heightOfTableView: NSLayoutConstraint!
    
    var passedItem : String = ""
    var menuData:Array<Menu> = [
        Menu(name: "Điều khoản sử dụng", image: "document"),
        Menu(name: "Chính sách bảo mật", image: "file")
    ]
    var htmlString : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mTableView.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        mTableView.register(UINib.init(nibName: "BorderBottomTableViewCell", bundle: nil), forCellReuseIdentifier: "BorderBottomTableViewCell")
        mTableView.rowHeight = UITableView.automaticDimension
//        mTableView.alwaysBounceVertical = false
        self.getWebViewContent()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.setNavigationTitle()
//        self.mTableView.rowHeight = UITableView.automaticDimension
//        self.mTableView.estimatedRowHeight = 120
    }
    
    func setNavigationTitle(){
        
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Giới thiệu"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(25.0)
        navigationItem.titleView = lbl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let index = self.mTableView.indexPathForSelectedRow{
            self.mTableView.deselectRow(at: index, animated: true)
        }
        self.setBackButton()
    }

    func getWebViewContent(){
        let htmlPath = Bundle.main.path(forResource: "test", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebView.loadRequest(req)
        mWebView.scrollView.isScrollEnabled = false
        //        mWebView.scrollView.bounces = false
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        var frame = webView.frame
        frame.size.height = 1
        webView.frame = frame
        let fittingSize = webView.sizeThatFits(CGSize.init(width: 0, height: 0))
        heightOfWebView.constant = fittingSize.height
        
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "aboutUs_to_Sub" {
            let viewController = segue.destination as! SubAboutUsViewController
            viewController.passedTitle = passedItem
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.heightOfTableView?.constant = self.mTableView.contentSize.height
    }
}

extension AboutUsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        passedItem = menuData[indexPath.row].name
        performSegue(withIdentifier: "aboutUs_to_Sub", sender: self)
    }
}

extension AboutUsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 4 {
            let cell:BorderBottomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BorderBottomTableViewCell", for: indexPath) as! BorderBottomTableViewCell
            cell.height = 1
            
            
            return cell
        }
        else {
            let cell:GuideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
            cell.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
            cell.mImageView.image = UIImage(named: menuData[indexPath.row].image)
            cell.lblTitle.text = menuData[indexPath.row].name
            cell.lblTitle.font = UIFont.systemFont(ofSize: 14)
            
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if (indexPath.row == tableView.indexPathsForVisibleRows?.last?.row) {
//            heightOfTableView.constant = tableView.contentSize.height
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4{
            return 1
        }
        else {
            return 48
        }
        
    }
}


