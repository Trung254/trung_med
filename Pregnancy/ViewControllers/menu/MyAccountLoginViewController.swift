//
//  MyAccountLoginViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

class MyAccountLoginViewController: BaseViewController {
    
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mBtnYouAreThe: UIButton!
    @IBOutlet weak var mBtnLocation: UIButton!
    @IBOutlet weak var mProfileTopContraint: NSLayoutConstraint!
    
    @IBOutlet weak var mLogoutBtn: UIButton!
    @IBOutlet weak var mPickerTopLayout: NSLayoutConstraint!
    @IBOutlet weak var mLblAccountEmail: UILabel!
    @IBOutlet weak var mlblLastLogin: UILabel!
    @IBOutlet weak var mFirstNameTF: UITextField!
    @IBOutlet weak var mLastNameTF: UITextField!
    
    var pickerItemYouAreThe : [String] = ["Mẹ", "Bố", "Mẹ đơn thân", "Ông", "Dì/Chú", "Người đồng hành", "Bạn bè"]
    var pickerItemLocation : [String] = ["Việt Nam", "England"]
    var selectedPicker : String = ""
    var passedTitle = ""
    var authType: AuthType = .SignUp
    var userId : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showHud()
        DatabaseManager.sharedManager.getUserInfo(nil, nil) { (user, isTempData, isSuccess) in
            self.setData(user)
            self.hideHud()
        }
        
        self.setLeftMainMenu()
        self.setTitle(title: "Tài khoản của tôi")
        mPickerView.isHidden = true
        //mPickerViewLocation.isHidden = true
        showMessageIfLogin()
//        userId = (DatabaseManager.sharedManager.getLocalUserInfo()?.id)!
        DatabaseManager.sharedManager.getUserInfo { (pregUser, mnessage, isSuccess) in
            if pregUser != nil {
                self.userId = (pregUser?.id)!
                
            }
            
        }
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    func showMessageIfLogin() {
        guard DatabaseManager.sharedManager.isGuest() != true else {
            print("failed")
            self.mLblAccountEmail.isHidden = true
            return
        }
        guard DatabaseManager.sharedManager.isLogin() != true else {
            print("failed")
            self.mLblAccountEmail.isHidden = false
            return
            
        }

//        if DatabaseManager.sharedManager.isLogin() == true {
//            self.mLblLastBackUp.isHidden = false
//            self.mLblAccountEmail.isHidden = false
//        } else {
//            if DatabaseManager.sharedManager.isGuest() == true {
//                print(123)
//            self.mLblLastBackUp.isHidden = true
//            self.mLblAccountEmail.isHidden = true
//            }
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.mIsShowBackButton = false
        self.mIsShowRightButton = true
        //self.addPadLockButton()
        
        super.viewWillAppear(animated)
        
        if(DatabaseManager.sharedManager.isGuest()) {
            self.mProfileTopContraint.constant = 120
            self.mLogoutBtn.isHidden = true
        } else {
            self.mProfileTopContraint.constant = 0
            self.mLogoutBtn.isHidden = false
        }
        
    }
    
    private func getUserIdProfile() {
        NetworkManager.shareInstance.apiGetProfile(){
            (data, message, isSuccess) in
            let data = data as? JSON
            self.userId = data!["id"].intValue
        }
    }
    
    func setData(_ user: PregUser?) -> Void {
        
        if let user = user {
            self.mFirstNameTF.text = user.first_name
            self.mLastNameTF.text = user.last_name
            if (user.social_type_id == "1") {
                self.mLblAccountEmail.text = "Tài khoản: "
                self.fetchUserData { (femail) in
                    if let femail = femail {
                        self.mLblAccountEmail.text = "Tài khoản: \(femail)"
                    }
                    
                }
            } else if (user.social_type_id == "2") {
                if let ggShare = GIDSignIn.sharedInstance() {
                    if let cUser = ggShare.currentUser, let profile = cUser.profile, let email = profile.email {
                        self.mLblAccountEmail.text = "Tài khoản: \(email)"
                    }
                    
                }
            } else {
                self.mLblAccountEmail.text = "Tài khoản: \(user.phone)"
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
                let date = dateFormatter.date(from: user.time_last_login)
                dateFormatter.dateFormat = "dd MMM yyyy, HH:mm:ss"
                dateFormatter.locale = Locale(identifier: "vi_VI")
                if date != nil {
                    let time_last_login = dateFormatter.string(from: date!)
                
                    self.mlblLastLogin.text = "Đăng nhập lần cuối: \(time_last_login)"
                }
            }
            self.mBtnYouAreThe.setTitle(user.you_are_the, for: .normal)
            self.mBtnLocation.setTitle(user.location, for: .normal)
        }
        
        
        
    }
    
    private func fetchUserData(_ callBack: @escaping (_ femail:String?)-> ()) {
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            if user.social_type_id == "1" {
                let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
                graphRequest?.start(completionHandler: { (connection, result, error) in
                    if error != nil {
                        print("Error",error!.localizedDescription)
                    }
                    else{
                        print(result!)
                        if let field = result! as? [String:Any] {
                            if let email = field["email"] as? String {
                                callBack(email)
                            }
                        }
                    }
                })
            }
        }
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "myAccount_to_Login" {
            let viewController = segue.destination as! AuthViewController
            
            // check neu nhu authType la login nghia la dang nhap bang tai khoan co san
            // truyen loginType = 1 de chia truong hop login dac biet.
            viewController.loginType = authType == .Login ? 1 : 0
            viewController.authType = authType
            viewController.idGuest = self.userId
        }
    }
    
    
    @IBAction func mBtnLogIn(_ sender: Any) {
        self.authType = .Login
        performSegue(withIdentifier: "myAccount_to_Login", sender: self)
    }
    
    @IBAction func mBtnSignUp(_ sender: Any) {
        self.authType = .SignUp
        performSegue(withIdentifier: "myAccount_to_Login", sender: self)
    }
    
    @IBAction func mBtnYouAreTheTouchUpInside(_ sender: UIButton) {
        mPickerView.isHidden = false
        mPickerView.tag = 1
        self.mPickerTopLayout.constant = self.mProfileTopContraint.constant + sender.y
        mPickerView.reloadAllComponents()
        
    }
    
    @IBAction func mBtnLocationTouchUpInside(_ sender: UIButton) {
        mPickerView.isHidden = false
        mPickerView.tag = 2
        self.mPickerTopLayout.constant = self.mProfileTopContraint.constant + sender.y 
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnLogOutEveryWhere(_ sender: Any) {
        
        DatabaseManager.sharedManager.logout {
            
        }
    }
    
    @IBAction func mBtnDone(_ sender: Any) {
        
        if var name = mFirstNameTF.text {
            if var lastname = mLastNameTF.text {
                lastname = lastname.trimmingCharacters(in: .whitespacesAndNewlines)
            
            name = name.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if name.count > 0 {
                DatabaseManager.sharedManager.updateUserInformations(in: self, userId: userId,
                                                                     firstName: name, lastname: lastname,
                                                                     youAreThe: mBtnYouAreThe.title(for: .normal),
                                                                     location: mBtnLocation.title(for: .normal))
                { (user, isTempData, isSuccess) in
                    NotificationCenter.default.post(name: .didUpdatedCurrentUserInformations, object: nil)
                    
                    self.navigationController?.popToRootViewController(animated: true)
                }
                return
            }
        }
        
        self.alert("Vui lòng nhập tên bạn")
        mFirstNameTF.text = ""
        
    }
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = super.gestureRecognizer(gestureRecognizer, shouldReceive: touch)
        if let view = touch.view {
            if !view.isFirstResponder {
                self.view.endEditing(true)
            }
            if (view.isKind(of: UITextField.self)) {
                mPickerView.isHidden = true
            }
            
        }
        if(should) {
            mPickerView.isHidden = true
        }
        return should
    }
    
}

extension MyAccountLoginViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if mPickerView.tag == 1 {
            return pickerItemYouAreThe.count
        }
        else {
            return pickerItemLocation.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if mPickerView.tag == 1 {
            if let selected = self.mBtnYouAreThe.title(for: .normal) {
                if selected == pickerItemYouAreThe[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return pickerItemYouAreThe[row]
        }
        else {
            if let selected = self.mBtnLocation.title(for: .normal) {
                if selected == pickerItemLocation[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return pickerItemLocation[row]
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if mPickerView.tag == 1 {
                mBtnYouAreThe.setTitle(pickerItemYouAreThe[row], for: .normal)
                mPickerView.isHidden = true
        }
        else if mPickerView.tag == 2 {
            mBtnLocation.setTitle(pickerItemLocation[row], for: .normal)
            mPickerView.isHidden = true
        }

    }
}

extension MyAccountLoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 15
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
