//
//  SettingCreatAccountViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/24/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SnapKit

class SettingCreatAccountViewController: BaseViewController {

    @IBOutlet weak var mTextFieldName: UITextField!
    @IBOutlet weak var topPickerView: NSLayoutConstraint!
    @IBOutlet weak var mBtnYouAreThe: UIButton!
    @IBOutlet weak var mBtnBabyGender: UIButton!
    @IBOutlet weak var mBtnUnits: UIButton!
    @IBOutlet weak var mBtnLocation: UIButton!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mBtnTerms: UIButton!
    @IBOutlet weak var mBtnDone: UIButton!
    
    var dataPickerYouAreThe : [String] = ["Mẹ", "Bố", "Mẹ đơn thân", "Ông", "Dì/Chú", "Người đồng hành", "Bạn bè"]
    var dataPickerBabyGender : [String] = ["Trai", "Gái", "Sinh đôi", "Chưa xác định"]
    var dataPickerUnits : [String] = ["kg/cm"]
    var dataPickerLocation : [String] = ["Việt Nam", "England"]
    var checkGender : Int = 1
    var isGuest: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Thiết lập")
        mPickerView.isHidden = true
        
        if(dataUser.isSignup == false) {
            if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
                mTextFieldName.text = user.first_name
            }
            
        }
        
        let textAttr : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 15),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        mBtnTerms.setAttributedTitle(NSMutableAttributedString(string: "điều khoản",attributes: textAttr), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        
        
        if isGuest == true {
            NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        }
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    @objc func appMovedToBackground() {
        DatabaseManager.sharedManager.logout {}
    }
    
    @IBAction func mBtnYouAreTheTouchUpInside(_ sender: Any) {
        mPickerView.isHidden = false
        mPickerView.tag = 1
        topPickerView.constant = mBtnYouAreThe.y + mBtnYouAreThe.height
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnBabyGenderTouchUpInside(_ sender: Any) {
        mPickerView.isHidden = false
        mPickerView.tag = 2
        topPickerView.constant = mBtnBabyGender.y + mBtnBabyGender.height
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnUnitsTouchUpInside(_ sender: Any) {
//        mPickerView.isHidden = false
//        mPickerView.tag = 3
//        topPickerView.constant = mBtnUnits.y + mBtnUnits.height
//        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnLocationTouchUpInside(_ sender: Any) {
        mPickerView.isHidden = false
        mPickerView.tag = 4
        topPickerView.constant = mBtnLocation.y + mBtnLocation.height
        mPickerView.reloadAllComponents()
    }
    
    @IBAction func mBtnDoneTouchUpInside(_ sender: Any) {
        
        if let name = mTextFieldName.text {
            let firstName = name.trimmingCharacters(in: .whitespacesAndNewlines)
            if (firstName.count > 0 ) {
                dataUser.firstName = firstName
                dataUser.youAreThe = mBtnYouAreThe.title(for: .normal)!
                dataUser.location = mBtnLocation.title(for: .normal)!
                
                switch mBtnBabyGender.title(for: .normal) {
                case "Gái" :
                    checkGender = 2
                case "Sinh đôi" :
                    checkGender = 3
                case "Chưa xác định" :
                    checkGender = 4
                default:
                    checkGender = 1
                }

                if mBtnUnits.currentTitle == "kg/cm" {
                    dataUser.weightUnit = 1
                    dataUser.lengthUnit = false
                }
                else {
                    dataUser.weightUnit = 2
                    dataUser.lengthUnit = true
                }
                
                dataUser.dataGender = checkGender
                self.performSegue(withIdentifier: "setting_create_account_to_due_date_calculate", sender: nil)
                return
            }
        }
        self.alert("Vui lòng nhập tên bạn")
        mTextFieldName.text = ""
    }

//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        return touch.view == gestureRecognizer.view
//    }
    
    @IBAction func didTapOutSide(_ sender: Any) {
        mTextFieldName.resignFirstResponder()

    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = super.gestureRecognizer(gestureRecognizer, shouldReceive: touch)
        if let view = touch.view {
            if !view.isFirstResponder {
                self.view.endEditing(true)
            }
            if (view.isKind(of: UITextField.self)) {
                mPickerView.isHidden = true
            }
            
        }
        if(should) {
            mPickerView.isHidden = true
        }
        return should
    }
}

extension SettingCreatAccountViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch mPickerView.tag {
        case 1:
            return dataPickerYouAreThe.count
        case 2:
            return dataPickerBabyGender.count
        case 3:
            return dataPickerUnits.count
        default:
            return dataPickerLocation.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch mPickerView.tag {
        case 1:
            if let selected = self.mBtnYouAreThe.title(for: .normal) {
                if selected == dataPickerYouAreThe[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return dataPickerYouAreThe[row]
        case 2:
            if let selected = self.mBtnBabyGender.title(for: .normal) {
                if selected == dataPickerBabyGender[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return dataPickerBabyGender[row]
        case 3:
            return dataPickerUnits[row]
        default:
            if let selected = self.mBtnLocation.title(for: .normal) {
                if selected == dataPickerLocation[row] && pickerView.selectedRow(inComponent: component) != row {
                    pickerView.selectRow(row, inComponent: component, animated: false)
                }
            }
            return dataPickerLocation[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch mPickerView.tag {
        case 1:
            mBtnYouAreThe.setTitle(dataPickerYouAreThe[row], for: .normal)
            mPickerView.isHidden = true
            break
        case 2:
            mBtnBabyGender.setTitle(dataPickerBabyGender[row], for: .normal)
            mPickerView.isHidden = true
            break
        case 3:
            mBtnUnits.setTitle(dataPickerUnits[row], for: .normal)
            mPickerView.isHidden = true
            break
        default:
            mBtnLocation.setTitle(dataPickerLocation[row], for: .normal)
            mPickerView.isHidden = true
            break
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

extension SettingCreatAccountViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 15
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
