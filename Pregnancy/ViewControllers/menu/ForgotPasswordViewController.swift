//
//  ForgotPasswordViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/21/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import RealmSwift
import FirebaseAuth
import SwiftyJSON

enum AuthTypePassword : Int {
    case ForgotPassword = 0
    case SignUp
    
}

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var mLblPhoneNumber: UILabel!
    @IBOutlet weak var mTextFieldPhoneNumber: UITextField!
    @IBOutlet weak var mSubLblPhoneNumber: UILabel!
    @IBOutlet weak var btnContinueTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPhoneNumberTopConstraint: NSLayoutConstraint!

    var mIsForgotPassWord : Bool = false
    var authTypePassword : AuthTypePassword = .ForgotPassword
    
    // tạo một biến mới
    
    var ForgotPasswordnew: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Quên mật khẩu")
        tapTextFieldPhoneNumber()
        mLblPhoneNumber.isHidden = true
        mSubLblPhoneNumber.isHidden = true

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        self.mLblPhoneNumber.isHidden = true
        self.mTextFieldPhoneNumber.placeholder = "Nhập số di động"
    }

    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func tapTextFieldPhoneNumber() {
        mTextFieldPhoneNumber.addTarget(self, action: #selector(tapTextField), for: .editingDidBegin)
    }
    @objc func tapTextField(){
        self.mLblPhoneNumber.isHidden = false
        self.mSubLblPhoneNumber.isHidden = true
        self.mTextFieldPhoneNumber.placeholder = ""
    }
    
    @IBAction func mBtn_AddPhoneNumber(_ sender: Any) {
        if mTextFieldPhoneNumber.text == "" {
            mSubLblPhoneNumber.isHidden = false
        } else {
            let phone = mTextFieldPhoneNumber.text
            let numberOfForgot = UserDefaults.standard.integer(forKey: phone!)
            if numberOfForgot > 0 {
                if numberOfForgot <= 3 {
                    UserDefaults.standard.set(numberOfForgot + 1, forKey: phone!)
                    UserDefaults.standard.synchronize()
                    self.signup()
                } else {
                    if let timeInThirdForgot = UserDefaults.standard.string(forKey: "time-\(phone)") {
                        let dateFormater = DateFormatter()
                        dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                        let differenceTime = Calendar.current.dateComponents([.second], from: dateFormater.date(from: timeInThirdForgot) ?? Date(), to: Date()).second!
                        
                        if differenceTime >= 300 {
                            UserDefaults.standard.removeObject(forKey: "time-\(phone)")
                            UserDefaults.standard.removeObject(forKey: phone!)
                            self.signup()
                        } else {
                            alert("Bạn đã quên mật khẩu quá 3 lần, quay lại sau 5  ")
                        }
                        
                    } else {
                        UserDefaults.standard.set("\(Date())", forKey: "time-\(phone)")
                        UserDefaults.standard.synchronize()
                        alert("Bạn đã quên mật khẩu quá 3 lần, quay lại sau 5 phút")
                    }
                }
            } else {
                UserDefaults.standard.set(1, forKey: phone!)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func signup() {
        
        if let phone = mTextFieldPhoneNumber.text {
            UserDefaults.standard.set(nil, forKey: "authVerificationID")
            UserDefaults.standard.synchronize()
            
            if let vnphone = phone.convertVietnamPhoneNumer() {
                self.showHud()
                NetworkManager.shareInstance.apiCheckPhoneNumber(phone: vnphone) { (data, message, isSuccess) in
                    if(isSuccess ) {
                        Auth.auth().useAppLanguage()
                        PhoneAuthProvider.provider().verifyPhoneNumber(vnphone.convertVietnamPhoneNumer(true)!, uiDelegate: self) { (verificationID, error) in
                            if let error = error {
                                self.alert(error.localizedDescription)
                                self.hideHud()
                                return
                            }
                            // Sign in using the verificationID and the code sent to the user
                            // ...
                            
                            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                            UserDefaults.standard.synchronize()
                            dataUser.isSignup = false
                            dataUser.phone = self.mTextFieldPhoneNumber.text!.convertVietnamPhoneNumer()!
                            self.performSegue(withIdentifier: "ForgotPassWordToOTPViewController", sender: nil)
                            self.hideHud()
                            let alert = UIAlertController(title: "Thông báo", message: "Đã gửi SMS thành công", preferredStyle: .alert)
                            self.present(alert, animated: true, completion: nil)
                            
                            // change to desired number of seconds (in this case 5 seconds)
                            let when = DispatchTime.now() + 2
                            DispatchQueue.main.asyncAfter(deadline: when){
                                // your code with delay
                                alert.dismiss(animated: true, completion: nil)
                            }
                        }
                    } else {
                        self.hideHud()
                        self.alert("Số điện thoại của bạn chưa đăng ký")
                    }
                }
            } else {
                self.alert("Vui lòng nhập đúng số điện thoại")
            }
        } else {
            self.alert("Vui lòng nhập số điện thoại")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if(segue.identifier == "ForgotPassWordToOTPViewController") {
            if let vc = segue.destination as? OTPViewController {
                if(authTypePassword == .ForgotPassword) {
                    vc.mIsForgotPassWord = true
                }
                
            }
        }
    }
}

//extension ForgotPasswordViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        lblPhoneNumberTopConstraint.constant = 0
//        btnContinueTopConstraint.constant = 10.0
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        lblPhoneNumberTopConstraint.constant = 80.0
//        btnContinueTopConstraint.constant = 32.0
//    }
//}
extension ForgotPasswordViewController: AuthUIDelegate {
    
}
