//
//  TermOfUseViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/24/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class TermOfUseViewController: BaseViewController {

    @IBOutlet weak var mWebViewTermOfUse: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle(title: "Điều khoản của chúng tôi")
        
        let htmlPath = Bundle.main.path(forResource: "dieukhoansudung", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebViewTermOfUse.loadRequest(req)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    func setNavigationTitle() {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
//        lbl.text = "MomTour"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(18.0)
        navigationItem.titleView = lbl
    }
    
    override func doDefaultBack(_ sender: Any?) {
        super.doDefaultBack(sender)
        self.navigationController?.isNavigationBarHidden = true
    }

}
