//
//  MenuTellFriendViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 1/9/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit
import FBSDKCoreKit
import SnapKit
import MessageUI
import SDWebImage


class MenuTellFriendViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var mTellFriendView: UIView!
    @IBOutlet weak var mViewMessage: UIView!
    @IBOutlet weak var mAvatar: UIImageView!
    @IBOutlet weak var mTextViewMessage: UITextView!
    @IBOutlet weak var mBtnShareFb: UIButton!
    @IBOutlet weak var mViewShareFb: UIView!
    @IBOutlet weak var mViewShareGooglePlus: UIView!
    @IBOutlet weak var mShareImageBtn: UIButton!
    
    
    let aboveView:UIView = UIView(frame: UIScreen.main.bounds)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.removeSubsView()
        self.view.alpha = 0

        // Do any additional setup after loading the view.
        self.mTellFriendView.layer.cornerRadius = 5
        self.mViewMessage.layer.cornerRadius = 9
        self.mAvatar.layer.cornerRadius = self.mAvatar.frame.width / 2
        self.mAvatar.layer.masksToBounds = true
//        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
//        let IconMomTour = UIImage(named: "AppIcon");
//        mAvatar.image = IconMomTour;
        self.setupAvatart()
        
//        self.mBtnShareFb.isHidden = true
        self.mViewShareGooglePlus.isHidden = false
        let content : FBSDKShareLinkContent = FBSDKShareLinkContent()
        content.contentURL = (NSURL(string: "https://www.google.com.vn/")! as URL)
        let shareButton: FBSDKShareButton = FBSDKShareButton()
        shareButton.shareContent = content

        shareButton.frame = CGRect(x: 6, y: 0, width: 50, height: 50)
        let imageBtn = UIImage(named: "facebook-icon")
        shareButton.setBackgroundImage(imageBtn, for: .normal)
        shareButton.imageView?.removeFromSuperview()
        shareButton.setTitle("", for: .normal)
        shareButton.addTarget(self, action: #selector(mBtnShareFbTouchUpInside), for: .touchUpInside)
        self.mViewShareFb.addSubview(shareButton)
        
        // mShareImageBtn share mail
        self.mShareImageBtn.addTarget(self, action: #selector(shareEmail), for: .touchUpInside)
        
        self.showAnimate()
        self.setupViewAboveTellFriend()
    }
    
    @objc func postFacebook(sender: UIButton){

    }
    
    func setupAvatart() {
        DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTemp, isSuccess) in
            if let user = user {
                var avatar = NetworkManager.rootDomain
                if user.avatar.hasPrefix("http") {
                    avatar = user.avatar
                } else {
                    avatar.append(user.avatar)
                }
                self.mAvatar.sd_setImage(with: URL(string:avatar), completed: {(image, error, cacheType, url) in
                    if let image = image {
                        self.mAvatar.image = image
                    } else {
                        self.mAvatar.image = UIImage(named: "user_placeholder")
                    }
                })
            } else {
                self.mAvatar.image = UIImage(named: "user_placeholder")
            }
        })
    }
    
    @IBAction func PopupCancel(_ sender: Any) {
//        dismiss(animated: true)
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    @IBAction func FocusTextView(_ sender: Any) {
        self.mTextViewMessage.isEditable = true
        self.mTextViewMessage.becomeFirstResponder()
    }

    @objc func mBtnShareFbTouchUpInside() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    // setup View above navigation and tabbar
    func setupViewAboveTellFriend() {
        if let applicationDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate? {
            if let window:UIWindow = applicationDelegate.window {

                self.aboveView.backgroundColor = UIColor.clear.withAlphaComponent(0)

                aboveView.addSubview(mTellFriendView)
                aboveView.sendSubviewToBack(mTellFriendView)
                mTellFriendView.snp.makeConstraints { (maker) in
                    maker.top.equalToSuperview().offset(120)
                    maker.bottom.equalToSuperview().offset(-120)
                    maker.leading.equalToSuperview().offset(16)
                    maker.trailing.equalToSuperview().offset(-16)
                }

                window.addSubview(aboveView)
            }
        }
    }
    
    //Mark : Create Animated view
    func showAnimate()
    {
        self.aboveView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        self.aboveView.alpha = 0.5
        UIView.animate(withDuration: 0.2, animations: {
            self.aboveView.alpha = 1.0
            self.aboveView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.aboveView.transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
            self.aboveView.alpha = 0.5
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.aboveView.removeFromSuperview()
                
            }
        });
    }
    
    
    // share email
    @objc func shareEmail() {
        self.removeAnimate()
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
//        mailComposerVC.setToRecipients(["info@medlatec.com"])
        mailComposerVC.setSubject("Đánh giá về ứng đụng của chúng tôi")
        mailComposerVC.setMessageBody(mTextViewMessage.text, isHTML: false)
        
        return mailComposerVC
    }
    
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Không thể gửi Email", message: "Thiết bị của bạn không thể gửi được Email. Vui lòng thử lại sau.\n Xin cảm ơn", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
