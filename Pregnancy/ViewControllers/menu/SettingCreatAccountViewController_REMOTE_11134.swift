//
//  SettingCreatAccountViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/22/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SettingDueDateViewController: BaseViewController {

    @IBOutlet weak var btnDueDate: UIButton!
    @IBOutlet weak var btnDueDateLbl: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textAttr : [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font : UIFont.italicSystemFont(ofSize: 17),
            NSAttributedString.Key.foregroundColor : UIColor.black,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
        btnDueDateLbl.setAttributedTitle(NSMutableAttributedString(string: "phân tích",
                                                                 attributes: textAttr), for: .normal)
        
        let image = UIImage(named:"Acc5")
        btnDueDate.setImage(image, for: .normal)
        btnDueDate.imageView?.contentMode = .scaleAspectFit
        btnDueDate.contentHorizontalAlignment = .left
        btnDueDate.imageEdgeInsets = UIEdgeInsets(top:0, left:btnDueDate.frame.width - 40, bottom:0, right:0)
        btnDueDate.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
    @IBAction func mBtnDueDateTouchUpInside(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
    }
    
    @IBAction func mBtnAgreeTouchUpInside(_ sender: Any) {
        if let currentViewController = ViewManager.getCurrentViewController() {
            if let navigationController = currentViewController.navigationController {
                navigationController.popToRootViewController(animated: true)
            }
        }
    }
}

extension SettingDueDateViewController: PickerDateViewDelegate {
    func didSelectedDateString(dateString: String) {
        btnDueDate.setTitle(dateString, for: .normal)
    }
}
