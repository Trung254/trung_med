//
//  NoticeDueDateViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class NoticeDueDateViewController: BaseViewController {

    @IBOutlet weak var mWebViewNoticeDueDate: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Thông tin bảo mật")
        
        let htmlPath = Bundle.main.path(forResource: "test", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebViewNoticeDueDate.loadRequest(req)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }

}
