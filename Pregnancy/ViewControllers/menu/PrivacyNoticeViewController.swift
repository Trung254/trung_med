//
//  PrivacyNoticeViewController.swift
//  Pregnancy
//
//  Created by dady on 2/18/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class PrivacyNoticeViewController: BaseViewController, UIWebViewDelegate {
    
    @IBOutlet weak var mWebView: UIWebView!
    
    var mNavigationHidden: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationTitle()
        self.getWebViewContent()
        mNavigationHidden = self.navigationController?.navigationBar.isHidden ?? true
        self.mIsShowRightButton = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func getWebViewContent(){
        let htmlPath = Bundle.main.path(forResource: "privacy", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebView.loadRequest(req)
        mWebView.scrollView.isScrollEnabled = false
    }
    
    func setNavigationTitle() {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = "Quyền riêng tư"
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(18.0)
        navigationItem.titleView = lbl
    }

    override func doDefaultBack(_ sender: Any?) {
        super.doDefaultBack(sender)
        self.navigationController?.setNavigationBarHidden(mNavigationHidden, animated: true)
    }
}
