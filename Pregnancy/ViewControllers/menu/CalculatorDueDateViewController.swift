//
//  CaculatorDueDateViewController.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 1/22/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SnapKit
import RealmSwift

protocol delegateCaculatorDueDate: class {
    func dateCalculatorSend(with data: String)
    func switchWasBorn(with data: Bool)
}

class CalculatorDueDateViewController: BaseViewController, PickerDateViewDelegate {
    func didSelectedDateStringForCalculator(dateString: String) {
        
    }
    
    
    @IBOutlet weak var mBtnSetCalculator: UIButton!
    @IBOutlet weak var viewPeriod: UIView!
    @IBOutlet weak var viewConception: UIView!
    @IBOutlet weak var lbl1stDay: UILabel!
    @IBOutlet weak var lblConception: UILabel!
    @IBOutlet weak var lblEndCaculator: UILabel!
    
    @IBOutlet weak var mBtnCancel: UIButton!
    @IBOutlet weak var mBtnSave: UIButton!
    @IBOutlet weak var bottomConstrainBtnCancel: NSLayoutConstraint!
    @IBOutlet weak var bottomConstrainBtnSave: NSLayoutConstraint!
    var dateGet: String = ""
    var tagCheck = 0
    
    var periodDate = Date()
    var conceptionDate = Date()
    var dueDate = Date()
    
    var datePushFromSetting = ""
    var mSettingCreateAccount: SettingDueDateViewController!
    
    
    var authType: AuthType = .SignUp
    weak var delegate: delegateCaculatorDueDate?
    var dateFromString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Tính ngày dự sinh")
        
        self.showMessageIfLogin()
        self.mIsShowRightButton = false
        
        let lastPeriod = UITapGestureRecognizer(target: self, action: #selector(LastPeriodTapped))
        self.viewPeriod.addGestureRecognizer(lastPeriod)
        
        
        let conception = UITapGestureRecognizer(target: self, action: #selector(ConceptionTapped))
        self.viewConception.addGestureRecognizer(conception)
        
//        let realm = try! Realm()
//        try! realm.write {
//            let order = DatabaseManager.sharedManager.getLocalUserInfo()
//            
//            print(order)
//        }
        
    }
    // func xu li view help calculator khi dang login va da login
    func showMessageIfLogin() {
        guard DatabaseManager.sharedManager.isLogin() != false else {
            print("dki lan dau")
            self.mBtnCancel.isHidden = true
            self.mBtnSave.isHidden = true
            //self.mBtnSetCalculator.isHidden = false
            return
        }
        guard DatabaseManager.sharedManager.isLogin() != true else {
            print("da login xong")
            self.mBtnSetCalculator.isHidden = true
            self.mBtnSave.isHidden = false
            self.mBtnCancel.isHidden = false
            self.bottomConstrainBtnCancel.constant = 20
            self.bottomConstrainBtnSave.constant = 20
            return
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if datePushFromSetting == "" {
            self.periodDate = Date()
            self.lbl1stDay.text = self.dateToString(date: periodDate)
            self.setConceptionDate(date: periodDate)
            self.caculatorEndCalDate(date: periodDate)
            
        } else {
            self.lbl1stDay.text = datePushFromSetting
            self.periodDate = self.stringToDate(dateString: datePushFromSetting)
            self.setConceptionDate(date: periodDate)
            self.caculatorEndCalDate(date: periodDate)
        }
        
    }
    
    
    @objc func LastPeriodTapped(){
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
        self.tagCheck = 1
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.maximumDate = Date()
        pickerDate.datePicker.minimumDate = Date().addingTimeInterval(-(280*24*60*60))
        pickerDate.datePicker.date = self.stringToDate(dateString: self.lbl1stDay.text!)
        
    }
    
    @objc func ConceptionTapped() {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.maximumDate = Date().addingTimeInterval(14*24*60*60)
        pickerDate.datePicker.minimumDate = Date().addingTimeInterval(-(38*7*24*60*60))
        self.tagCheck = 2
        pickerDate.datePicker.date = self.stringToDate(dateString: self.lblConception.text!)
    }
    
    func didSelectedDateString(dateString: String) {
        if tagCheck == 1 {
            self.lbl1stDay.text = dateString
            periodDate = self.stringToDate(dateString: dateString)
            setConceptionDate(date: periodDate)
            
        } else {
            self.lblConception.text = dateString
            conceptionDate = self.stringToDate(dateString: dateString)
            setPeriodDate(date: conceptionDate)
        }
    }
    
    
    
    @IBAction func btnCancelTouchUpInside(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveTouchUpInside(_ sender: Any) {
        dataUser.dueDate = self.lblEndCaculator.text ?? "\(Date())"
        
        delegate?.dateCalculatorSend(with: self.lblEndCaculator.text ?? "\(Date())")
        delegate?.switchWasBorn(with: true)
        let realm = try! Realm()
        try! realm.write {
            var user = DatabaseManager.sharedManager.getLocalPregnancys()
            user?.baby_already_born = 0
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from:dataUser.dueDate)!
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day], from: date)
            let finalDate = calendar.date(from:components)
            user?.due_date = finalDate!
            realm.add(user!)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tappedSetCalculator(_ sender: Any) {
        self.mSettingCreateAccount.dateGetFromDueDate = self.lblEndCaculator.text
        self.mSettingCreateAccount.periodSaveFomCalDueDate = self.lbl1stDay.text!
        
        self.navigationController?.popViewController(animated: true)
        
        dataUser.dueDate = lblEndCaculator.text!
        let startDate = self.caculatorStartDate(lblEndCaculator.text!)
        dataUser.startDate = self.dateToString(date: startDate)
    }
    
    
    func doneTapped(_ sender: PickerDateView) {
        //        print("tapped done button calculator")
        self.caculatorEndCalDate(date: periodDate)
    }
    
    override func isHiddenTabBar() -> Bool {
        return true
    }
    
}

extension CalculatorDueDateViewController {
    
    func stringToDate(dateString : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.date(from: dateString)!
    }
    
    func dateToString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //        dateFormatter.locale = Locale(identifier: "vi_Vi")
        return dateFormatter.string(from: date)
    }
}

extension CalculatorDueDateViewController {
    
    func setConceptionDate(date: Date) {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        conceptionDate = (calendar?.date(byAdding: .day, value: 14, to: date, options: .init(rawValue: 0)))!
        self.lblConception.text = self.dateToString(date: conceptionDate)
    }
    
    func setPeriodDate(date: Date) {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        periodDate = (calendar?.date(byAdding: .day, value: -14, to: date, options: .init(rawValue: 0)))!
        self.lbl1stDay.text = self.dateToString(date: periodDate)
    }
    
    func caculatorEndCalDate(date : Date) {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let endDate = Calendar.current.date(byAdding: .day, value: 280, to: date)
        //            calendar!.component(.month, from: date)
        //        if (4 <= periodMonth) && (periodMonth <= 12) {
        //            let time1 = (calendar?.date(byAdding: .day, value: 7, to: date, options: .init(rawValue: 0)))!
        //            let time2 = (calendar?.date(byAdding: .month, value: -3, to: time1, options: .init(rawValue: 0)))!
        //            let time3 = (calendar?.date(byAdding: .year, value: 1, to: time2, options: .init(rawValue: 0)))!
        //            self.lblEndCaculator.text = self.dateToString(date: time3)
        //        } else {
        //            let time1 = (calendar?.date(byAdding: .day, value: 7, to: date, options: .init(rawValue: 0)))!
        //            let time2 = (calendar?.date(byAdding: .month, value: 9, to: time1, options: .init(rawValue: 0)))!
        //            self.lblEndCaculator.text = self.dateToString(date: time2)
        //        }
        
        self.lblEndCaculator.text = self.dateToString(date: endDate!)
    }
}
