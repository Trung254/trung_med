//
//  PremiumUpgradeTableViewCell.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 07/01/2019.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class PremiumUpgradeTableViewCell: UITableViewCell {

    @IBOutlet weak var mImagePremium: UIImageView!
    @IBOutlet weak var mLblPremium: UILabel!
    @IBOutlet weak var mSubLblPremium: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        mImagePremium.layer.cornerRadius = mImagePremium.frame.size.width / 2
        mImagePremium.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
