
//
//  BabyFavoriteNameViewController.swift
//  Pregnancy
//
//  Created by dady on 1/16/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class titleGirlsTableCell: UITableViewCell {
    @IBOutlet weak var shareGirlsName: UIButton!
    
}
class listGirlsTableCell: UITableViewCell {
    @IBOutlet weak var mGirlName: UILabel!
}
class titleBoysTableCell: UITableViewCell {
    @IBOutlet weak var shareBoysName: UIButton!
    
}
class listBoysTableCell: UITableViewCell {
    @IBOutlet weak var mBoyName: UILabel!
}

class BabyFavoriteNameViewController: BaseViewController {
    @IBOutlet weak var mtblFavoriteName: UITableView!
    @IBOutlet weak var mViewFavoriteName: UIView!
    var babynameboys: [JSON] = []
    var babynamegirls: [JSON] = []
    var userId = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addBackgroundImage("more-background-blur")
        self.mtblFavoriteName.sectionHeaderHeight = 60
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
        self.setColorNavigation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.mtblFavoriteName.tableFooterView = UIView()
        self.loadData()
    }
    
    private func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllBabyName(countryId: 1){ (data, message, isSuccess) in
            if let data = data as? JSON {
                self.babynameboys.removeAll()
                self.babynamegirls.removeAll()
                
                DispatchQueue.main.async {
                    self.mtblFavoriteName.reloadData()
                }
                
                for item in data.arrayValue {
                    if item["user_id"].stringValue == "\(self.userId)" {
                        if item["gender_id"].intValue == 1 {
                            self.babynameboys.append(item)
                        }
                        
                        else {
                            self.babynamegirls.append(item)
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mtblFavoriteName.reloadData()
                }
            }
        }
    }
    
    @objc func shareGirlsname(_ sender : UIButton){
        var shareText = "Tên con gái \n"
        if babynamegirls.isEmpty == false {
            for item in babynamegirls {
                shareText += "- \(item["name"].stringValue)\n"
            }
        }
    
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.setValue("Danh sách tên yêu thích của bạn", forKey: "subject")
        present(vc, animated: true, completion: nil)
    }
    
    @objc func shareBoysname(_ sender : UIButton){
        var shareText = "Tên con trai \n"
        if babynameboys.isEmpty == false {
            for item in babynameboys {
                shareText += "- \(item["name"].stringValue)\n"
            }
        }
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
}

extension BabyFavoriteNameViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return babynamegirls.count
        }
        else {
            return babynameboys.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "defaultCell")
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "listGirlsTableCell", for: indexPath) as! listGirlsTableCell
            (cell as! listGirlsTableCell).mGirlName.text = self.babynamegirls[indexPath.row ]["name"].stringValue
            (cell as! listGirlsTableCell).mGirlName.textColor = #colorLiteral(red: 0.9960784314, green: 0.5529411765, blue: 0.7254901961, alpha: 1)
            
            if babynamegirls.count == 0 {
                tableView.tableHeaderView = nil
            }
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "listBoysTableCell", for: indexPath) as! listBoysTableCell
            (cell as! listBoysTableCell).mBoyName.text = self.babynameboys[indexPath.row]["name"].stringValue
            (cell as! listBoysTableCell).mBoyName.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        }
        
        cell.selectionStyle = . none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "titleGirlsTableCell") as! titleGirlsTableCell
            headerCell.shareGirlsName.removeTarget(nil, action: nil, for: .allEvents)
            headerCell.shareGirlsName.addTarget(self, action: #selector(self.shareGirlsname(_:)), for: .touchUpInside)
            headerCell.backgroundColor = UIColor.white
            return headerCell
        }
        else {
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "titleBoysTableCell") as! titleBoysTableCell
            headerCell.shareBoysName.removeTarget(nil, action: nil, for: .allEvents)
            headerCell.shareBoysName.addTarget(self, action: #selector(self.shareBoysname(_:)), for: .touchUpInside)
            headerCell.backgroundColor = UIColor.white
            return headerCell
        }
    }

}

extension BabyFavoriteNameViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            var babynameId:Int?
            var custombabynameId:String?
            var section = indexPath.section
            
            if indexPath.section == 0 {
                babynameId =  self.babynamegirls[indexPath.row]["id"].intValue
                custombabynameId = self.babynamegirls[indexPath.row]["custom_baby_name_by_user_id"].stringValue
            }
            else {
                babynameId =  self.babynameboys[indexPath.row]["id"].intValue
                custombabynameId = self.babynameboys[indexPath.row]["custom_baby_name_by_user_id"].stringValue
            }
            
            if custombabynameId == "" {
                NetworkManager.shareInstance.apiDeleteUserBabyNamne(babyNameId: babynameId!)
                { (data, messge, isSuccess) in
                    if (isSuccess) {
                        switch section {
                        case 0:
                            self.babynamegirls.remove(at: indexPath.row)
                            break
                        case 1:
                            self.babynameboys.remove(at: indexPath.row)
                            break
                        default:
                            break
                        }
                        self.mtblFavoriteName.reloadData()
                    } else {
                        self.alert("Xóa tên em bé không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                            
                        })
                    }
                }
            }
            else {
                NetworkManager.shareInstance.apiDeleteUserBabyNamne(babyNameId: babynameId!)
                {
                    (data, messge, isSuccess) in
                    if isSuccess {
                        NetworkManager.shareInstance.apiDeleteBabyNamne(babyNameId: babynameId!)
                        { (data, message, isSuccess) in
                            if (isSuccess) {
                                switch section {
                                case 0:
                                    self.babynamegirls.remove(at: indexPath.row)
                                    break
                                case 1:
                                    self.babynameboys.remove(at: indexPath.row)
                                    break
                                default:
                                    break
                                }
                                self.mtblFavoriteName.reloadData()
                            } else {
                                self.alert("Xóa tên em bé không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                                    
                                })
                            }
                        }
                    } else {
                        self.alert("Xóa tên em bé không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                            
                        })
                    }
                }
            }
            
//            self.loadData()
//            tableView.reloadData()
        }
    }
}

