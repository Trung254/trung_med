//
//  FormPackageViewController.swift
//  Pregnancy
//
//  Created by Trung Duc on 3/3/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import WebKit
import RealmSwift

struct detailPackage {
    var id : Int
    var name: String
    var url_image: String
    var price_package: String
    var city: String
    
    init(id: Int, name: String, url_image: String, price_package: String, city: String  ) {
        self.id = id
        self.name = name
        self.url_image = url_image
        self.price_package = price_package
        self.city = city
    }
}

struct packageSchedule {
    var id : Int
    var time: String
    var weight_adjustment: Float
    var active: Int
    
    init(id: Int, time: String, weight_adjustment: Float, active: Int) {
        self.id = id
        self.time = time
        self.weight_adjustment = weight_adjustment
        self.active = active
    }
}

class TimeColllectionCell: UICollectionViewCell {
    @IBOutlet weak var timeButton: customButton!
    
}

class FormPackageViewController: BaseViewController , PickerDateViewDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var mImgPackage: UIImageView!
    @IBOutlet weak var mNamePackage: UILabel!
    @IBOutlet weak var mPrice: UILabel!
    @IBOutlet weak var mCity: UILabel!
    @IBOutlet weak var mIDDoctors: SkyFloatingLabelTextField!
    @IBOutlet weak var mUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var mPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var mAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var mEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var mbtnTypeAddress: UIButton!
    @IBOutlet weak var mbtnDate: UIButton!
    @IBOutlet weak var mbtnTime: UIButton!
    @IBOutlet weak var mbtnAddress: customButton!
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var mHeightScroll: NSLayoutConstraint!
    @IBOutlet weak var mHeightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var mViewTop: UIView!
//    @IBOutlet weak var mTimeScheduleLbl: UILabel!
    @IBOutlet weak var mPickerView: UIPickerView!
    @IBOutlet weak var mDatelbl: UILabel!
    @IBOutlet weak var mTimelbl: UILabel!
    @IBOutlet weak var mScrollView: UIScrollView!
    
    private var currentTextField:UITextField?
//    private var currentTextView:UITextView?
    
    var dataManufacture: [JSON] = []
    var dataTimeSchedule = [packageSchedule]()
    var dataAttributes : [JSON] = []
    var atributesUpdate = Array<Dictionary<String, Any>>()
    var dataPackage = detailPackage(id: 0, name: "", url_image: "", price_package: "", city: "")
    var addressAppointment: [JSON] = []
    
    let dateFormatter = DateFormatter()
    var products_id: Int = 0
    var nopcustomer_id:Int = 0
    var last_name:String = ""
    var location:String = ""
    var time_schedule_active: Int = 0
    var order_id:Int = 0
    var vnpay: VNPay = VNPay()
    var webView = WKWebView()
    var tag_button: Int = 0
    var curDate: String = ""
    var appointDate: String = ""
    var indexAppointment: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.addBoxShadow(uiview: mViewTop)
        self.addDelegateTextFiled()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.curDate = dateFormatter.string(from: Date())
        self.mbtnDate.setTitle("--Chọn ngày--", for: .normal)
        
        if dataPackage.url_image.isEmpty == false {
            let urlStr = dataPackage.url_image
            let url = URL(string: urlStr)
            self.mImgPackage.sd_setImage(with: url, completed: nil)
        }
        self.mNamePackage.text = dataPackage.name
        self.mPrice.text = "\(dataPackage.price_package) đ"
        
        self.mCity.font = UIFont.fontAwesome(ofSize: 13.0)
        self.mCity.text = String.fontAwesomeIcon(name: .mapMarker) + " " + dataPackage.city
        self.products_id = dataPackage.id
        
        self.vnpay.delegate = self
        let config = WKWebViewConfiguration()
        let contentController = WKUserContentController()
        let scriptSource = "document.body.style.backgroundColor = `red`;"
        let script = WKUserScript(source: scriptSource, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        contentController.addUserScript(script)
        config.userContentController = contentController
        let webView = WKWebView(frame: .zero, configuration: config)
        
        self.registerForNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DatabaseManager.sharedManager.getUserInfo { (pregUser, mnessage, isSuccess) in
            if pregUser != nil {
                self.mUserName.text = pregUser?.first_name
                self.mPhoneNumber.text = pregUser?.phone
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.nopcustomer_id = data.arrayValue[0]["nopcustomer_id"].intValue
                
                if data.arrayValue[0]["last_name"].stringValue != "" {
                    self.last_name = data.arrayValue[0]["last_name"].stringValue
                }
                else {
                    self.last_name = data.arrayValue[0]["first_name"].stringValue
                }
                
                if data.arrayValue[0]["location"].stringValue != "" {
                    self.location = data.arrayValue[0]["location"].stringValue
                }
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetProducts(id_product: self.products_id) { (data, message, isSuccess) in
            self.dataTimeSchedule.removeAll()
            
            if isSuccess {
                self.dataAttributes = (JSON(data))["products"][0]["attributes"].arrayValue
                for item in self.dataAttributes {
                    if item["product_attribute_name"] == "Khung giờ" {
                        for i in item["attribute_values"].arrayValue {
                            let id = i["id"].intValue
                            let value = i["name"].stringValue
                            let weight_adjustment = i["weight_adjustment"].floatValue
                            
                            self.dataTimeSchedule.append(packageSchedule(id: id, time: value, weight_adjustment: weight_adjustment, active: 1))
                        }
                    }
                    
                    if item["product_attribute_name"] == "Nơi khám" {
                        self.addressAppointment = item["attribute_values"].arrayValue
                        self.mbtnAddress.setTitle(self.addressAppointment[0]["name"].stringValue, for: .normal)
                    }
                }
                
                self.mbtnTime.setTitle(self.dataTimeSchedule[0].time, for: .normal)
                self.mHeightCollectionView.constant = 0
//                let screenWidth = Float(UIScreen.main.bounds.width) - 64
//                let num_time_row = Int(screenWidth / 110)
//                let num_time = Double(self.dataTimeSchedule.count)
//                self.mHeightCollectionView.constant = CGFloat(round(num_time / Double(num_time_row)) * 50 + 20)
//                self.mHeightScroll.constant = CGFloat(Float(self.mHeightCollectionView.constant)  + 784.5)
                
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mPickerView.reloadAllComponents()
                }
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    func addDelegateTextFiled() {
        self.mIDDoctors.delegate = self
        self.mUserName.delegate = self
        self.mPhoneNumber.delegate = self
        self.mAddress.delegate = self
        self.mEmail.delegate = self
    }
    
    
    @IBAction func chooseDateAppointment(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.view.addSubview(pickerDate)
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.minimumDate = Date()
    }
    
    @IBAction func PopupCancel(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showPickerview(_ sender: UIButton) {
        self.tag_button = sender.tag
        self.mPickerView.reloadAllComponents()
        self.mPickerView.isHidden = false
    }
    
    @IBAction func CreateOrder(_ sender: Any) {
        
        let customer_name = self.mUserName.text!
        let customer_phone = self.mPhoneNumber.text!
        let customer_address = self.mAddress.text!
        let customer_email = self.mEmail.text
        let customer_id_doctor = self.mIDDoctors.text
        let customer_date = self.mbtnDate.titleLabel?.text
        let customer_schedule_time = self.mbtnTime.titleLabel?.text
        
        self.atributesUpdate.removeAll()
        
        if isValidEmail(testStr: customer_email ?? "") {
            
        }
        
        if customer_name.isEmpty == false, customer_phone.isEmpty == false, customer_address.isEmpty == false , customer_schedule_time != "hết giờ đặt", customer_date != "--Chọn ngày--", isValidEmail(testStr: customer_email ?? "") {
            
            for item in dataAttributes {

                let id =  item["id"].intValue
                    // khung gio
                if item["product_attribute_name"] == "Khung giờ" {
                    let attr : Dictionary = ["id": id, "value" : item["attribute_values"][self.indexAppointment]["id"].intValue ] as Dictionary
                    self.atributesUpdate.append(attr)
                }
            }
            
            self.showHud()
            NetworkManager.shareInstance.apiPostShoppingCartItemProduct(arrDic: self.atributesUpdate, product_id: products_id, user_id: nopcustomer_id) {
                (data, message, isSuccess) in
                if isSuccess {
                    let dateCreate = self.dateFormatter.string(from: Date())
                    
                    NetworkManager.shareInstance.apiPostOrders(city: self.location, email: customer_email!, ship_address: customer_address, first_name: customer_name, last_name: self.last_name, phone_number: customer_phone, zip_postal_code: "7546547997", bill_address: customer_address, created_on_utc: "\(Date())", customer_id: self.nopcustomer_id ?? 0, customer_gender: "", customer_birthday: "", callBack: {
                        (data, message, isSuccess) in
                        if isSuccess {

                            self.order_id = (data as! JSON)["orders"][0]["id"].intValue
                            let order_price = self.dataPackage.price_package
                            
                            let data = VNPayOrderInfo(
                                amount: "\(Int(order_price)! * 100)",
                                orderInfo: "\(self.order_id)",
                                ref: "\(self.order_id + 100000000)"
                            )
                            if !self.vnpay.vnpay(makeOrderForData: data) {
                                self.alert("Khởi tạo thanh toán thất bại")
                            }
                            DispatchQueue.main.async {
                                self.hideHud()
                            }
                        }
                        else {
                            self.alert("Vui lòng thử lại")
                            DispatchQueue.main.async {
                                self.hideHud()
                            }
                        }
                    })
                } else {
                    self.prompt("Thông báo", message: "Đã xảy ra lỗi, vui lòng liên hệ với hệ với chúng tôi để được trợ giúp", okHandler: { (alert) in
                        let activityController = UIActivityViewController(activityItems: [""], applicationActivities: nil)
                        activityController.setValue("Phản ảnh lại với chúng tôi", forKey: "subject")
                        self.present(activityController, animated: true, completion: nil)
                    }, cancelHandler: { (alert) in
                        self.hideHud()
                    })
                }
            }
            
        }else {
//            self.alert("Bạn cần nhập đủ thông tin")
            if (self.mAddress.text?.isEmpty)! {
                self.mAddress.errorMessage = "* Địa chỉ"
            }
            if (self.mUserName.text?.isEmpty)! {
                self.mUserName.errorMessage = "* Họ tên"
            }
            if (self.mPhoneNumber.text?.isEmpty)! {
                self.mPhoneNumber.errorMessage = "* Số điện thoại"
            }
            if (self.mEmail.text?.isEmpty)! {
                self.mEmail.errorMessage = "* Ngày sinh"
            }
            if customer_schedule_time == "hết giờ đặt" {
                self.mTimelbl.textColor = .red
//                self.mTimelbl.text = "    Khung giờ"
            }
            if customer_date == "--Chọn ngày--" {
                self.mDatelbl.textColor = .red
//                self.mDatelbl.text = "    Ngày khám"
            }
            
            if !isValidEmail(testStr: customer_email ?? "") {
                self.mEmail.errorMessage = "* Email không đúng"
            }
            
//            if self.dataTimeSchedule.filter({ (data) -> Bool in
//                data.active == 1
//            }).isEmpty {
//                self.mTimeScheduleLbl.textColor = .red
//                self.mTimeScheduleLbl.text = "  * Khung giờ"
//            }
            
            self.alert("Bạn cần nhập đủ thông tin")
            
        }
        let realm = try! Realm()
        try! realm.write {
            let order = DatabaseManager.sharedManager.getLocalUserInfo()
            order?.hoTen = self.mUserName.text!
            order?.noiKham = self.mAddress.text!
            order?.tenBV = self.mAddress.text!
            order?.diaChi = self.mAddress.text!
            order?.sdt = self.mPhoneNumber.text!
            order?.email = self.mEmail.text!
            order?.type = "goikham"
            
            
            realm.add(order!)
        }
        
    }
    
    @IBAction func chooseTypeAppointment(_ sender: UIButton) {
        if sender.tag == 0 {
            self.mbtnTypeAddress.setImage(UIImage(named: "tick-3"), for: .normal)
            self.mbtnTypeAddress.tag = 1
            self.mbtnAddress.isUserInteractionEnabled = false
            self.mbtnAddress.titleLabel?.textColor = UIColor.lightGray
        }
        else {
            self.mbtnTypeAddress.setImage(UIImage(named: "untick"), for: .normal)
            self.mbtnTypeAddress.tag = 0
            self.mbtnAddress.isUserInteractionEnabled = true
            self.mbtnAddress.titleLabel?.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        }
    }
    
    
    func didSelectedDateString(dateString: String) {
//        self.mDatelbl.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
        self.mbtnDate.setTitle(dateString, for: .normal)
        self.checkTimebeforeFourHours(checkDate: dateString)
    }
    
    func doneTapped(_ sender: PickerDateView) {}
    
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let should = super.gestureRecognizer(gestureRecognizer, shouldReceive: touch)
        
        if should != true, let view = touch.view {
            if !view.isFirstResponder {
                self.view.endEditing(true)
            }
        }
        
        return should
    }
    
    private func checkTimebeforeFourHours(checkDate: String) {
        if curDate == checkDate {
            
            for i in 0..<dataTimeSchedule.count {
                dateFormatter.dateFormat = "HH"
                let hours = dateFormatter.string(from: Date())
                dateFormatter.dateFormat = "mm"
                let minutes = dateFormatter.string(from: Date())
                
                let cur_time = Float("\(hours).\(minutes)")
                
                let weight_adjustment = dataTimeSchedule[i].weight_adjustment
                if (weight_adjustment - cur_time!) < 4 {
                    dataTimeSchedule[i].active = 0
                }
            }
        }else {
            for i in 0..<dataTimeSchedule.count {
                dataTimeSchedule[i].active = 1
            }
        }
        
        if let index = dataTimeSchedule.firstIndex(where: { $0.active == 1 }) {
            self.mbtnTime.setTitle(self.dataTimeSchedule[index].time, for: .normal)
//            self.mTimelbl.textColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
            self.mbtnTime.isUserInteractionEnabled = true
            self.indexAppointment = index
        } else {
            self.mbtnTime.setTitle("hết giờ đặt", for: .normal)
            self.mbtnTime.isUserInteractionEnabled = false
        }
    }
    
    private func cleanTextField() {
        self.mUserName.text = ""
        self.mPhoneNumber.text = ""
        self.mAddress.text = ""
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}

extension FormPackageViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if tag_button == 1 {
            var count = 0
            for item in dataTimeSchedule {
                if item.active == 1 { count += 1}
            }
            
            return count
        }else {
            return addressAppointment.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if tag_button == 1 {
            for i in 0..<dataTimeSchedule.count {
                if dataTimeSchedule[i].active == 1 {
                    return dataTimeSchedule[i + row].time
                    break
                }
            }
        }else {
            return addressAppointment[row]["name"].stringValue
        }
        
        return addressAppointment[row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tag_button == 1 {
            for i in 0..<dataTimeSchedule.count {
                if dataTimeSchedule[i].active == 1 {
                    self.mbtnTime.setTitle(dataTimeSchedule[i + row].time, for: .normal)
                    self.indexAppointment = i + row
                    break
                }
            }
        }else {
            self.mbtnAddress.setTitle(addressAppointment[row]["name"].stringValue, for: .normal)
        }
        
        self.mPickerView.isHidden = true
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}

extension FormPackageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.dataTimeSchedule.count != 0 {
            collectionView.collectionViewLayout.invalidateLayout()
            return self.dataTimeSchedule.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeRangeCell", for: indexPath) as! TimeColllectionCell
        collectionView.collectionViewLayout.invalidateLayout()
        
        if self.dataTimeSchedule.count != 0 {
            let doctorTime = self.dataTimeSchedule[indexPath.row].time
            
            if dataTimeSchedule[indexPath.row].active == 1 {
                cell.timeButton.layer.borderColor = #colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1)
                cell.timeButton.setTitleColor(#colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1), for: .normal)
                cell.timeButton.layer.borderWidth = 1.5
            }
            else {
                cell.timeButton.layer.borderWidth = 1
                cell.timeButton.layer.borderColor = UIColor.lightGray.cgColor
                cell.timeButton.setTitleColor(UIColor.darkGray, for: .normal)
            }
            
            cell.timeButton.layer.cornerRadius = 20
            cell.timeButton.layer.masksToBounds = true
            cell.timeButton.setTitle(doctorTime, for: .normal)
//            cell.timeButton.tag = self.dataTimeSchedule[indexPath.row]["id"] as! Int
            cell.timeButton.tag = indexPath.row
            cell.timeButton.addTarget(self, action: #selector(AppointmentDoctorsViewController.registerTimeSchedule(sender:)), for: UIControl.Event.touchUpInside)
        }
        
        return cell
    }
    
    @objc func registerTimeSchedule(sender: UIButton) {
        
//        self.mTimeScheduleLbl.textColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
//        self.mTimeScheduleLbl.text = "    Khung giờ"
        
        for index in 0..<dataTimeSchedule.count {
            dataTimeSchedule[index].active = 0
        }
        let tag = sender.tag
        self.time_schedule_active = self.dataTimeSchedule[tag].id
        self.dataTimeSchedule[tag].active = 1
        self.mCollectionView.reloadData()
    }
}

extension FormPackageViewController: VNPayDelegate {
    func vnpay(_ vnpay: VNPay, dismiss viewController: VNPayViewController, withTransactionResult transactionIsSuccess: Bool) {
        self.showHud()
        viewController.dismiss(animated: true) {
            var vc: VNOrderResultViewController
            if transactionIsSuccess {
                vc = self.storyboard?.instantiateViewController(withIdentifier: "VNOrderResultViewController") as! VNOrderResultViewController
            } else {
                vc = self.storyboard?.instantiateViewController(withIdentifier: "VNOrderResultViewControllerfail") as! VNOrderResultViewController
            }
            
            vc.order_id = self.order_id + 1000000
            vc.presenter = self
            self.present(vc, animated: true)
            self.hideHud()
        }
    }
    
    func vnpay(_ vnpay: VNPay, present vnPayViewController: VNPayViewController) {
        self.present(vnPayViewController) {
            
        }
    }
    
    func vnpay(_ vnpay: VNPay, present WKWebViewController: WKWebView) {
        
    }
    
    
}

extension FormPackageViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.mAddress {
            self.mAddress.errorMessage = ""
        }
        if textField == self.mUserName {
            self.mUserName.errorMessage = ""
        }
        if textField == self.mPhoneNumber {
            self.mPhoneNumber.errorMessage = ""
        }
        if textField == self.mEmail {
            self.mEmail.errorMessage = ""
        }
        currentTextField = textField
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
}

extension FormPackageViewController {
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = self.mScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            self.mScrollView.contentInset = contentInset
            self.mScrollView.scrollIndicatorInsets = contentInset
        }
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = self.mScrollView.contentInset
        contentInset.bottom = 0
        self.mScrollView.contentInset = contentInset
        self.mScrollView.scrollIndicatorInsets = contentInset
    }
}
