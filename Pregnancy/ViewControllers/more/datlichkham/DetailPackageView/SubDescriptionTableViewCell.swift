//
//  SubDescriptionTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class SubDescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var mLblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
