//
//  CustomSlideTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class CustomSlideTableViewCell: UITableViewCell , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var mLblTitle: UILabel!
    @IBOutlet weak var mImgSlide: UIImageView!
    
    var images: [String] = []
    var namePackage: String = ""
    var currentItem: Int = 0
    
    private let cellId = "appCellId"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViews()
        autoScrollImage()
    }
    
    let slidesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    @objc func scrollToNextCell(){
        if images.count > 0 {
            let indexPath = IndexPath(item: currentItem, section: 0)
            slidesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
            if currentItem + 1 == images.count {
                currentItem = 0
            } else {
                currentItem += 1
            }
        }
    }
    
    func autoScrollImage() {
        let timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true);
    }
    
    func setupViews() {
        backgroundColor = UIColor.clear
        
        addSubview(slidesCollectionView)

        slidesCollectionView.dataSource = self
        slidesCollectionView.delegate = self
        
        slidesCollectionView.register(AppCell.self, forCellWithReuseIdentifier: cellId)
        
        slidesCollectionView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        slidesCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        slidesCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        slidesCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return max(1, images.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AppCell
        
        if images.count != 0 {
            if let url = URL(string: images[indexPath.item]) {
                if let data = try? Data(contentsOf: url)  {
                    cell.imageView.image = UIImage(data: data)
                }
            }
            
            cell.nameLabel.text = namePackage
            currentItem = indexPath.item
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class AppCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "imgAddTo")
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let boxTitle: UIView = {
        let box = UIView()
        box.backgroundColor = #colorLiteral(red: 0.1700000018, green: 0.1700000018, blue: 0.1700000018, alpha: 0.6)
        box.translatesAutoresizingMaskIntoConstraints = false
        return box
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .white
        label.text = "Gói xét nghiệm MEDLATEC"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupViews() {
        addSubview(imageView)
        addSubview(boxTitle)
        boxTitle.addSubview(nameLabel)
        
        imageView.frame = CGRect(x:0, y: 0, width: frame.width, height: frame.height)
        boxTitle.frame = CGRect(x:0, y: frame.height - 44, width: frame.width, height: 44)
        nameLabel.frame = CGRect(x: 14, y: 0, width: frame.width, height: 44)
    }
}

