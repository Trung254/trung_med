//
//  LocationTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/28/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import WebKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var heightWebView: NSLayoutConstraint!
    @IBOutlet weak var mWebviewDescription: UIView!
    @IBOutlet weak var mLblName: UILabel!
    @IBOutlet weak var mlblMap: UILabel!
    @IBOutlet var mViewHospital: UIView!
    @IBOutlet weak var mbtnUnit: UIButton!
    
    
//    @IBOutlet weak var mLblAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.mlblMap.font = UIFont.fontAwesome(ofSize: 26)
        self.mlblMap.text = String.fontAwesomeIcon(name: .mapMarker)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension LocationTableViewCell: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.heightWebView.constant = height as! CGFloat
                })
            }
            
        })
    }
    
}
