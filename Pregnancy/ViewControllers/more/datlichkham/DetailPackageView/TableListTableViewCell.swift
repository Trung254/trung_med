//
//  TableListTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/1/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class TableListTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
   
    struct DataFilter {
        var name = ""
        var cost = 0
    }
    
    var dataFilter : Array<DataFilter> = [] // Mảng chứa thông tin hiển thị ra table view

    var tableData = JSON()
    
    var subMenuTable:UITableView?
    var header:[String]? = nil
    
    @IBOutlet weak var mTblList: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpTable(cellHeightAuto:Int){
        
        self.getData()
        
        if subMenuTable == nil{
            subMenuTable = UITableView(frame: .init(x: 0, y: 0, width: screenSizeWidth, height: CGFloat(cellHeightAuto)), style:UITableView.Style.plain)
            subMenuTable?.delegate = self
            subMenuTable?.dataSource = self
            subMenuTable?.estimatedRowHeight = 200
            subMenuTable?.rowHeight = UITableView.automaticDimension
            self.addSubview(subMenuTable!)
            subMenuTable?.register(UINib.init(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "listTableViewCell")

        }
    }
    
    func getData() {
        for items in tableData["attributes"].arrayValue {
            for item in items["attribute_values"].arrayValue {
                if item["cost"].intValue != 0 {
                    var itemData = DataFilter()
                    itemData.name = item["name"].stringValue
                    itemData.cost = item["cost"].intValue
                    self.dataFilter.append(itemData)
                }
            }
        }
        self.subMenuTable?.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTableViewCell", for: indexPath) as! ListTableViewCell
        cell.mLblName.text = self.dataFilter[indexPath.row].name
        cell.mLblCost.text = String(self.dataFilter[indexPath.row].cost)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
