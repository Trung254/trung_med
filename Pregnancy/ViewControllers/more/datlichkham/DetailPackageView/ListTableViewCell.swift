//
//  ListTableViewCell.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/1/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var mLblCost: UILabel!
    @IBOutlet weak var mLblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state

    }
    
}
