
//
//  AddNewItemBagViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 1/10/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddNewItemBagViewController: BaseViewController {

    @IBOutlet weak var mlblBagType: UILabel!
    @IBOutlet weak var mAddSubText: UITextField!
    @IBOutlet var mViewAddBag: UIView!
    @IBOutlet weak var mPickerBag: UIView!
    @IBOutlet weak var mPickerview: UIPickerView!
    
    var dataPicker = ["Đồ dùng thiết yếu cho bé","Đồ dùng chăm sóc sức khỏe em bé","Đồ dùng chăm sóc sức khỏe mẹ"]
    var tagBag = 1
    var userId = 0
    var arrIdBag: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addBackgroundImage("more-background-blur.png")
        self.mPickerBag.isHidden = true
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
        mlblBagType.text = dataPicker[tagBag - 1]
        mPickerview.selectRow(tagBag - 1, inComponent:0, animated:true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
    
    @IBAction func saveBag(_ sender: Any) {
        let name = self.mAddSubText.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if name == "" {
            self.alert("Bạn cần nhập tên đồ dùng")
        }
        else {
            self.pushHospitalBag(name!, tagBag, userId)
        }
    }
    
    @IBAction func showPickerView(_ sender: Any) {
        self.mPickerBag.isHidden = false
    }
    
    @IBAction func hiddenPickerView(_ sender: Any) {
        self.mPickerBag.isHidden = true
    }
    
    private func pushHospitalBag(_ name: String,_ type: Int,_ cusId: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiPostHospitalBagItems(name: name, type: type, customItemByUserId: cusId){
            (data, message, isSuccess) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.loadDataHospital(name)
                }
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    private func loadDataHospital(_ name: String) {
        NetworkManager.shareInstance.apiGetAllHospitalBagItems() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["name"].stringValue == name ,item["custom_item_by_user_id"].intValue == self.userId{
                        self.arrIdBag.append(item["id"].intValue)
                    }
                }
                
                if self.arrIdBag.count != 0 {
                    let lastId = self.arrIdBag.last
                    self.pushUserHospital(lastId!)
                }
                
            } else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
    
    private func pushUserHospital(_ hospitalBagItemId: Int) {
        NetworkManager.shareInstance.apiPostUserHospitalBagItems(hospitalBagItemId: hospitalBagItemId) {
            (data, message, isSuccess) in
            if isSuccess {
                DispatchQueue.main.async {
                    self.hideHud()
                    self.navigationController?.popViewController(animated: true)
                }
            }else {
                self.hideHud()
                self.alert("Vui lòng thử lại")
            }
        }
    }
}

extension AddNewItemBagViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataPicker[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tagBag = (row + 1)
        self.mlblBagType.text = dataPicker[row]
    }
}
