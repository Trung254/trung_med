//
//  SubShoppingViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/13/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubShoppingViewController: BaseViewController {
    
    @IBOutlet weak var subShoppingTableView: UITableView!
    
    var dataShoppingByID = [PregShoppingItemByID]()
    var dataShoppingCart: [JSON] = []
    
    var passedTitle:String = ""
    var passedIndex:Int?
    var selectedIndexPath: NSIndexPath = NSIndexPath()
    var firstItem:Array<String> = ["share" ,"Chia sẻ", "Nhận", "Mua"]
    var shoppingItem:Array<String>?
    var passedData:String = ""
    var numberOfBag:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadItemShoppingByID()
        self.navigationController?.navigationBar.tintColor = .white
        subShoppingTableView.register(UINib.init(nibName: "SubShoppingTableViewCell", bundle: nil), forCellReuseIdentifier: "subShoppingCell")
        subShoppingTableView.register(UINib.init(nibName: "FirstCellShoppingTableViewCell", bundle: nil), forCellReuseIdentifier: "FirstCellShopping")
        subShoppingTableView.rowHeight = UITableView.automaticDimension
        subShoppingTableView.estimatedRowHeight = 60.5
        self.addNavigationTitle()
        subShoppingTableView.backgroundView = UIImageView(image: UIImage(named: "me-background-blur"))
        self.subShoppingTableView.tableFooterView = UIView()
    }
    
    func addNavigationTitle(){
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 44))
        lbl.text = passedTitle
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = lbl.font.withSize(22)
        navigationItem.titleView = lbl
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "add"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 30, height: 44)
        infoButton.addTarget(self, action: #selector(handleNavigationIcon), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: addButton)
        let barButton2 = UIBarButtonItem(customView: infoButton)
        
        return [barButton,barButton2]
    }
    
    @objc func handleNavigationIcon(){
        print("handle navigatin icon")
    }
    @objc func addButtonAction(){
        performSegue(withIdentifier: "subShopping_to_add", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "subShopping_to_add" {
            let viewController = segue.destination as! AddToBuyViewController
            viewController.passedItem = passedTitle
        }
    }
    
    @objc func toBuy(_ sender: UIButton) {
        let id = dataShoppingByID[sender.tag - 1].id_item
        if sender.isSelected == true {
            self.updateShoppingItem(id, 3)
        }
        else {
            self.updateShoppingItem(id, 1)
            self.deleteShoppingCart(id)
        }
    }
    
    @objc func gotIt(_ sender: UIButton) {
        let id = dataShoppingByID[sender.tag - 1].id_item
        if sender.isSelected == true {
            self.updateShoppingItem(id, 2)
        }
        else {
            self.updateShoppingItem(id, 1)
            self.deleteShoppingCart(id)
        }

    }
    
    private func loadItemShoppingByID() {
        self.showHud()
        NetworkManager.shareInstance.apiGetShoppingItemsByID(categoryId: passedIndex!) { (dataItemByID, message, isSuccess) in
            self.dataShoppingByID = dataItemByID
            DispatchQueue.main.async {
                self.loadShoppingCart()
            }
        }
    }
    
    private func loadShoppingCart() {
        NetworkManager.shareInstance.apiGetShoppingCart() {
            (data, message, isSuccess) in
            self.dataShoppingCart = []
            if isSuccess {
                let data = data as? JSON
                self.dataShoppingCart = (data?.arrayValue)!
            }
            
            DispatchQueue.main.async {
                self.subShoppingTableView.reloadData()
                self.hideHud()
            }
        }
    }
    
    private func updateShoppingItem(_ shoppingitemid: Int,_ status: Int) {
        NetworkManager.shareInstance.apiPutShoppingItems(shoppingItemId: shoppingitemid, status: status) {
            (data, message, isSuccess) in
            if isSuccess && status == 3 {
                self.pushShoppingCart(shoppingitemid)
            }
            DispatchQueue.main.async {
                self.loadItemShoppingByID()
            }
        }
    }
    
    private func deleteShoppingCart(_ shoppingitemid: Int) {
        NetworkManager.shareInstance.apiDeleteShoppingCart(shoppingItemId: shoppingitemid) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadItemShoppingByID()
            }
        }
    }
    
    private func pushShoppingCart(_ shoppingitemid: Int) {
        NetworkManager.shareInstance.apiPostShoppingCart(shoppingItemId: shoppingitemid) {
            (data, message, isSuccess) in
            DispatchQueue.main.async {
                self.loadItemShoppingByID()
            }
        }
    }
}



extension SubShoppingViewController: UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataShoppingByID.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let firstCell : FirstCellShoppingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FirstCellShopping", for: indexPath) as! FirstCellShoppingTableViewCell
            firstCell.mFirstImage.image = UIImage(named: firstItem[0])
            firstCell.mFirstLabel.text = firstItem[1]
            firstCell.mSecondLabel.text = firstItem[2]
            firstCell.mThirdLabel.text = firstItem[3]
            firstCell.contentView.isUserInteractionEnabled = true
            return firstCell
        }
        else{
            let cell : SubShoppingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "subShoppingCell", for: indexPath) as! SubShoppingTableViewCell
            cell.mToBuyButton.addTarget(self, action: #selector(self.toBuy(_:)), for: .touchUpInside)
            cell.mGotItButton.addTarget(self, action: #selector(self.gotIt(_:)), for: .touchUpInside)
            cell.mToBuyButton.tag = indexPath.row
            cell.mGotItButton.tag = indexPath.row
            cell.mLabel.text = dataShoppingByID[indexPath.row - 1].item_name
            var status = 1
            status = dataShoppingByID[indexPath.row - 1].status
            switch status {
            case 1:
                cell.mToBuyButton.isSelected = false
                cell.mGotItButton.isSelected = false
            case 2:
                cell.mToBuyButton.isSelected = false
                cell.mGotItButton.isSelected = true
            case 3:
                cell.mToBuyButton.isSelected = true
                cell.mGotItButton.isSelected = false
            default:
                break
            }
            return cell
        }
        
    }
}

extension SubShoppingViewController: UITableViewDelegate {
    
}
