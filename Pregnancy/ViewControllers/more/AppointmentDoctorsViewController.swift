//
//  AppointmentDoctorsViewController.swift
//  Pregnancy
//
//  Created by dady on 2/27/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import Cosmos

class DoctorsTableViewCell: UITableViewCell {
    @IBOutlet weak var mAvatar: UIImageView!
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mInfo: UILabel!
    @IBOutlet weak var mPrice: UILabel!
    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var mLabel: UILabel!
    @IBOutlet weak var mheightCollection: NSLayoutConstraint!
    @IBOutlet weak var mHeightLabel: NSLayoutConstraint!
    @IBOutlet weak var mRating: CosmosView!
}

struct timeSchedule {
    var id_product: Int
    var arrSchedule: Array<keyValues>
    
    init(_ id_product: Int,_ arrSchedule: Array<keyValues>) {
        self.id_product = id_product
        self.arrSchedule = arrSchedule
    }
}

class keyValues {
    var key: Int
    var value: String
    
    init(_ key: Int,_ value: String) {
        self.key = key
        self.value = value
    }
}

class AppointmentDoctorsViewController: BaseViewController, PickerDateViewDelegate {
    
    @IBOutlet weak var mtblDoctors: UITableView!
    @IBOutlet weak var mDate: UIButton!
    @IBOutlet weak var mCountDoctors: UILabel!
    
    
//    var dataDoctors: Dictionary<String, JSON> = [:]
    var dataDoctors: JSON = []
    var dataProducts: Array<Dictionary<String, JSON>> = [[:]]
    var dataSchedule: [timeSchedule] = [timeSchedule(0, [keyValues(0,"")])]
    var doctor_index: Int = 0
    var doctor_id: Int = 0
    var hospital_id: Int = 0
    var infoHospital = keyValues(0, "")
    let dateFormatter = DateFormatter()
    var infoDoctor = keyValues(0, "")
    var infoPrice = keyValues(0, "")
    var infoTimeSchedule = keyValues(0, "")
    var manufacture_id = 0
    var curDate:String = ""
    var datePicker:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mIsShowRightButton = false
        self.mtblDoctors.rowHeight = UITableView.automaticDimension
        self.mtblDoctors.estimatedRowHeight = UITableView.automaticDimension
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.curDate = dateFormatter.string(from: Date())
        self.datePicker = curDate
        self.mDate.setTitle(self.curDate, for: .normal)
        self.mtblDoctors.tableFooterView = UIView()
        self.mtblDoctors.backgroundView = UIImageView(image: UIImage(named: "more-background-blur"))
        self.getApiDoctors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func getApiDoctors() {
        
        self.showHud()
        NetworkManager.shareInstance.apiGetUserProfile() { (data, message, isSuccess) in
            if isSuccess {
                let result = data as! JSON
                if result.arrayValue.isEmpty == false {
                    let nopcustomer_id = result[0]["nopcustomer_id"].intValue
                    
//                    NetworkManager.shareInstance.apiGetAppointmentCustomersById(nopcustomer_id: nopcustomer_id) { (data, messge, isSuccess) in
//                        if(isSuccess) {
//                            let result = JSON(data)
//
//                            let cusGUID = result["customers"].arrayValue[0]["customer_guid"].stringValue
//                        }
//                    }
                    
                    NetworkManager.shareInstance.apiGetCategories { (data, message, isSuccess) in
                        if isSuccess {
                            let data = data as! JSON
                            let result = data["categories"].arrayValue.filter({ $0["name"] == "Bác sĩ"})
                            let category_id = result[0]["id"].stringValue
                            
                            NetworkManager.shareInstance.apiGetProductByCategory(categoryId: category_id, callBack: { (data, message, isSuccess) in
                                let data = data as! JSON
                                self.dataDoctors = data["Products"]
                                self.mCountDoctors.text = "Kết quả tìm kiếm (\(self.dataDoctors.count))"
                                self.dataSchedule.removeAll()
                                var count = 0
                                
                                for item in 0..<self.dataDoctors.count {
                                    let id_product = self.dataDoctors[item]["Id"].intValue
                                    
                                    NetworkManager.shareInstance.apiGetProducts(id_product: id_product, callBack: { (data, message, isSuccess) in
                                        if isSuccess {
                                            
                                            let data = data as! JSON
                                            self.dataProducts.removeAll()
                                            self.dataProducts.append(data.dictionaryValue)
                                            let data1 = self.dataProducts[0]["products"]![0]["attributes"]
                                            
                                            for i in 0..<data1.count {
                                                if data1[i]["product_attribute_id"] == 58 {
                                                    var schedule = [keyValues]()
                                                    for j in 0..<data1[i]["attribute_values"].count {
                                                        let key = data1[i]["attribute_values"][j]["id"].intValue
                                                        let value = data1[i]["attribute_values"][j]["name"].stringValue
                                                        
                                                        schedule.append(keyValues(key,value))
                                                    }
                                                    self.dataSchedule.append(timeSchedule(id_product,schedule))
                                                    break
                                                }
                                            }
                                            
                                            count += 1
                                            if count == self.dataDoctors.count {
                                                self.dataSchedule = self.dataSchedule.sorted(by: { $0.id_product < $1.id_product })
                                                DispatchQueue.main.async {
                                                    let alert = UIAlertController(title: "Thông báo", message: "Tìm kiếm dữ liệu bác sĩ thành công", preferredStyle: .alert)
                                                    self.present(alert, animated: true, completion: nil)
                                                    DispatchQueue.delay(.milliseconds(300), closure: {
                                                        alert.dismiss()
                                                        self.hideHud()
                                                        self.mtblDoctors.reloadData()
                                                    })
                                                }
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
            }
            else if (message == "Authorization has been denied for this request." ){
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.hideHud()
                self.alert("Vui lòng kiểm tra lại kết nối")
            }
        }
    }
    
    @IBAction func showDatePicker(_ sender: Any) {
        let pickerDate = PickerDateView.initView()
        pickerDate.delegate = self
        self.navigationController?.view.addSubview(pickerDate)
        pickerDate.datePicker.locale = Locale.init(identifier: "vi")
        pickerDate.datePicker.minimumDate = Date()
    }
    
    func didSelectedDateString(dateString: String) {
        self.mDate.setTitle(dateString, for: .normal)
        self.datePicker = dateString
    }
    
    func doneTapped(_ sender: PickerDateView) {
        self.mtblDoctors.reloadData()
    }
    
    private func checkTimebeforeFourHours(_ timeSchedule: String) -> Bool{
        if curDate == datePicker {
            dateFormatter.dateFormat = "HH"
            let hours = dateFormatter.string(from: Date())
            dateFormatter.dateFormat = "mm"
            let minutes = dateFormatter.string(from: Date())
            
            let cur_time = Float("\(hours).\(minutes)")
            
            if (Float(timeSchedule)! - cur_time!) >= 4 {
                return true
            }else {
                return false
            }
        }
        else {
            return true
        }
    }
}

extension AppointmentDoctorsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataDoctors.isEmpty == true {
            return 0
        }
        else {
            return dataDoctors.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorsTableViewCell", for: indexPath) as! DoctorsTableViewCell
        
        if dataDoctors.isEmpty == false {
            let url = URL(string: dataDoctors[indexPath.row]["DefaultPictureModel"]["ImageUrl"].stringValue)
            if let data = try? Data(contentsOf: url!) {
                cell.mAvatar.image = UIImage(data: data)
            }
            
            cell.mName.text = dataDoctors[indexPath.row]["Name"].stringValue
            cell.mInfo.text = dataDoctors[indexPath.row]["ShortDescription"].stringValue
            cell.mPrice.text = "\(dataDoctors[indexPath.row]["ProductPrice"]["Price"].stringValue) / lượt khám"
            
            let RatingSum = dataDoctors[indexPath.row]["ReviewOverviewModel"]["RatingSum"].doubleValue
            let TotalReviews = dataDoctors[indexPath.row]["ReviewOverviewModel"]["TotalReviews"].intValue
            cell.mRating.text = "(\(TotalReviews) đánh giá)"
            cell.mRating.tintColor = UIColor.darkGray
            cell.mRating.rating = RatingSum
            cell.mRating.isUserInteractionEnabled = false

            if dataSchedule.count != 0 {
                cell.mCollectionView.register(UINib(nibName: "SAColllectionCell", bundle: nil), forCellWithReuseIdentifier: "STCell")
                cell.mCollectionView.tag =  indexPath.row

                if dataSchedule[indexPath.row].arrSchedule.isEmpty == false {
                    cell.mLabel.text = "Chọn giờ khám"
                    cell.mLabel.textColor = UIColor.black
                    cell.mCollectionView.isHidden = false
                }else{
                    cell.mLabel.text = "Không có khung giờ khám"
                    cell.mLabel.textColor = UIColor.red
                    cell.mCollectionView.isHidden = true
                }

                self.doctor_id = dataDoctors[indexPath.row]["Id"].intValue

//                let screenWidth = Float(UIScreen.main.bounds.width) - 30 - 20
//                let num_time_row = Int(screenWidth / 110)
//                let num_time = Double(dataSchedule[doctor_index].arrSchedule.count)
//                cell.mheightCollection.constant = CGFloat(round(num_time / Double(num_time_row)) * 50 + 20)
//                cell.mHeightLabel.constant = CGFloat(Float(cell.mheightCollection.constant)  + 209 - 50)
//                cell.mCollectionView.reloadData()
            }
        }
        
        cell.mCollectionView.reloadData()
        cell.selectionStyle = .none
        return cell
    }
}

extension AppointmentDoctorsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if self.dataSchedule.count != 0 {
            collectionView.collectionViewLayout.invalidateLayout()
            let tag = collectionView.tag
            return self.dataSchedule[tag].arrSchedule.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.collectionViewLayout.invalidateLayout()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STCell", for: indexPath) as! SAColllectionCell
        let tag = collectionView.tag
        let doctorTime = self.dataSchedule[tag].arrSchedule[indexPath.row].value
        
        if let index = doctorTime.index(of: "-") {
            var firstTime = String(doctorTime[..<index])
            firstTime = firstTime.replacingOccurrences(of: "h", with: ".")
            
            if self.checkTimebeforeFourHours(firstTime) == false {
                cell.timeButton.isUserInteractionEnabled = false
                cell.timeButton.setTitleColor(UIColor.lightGray, for: .normal)
            }else {
                cell.timeButton.isUserInteractionEnabled = true
                cell.timeButton.setTitleColor(#colorLiteral(red: 0, green: 0.5882352941, blue: 1, alpha: 1), for: .normal)
            }
        }
        
        cell.timeButton.layer.cornerRadius = 5
        cell.timeButton.layer.masksToBounds = true
        cell.timeButton.setTitle(doctorTime, for: .normal)
        cell.timeButton.tag = tag
        cell.timeButton.accessibilityValue = "\(doctor_id)"
        cell.timeButton.accessibilityHint = "\(self.dataSchedule[tag].arrSchedule[indexPath.row].key)"
        cell.timeButton.addTarget(self, action: #selector(AppointmentDoctorsViewController.registerTimeSchedule(sender:)), for: UIControl.Event.touchUpInside)
        
        return cell
    }
    
    @objc func registerTimeSchedule(sender: UIButton) {
        let doctor_name = dataDoctors[sender.tag]["Name"].stringValue
        let doctor_price = dataDoctors[sender.tag]["ProductPrice"]["Price"].stringValue
        let id_time = Int((sender.accessibilityHint)!)
        
        self.infoTimeSchedule = keyValues(id_time!, (sender.titleLabel?.text)!)
        self.infoDoctor = keyValues(
            Int(
                (sender.accessibilityValue)!)!, doctor_name)
        self.infoPrice = keyValues(0, doctor_price)
        self.performSegue(withIdentifier: "doctor_to_appointment_detail", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "doctor_to_appointment_detail" {
            let date_appoint = self.mDate.titleLabel?.text
            let vc = segue.destination as! AppointmentDoctorDetailViewController
            
            vc.dataAppointment.append(self.infoHospital)
            vc.dataAppointment.append(keyValues(57, date_appoint!))
            vc.dataAppointment.append(self.infoPrice)
            vc.dataAppointment.append(self.infoTimeSchedule)
            vc.dataAppointment.append(self.infoDoctor)
        }
    }
}
