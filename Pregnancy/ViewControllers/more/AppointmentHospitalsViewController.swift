//
//  AppointmentHospitalsViewController.swift
//  Pregnancy
//
//  Created by dady on 2/27/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit


class AppointmentHospitalsViewController: BaseViewController {

    @IBOutlet weak var mtblHospital: UITableView!
    
    var dataHospital: [JSON] = []
    var selectedRow: Int = 0
    var manufacture_id = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: "Chọn cơ sở khám")
//        self.mIsShowRightButton = false

        // Do any additional setup after loading the view.
        self.mtblHospital.tableFooterView = UIView()
        self.addBackgroundImage("more-background-blur")
       
        self.mtblHospital.estimatedRowHeight = 200
        self.mtblHospital.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        
        self.showHud()
        NetworkManager.shareInstance.apiGetManufacture() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.dataHospital = data.arrayValue
            }
            
            DispatchQueue.main.async {
                self.hideHud()
                self.mtblHospital.reloadData()
            }
        }
    }
    
//    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
//        let infoButton = UIButton(type: .custom)
//        infoButton.setImage(UIImage(named: "search"), for: .normal)
//        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
//        infoButton.addTarget(self, action: #selector(searchManufacture), for: .touchUpInside)
//        let barButton = UIBarButtonItem(customView: infoButton)
//        return [barButton]
//    }
    
    @objc func searchManufacture() {
        self.alert("Chức năng đang xây dựng", title: "Thông báo") { (Action) in
            print("Chức năng đang xây dựng")
        }
    }
}

extension AppointmentHospitalsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataHospital.count == 0 {
            return 0
        }
        else {
            return dataHospital.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalTableViewCell", for: indexPath) as! HospitalTableViewCell
        cell.mNameHos.text = dataHospital[indexPath.row]["Name"].stringValue.uppercased()
        let url = URL(string: dataHospital[indexPath.row]["PictureModel"]["ImageUrl"].stringValue)
        let data = try? Data(contentsOf: url!)
        cell.mImgHos.image = UIImage(data: data!)
        let urlStr = dataHospital[indexPath.row]["Description"].stringValue
        var cellWebView: WKWebView
        if let webView  = cell.viewAddress.viewWithTag(100) as? WKWebView {
            cellWebView = webView
        } else {
            let webConfiguration = WKWebViewConfiguration()
            let webView = WKWebView(frame: .zero, configuration: webConfiguration)
            webView.uiDelegate = cell
            webView.navigationDelegate = cell
            
            webView.isUserInteractionEnabled = false
            webView.tag = 100
            
            cell.viewAddress.addSubview(webView)
            webView.translatesAutoresizingMaskIntoConstraints = false
            webView.snp.makeConstraints { (maker) in
                maker.top.equalToSuperview()
                maker.leading.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.trailing.equalToSuperview()
            }
            cellWebView = webView
        }
        cell.mTableview = tableView
        cellWebView.loadHTMLString("""
            <html><head> <meta name="viewport" content="width=device-width, initial-scale=1"> <style>*{font-family: "Helvetica";}img{margin: 0;}</style> <meta charset="utf-8"></head><body>\(urlStr.htmlDecoded)</body></html>
            """, baseURL: nil)
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension AppointmentHospitalsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        self.manufacture_id = dataHospital[selectedRow]["Id"].intValue
        self.performSegue(withIdentifier: "hospital_to_doctors", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "hospital_to_doctors" {
            let vc = segue.destination as! AppointmentDoctorsViewController
        
            vc.infoHospital.key = dataHospital[selectedRow]["Id"].intValue
            vc.infoHospital.value = dataHospital[selectedRow]["Name"].stringValue
            vc.manufacture_id = self.manufacture_id
        }
    }
}

class HospitalTableViewCell: UITableViewCell {
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var mImgHos: UIImageView!
    @IBOutlet weak var mNameHos: UILabel!
    //    @IBOutlet weak var mAddress: UILabel!
    @IBOutlet weak var heightViewAddress: NSLayoutConstraint!
    var mTableview: UITableView?
    var flag = 0
}
extension HospitalTableViewCell: WKUIDelegate {
    
}
extension HospitalTableViewCell: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                
                webView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if error == nil {
                        let height: CGFloat = height as! CGFloat
                        if self.heightViewAddress.constant != height {
                            self.heightViewAddress.constant = height
                            if let tbl = self.mTableview {
                                self.flag = 1
                                tbl.reloadData()
                            }
                        }
                    }
                    
                })
            }
            
        })
    }

    
}
