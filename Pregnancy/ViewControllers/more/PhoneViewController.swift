//
//  PhoneViewController.swift
//
//
//  Created by mai kim tai  on 12/12/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON

class PhoneViewController: BaseViewController {
    
    @IBOutlet weak var mViewGuide: UILabel!
    @IBOutlet weak var tblPhoneList: UITableView!

    var dataphones: [JSON] = []
    var userId = 0
    var dataProfession: [JSON] = []
    var indexRow: Int?
    var pNumber:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mViewGuide.isHidden = true

        self.mIsShowRightButton = false
        // Do any additional setup after loading the view.
        tblPhoneList.tableFooterView = UIView()
//        tblPhoneList.addBackground(imageName: "more-background-blur")
        tblPhoneList.register(UINib.init(nibName: "PhoneTableViewCell", bundle: nil), forCellReuseIdentifier: "PhoneNumberCell")
        userId = (DatabaseManager.sharedManager.getLocalUserInfo()?.id)!
        self.addBackgroundImage("more-background-blur.png")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if dataphones.isEmpty == false {
            dataphones.removeAll()
        }
        if dataProfession.isEmpty == false {
            dataProfession.removeAll()
        }
        
        self.loadDataPhones()
        self.loadProfessionsType()
    }
    
    // Action back to phone list
    @IBAction func backPhoneListViewConTroller(segue:UIStoryboardSegue){}
    
    // Action save new contact
    @IBAction func savePhoneNumbers(segue:UIStoryboardSegue){
        if segue.identifier == "saveBabyPhoneSegue" {
            let vc = segue.source as! AddPhoneViewController
            let NewName = vc.mlblName.text
            let NewPhone = vc.mlblPhone.text
            let profession_id = vc.profession_id
            
            let lblTextSendBaby = NewName?.trimmingCharacters(in: .whitespaces)
            
            if lblTextSendBaby != ""  && lblTextSendBaby != "Nhập ở đây..." {
//                self.backtoFavoriteName(lblTextSendBaby)
                // Validate name and phone field
                if NewName?.isEmpty == true {
                    self.alert("Tên không được để trống")
                }
                else if NewPhone?.isEmpty == true {
                    self.alert("Số điện thoại không được để trống")
                }
                else {
                    
                    let convert = NewPhone!.convertVietnamPhoneNumer()
                    
                    if convert == nil {
                        self.alert("Bạn nhập sai định dạn  số điện thoại", title: "Thông báo") { (action) in
                            //                        vc.mlblPhone.resignFirstResponder()
//                            vc.mlblPhone.textColor = .red
                        }
                    } else {
                        if indexRow == nil {
                            self.pushDataPhones(profession_id, NewPhone!, lblTextSendBaby!)
                        }
                        else {
                            let id = dataphones[indexRow!]["id"].intValue
                            self.updateDataPhone(id, profession_id, NewPhone!, lblTextSendBaby!)
                        }
                    }
                    //                self.pushDataProfession(userId, profession_id)
                    
                }
            }
            else {
                self.alert("Tên không được để trống. Xin vui lòng thử lại", title: "Thông báo") { (action) in
                    vc.mlblName.text = ""
                }
                //            self.navigationController?.popViewController(animated: true)
            }
            
           
        }
    }
    
    private func pushDataProfession(_ userId: Int,_ professionId: Int) {
        NetworkManager.shareInstance.apiPostProfession(userId: userId, professionId: professionId, status: 1) {
            (data, message, isSuccess) in
        }
    }
    
    private func pushDataPhones(_ professionId: Int,_ phoneNumber: String,_ name: String) {
        NetworkManager.shareInstance.apiPostPhones(professionId: professionId, phoneNumber: phoneNumber, userId: userId, name: name) {
            (data, message, isSuccess) in
            if (isSuccess) {
                DispatchQueue.main.async {
                    self.indexRow = nil
                    self.loadDataPhones()
                }
            } else {
                self.alert("Thêm số điện thoại không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                    
                })
            }
            
        }
    }
    
    private func updateDataPhone(_ phoneId: Int,_ professionId: Int,_ phoneNumber: String,_ name: String) {
        NetworkManager.shareInstance.apiPutPhones(phoneId: phoneId, professionId: professionId, phoneNumber: phoneNumber, name: name) {
            (data, message, isSuccess) in
            if (isSuccess) {
                DispatchQueue.main.async {
                    self.indexRow = nil
                    self.loadDataPhones()
                }
            } else {
                self.alert("Thay đổi số điện thoại không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                    
                })
            }
            
        }
    }
 
    private func loadProfessionsType() {
        NetworkManager.shareInstance.apiGetAllProfessionType() {
            (data,message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    self.dataProfession.append(item)
                }
            }

            DispatchQueue.main.async {
                self.tblPhoneList.reloadData()
            }
        }
    }
    
    private func loadDataPhones() {
        NetworkManager.shareInstance.apiGetAllPhones() {
            (data,message, isSuccess) in
            self.dataphones = []
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    if item["user_id"].intValue == self.userId {
                        self.dataphones.append(item)
                    }
                }
                if self.dataphones.isEmpty {
                    self.tblPhoneList.isHidden = true
                    self.mViewGuide.isHidden = false
                } else {
                    self.tblPhoneList.isHidden = false
                    self.mViewGuide.isHidden = true
                    DispatchQueue.main.async {
                        self.tblPhoneList.reloadData()
                    }
                }
                
            } else {
                self.tblPhoneList.isHidden = true
                self.mViewGuide.isHidden = false
            }

            
        }
    }
    
    private func deleteDataPhones(_ phoneId: Int) {
        NetworkManager.shareInstance.apiDeletePhones(phoneId: phoneId) {
            (data,message, isSuccess) in
            DispatchQueue.main.async {
                self.loadDataPhones()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "phone_number_to_add_phone" && indexRow != nil {
            let vc = segue.destination as! AddPhoneViewController
            vc.profession_id = dataphones[indexRow!]["profession_id"].intValue
            vc.phoneNumber = dataphones[indexRow!]["phone_number"].stringValue
            vc.name = dataphones[indexRow!]["name"].stringValue
        }
    }
    
    @objc func callPhone(sender: UIButton) {
        let number = sender.tag
        
        self.prompt("", message: "Bạn có muốn thực hiện cuộc gọi đến số \(number)", okHandler: { (alert) in
            if let url = URL(string: "tel://\(number)") {
                UIApplication.shared.openURL(url)
            }
        }, cancelHandler: nil)
    }
}

extension PhoneViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataphones.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:PhoneTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PhoneNumberCell", for: indexPath) as! PhoneTableViewCell
        cell.selectionStyle = .none
        
        for item in dataProfession {
            if item["id"].intValue == dataphones[indexPath.row]["profession_id"].intValue {
                cell.lblBabyName.text = "\(dataphones[indexPath.row]["name"].stringValue) - \(item["profession_type"].stringValue)"
            }
        }
        cell.lblBabyPhone.text = dataphones[indexPath.row]["phone_number"].stringValue
        cell.mActionPhone.tag = Int(cell.lblBabyPhone.text ?? "0")!
        cell.mActionPhone.addTarget(self, action: #selector(callPhone), for: .touchUpInside)
        return cell
    }
}

extension PhoneViewController: UITableViewDelegate{
    // Switch screen to edit when click a cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexRow = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "phone_number_to_add_phone", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteDataPhones(dataphones[indexPath.row]["id"].intValue)
        }
    }
}
