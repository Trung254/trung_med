//
//  PhoneTableViewCell.swift
//  
//
//  Created by mai kim tai  on 12/12/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit

class PhoneTableViewCell: UITableViewCell {

    @IBOutlet weak var lblBabyName: UILabel!
    @IBOutlet weak var lblBabyPhone: UILabel!
    @IBOutlet weak var mActionPhone: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
