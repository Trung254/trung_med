//
//  AddBabyNameViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/12/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON

class AddBabyNameViewController: BaseViewController {
    
    @IBOutlet weak var mSwitchGender: UISwitch!
    @IBOutlet weak var mBabyName: UITextField!
    @IBOutlet weak var mViewAddName: UIView!
    
    var userId = 0
    var babynames: [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.hidesBackButton = true
        self.addBackgroundImage("more-background-blur")
        userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
    }
    
    override func viewWillAppear(_ animated: Bool) {    
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    
//    @IBAction func babynameSave(_ sender: Any) {
//        let lblTextBaby = self.mBabyName.text
//
//        let lblTextSendBaby = lblTextBaby?.trimmingCharacters(in: .whitespaces)
//
//        if lblTextSendBaby != ""  && lblTextSendBaby != "Nhập ở đây..." {
//            self.backtoFavoriteName(lblTextSendBaby)
//        }
//        else {
//            self.alert("Thêm công việc mới không được để trống. Xin vui lòng thử lại", title: "Thông báo") { (action) in
//                self.mBabyName.text = ""
//            }
//            //            self.navigationController?.popViewController(animated: true)
//        }
//    }

    @IBAction func backtoFavoriteName(_ sender: Any) {
        let babyName = self.mBabyName.text
        var babynameId:Int?
        
//        let lblTextBaby = self.mBabyName.text
        
        let lblTextSendBaby = babyName?.trimmingCharacters(in: .whitespaces)
        
        if lblTextSendBaby != ""  && lblTextSendBaby != "Nhập ở đây..." {
            if babyName != "" {
                var genderId = "1"
                if self.mSwitchGender.isOn == true {
                    genderId = "1"
                }
                else {
                    genderId = "2"
                }
                
                self.showHud()
                NetworkManager.shareInstance.apiPostBabyName(countryId: 1, genderId: genderId, name: lblTextSendBaby!, customBabyNameByUserId: userId) {
                    (data, messge, isSuccess) in
                    
                    if isSuccess {
                        let data = data as? JSON
                        self.babynames = (data?.arrayValue)!
                        
                        NetworkManager.shareInstance.apiGetAllBabyName(countryId: 1){
                            (data, message, isSuccess) in
                            if let data = data as? JSON {
                                for item in (data.arrayValue) {
                                    if lblTextSendBaby == item["name"].stringValue && self.userId == item["custom_baby_name_by_user_id"].intValue {
                                        babynameId = item["id"].intValue
                                        if babynameId != nil {
                                            NetworkManager.shareInstance.apiPostUserBabyName(userId: self.userId, babyNameId: babynameId!) {
                                                (data, messge, isSuccess) in
                                                if isSuccess {
                                                    self.navigationController?.popViewController(animated: true)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        self.hideHud()
                        self.alert("Thêm tên em bé không thành công. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        }
        else {
            self.alert("Tên em bé không được để trông", title: "Thông báo") { (action) in
                self.mBabyName.text = ""
            }
        }
    }
}
