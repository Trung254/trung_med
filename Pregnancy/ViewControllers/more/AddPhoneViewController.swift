
//
//  AddPhoneViewController.swift
//
//
//  Created by mai kim tai  on 12/12/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON
import Contacts
import ContactsUI

class AddPhoneViewController: BaseViewController {
    
    @IBOutlet weak var mlblName: UITextField!
    @IBOutlet weak var mlblPhone: UITextField!
    @IBOutlet weak var pvBabyAddProfession: UIPickerView!
    @IBOutlet weak var btnBabyProfession: UIButton!
    @IBOutlet weak var mlblProfession: UILabel!
    @IBOutlet var mViewAddPhone: UIView!
    @IBOutlet weak var mImportPhone: UIButton!
    // Pickerview data
    var dataProfession: [JSON] = []
    
    var profession_id = 1
    var phoneNumber = ""
    var name = ""
    
    var checkPickerHiden = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        pvBabyAddProfession.isHidden = true
        mlblPhone.keyboardType = UIKeyboardType.phonePad
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.addBackgroundImage("more-background-blur.png")
        self.loadProfessionsType()
        
        if phoneNumber != "" {
            mlblPhone.text = phoneNumber
        }
        if name != "" {
            mlblName.text = name
        }
        
        self.mlblPhone.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
    }
    @IBAction func mAddPhone(_ sender: Any) {
        mlblPhone.keyboardType = UIKeyboardType.numberPad
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func loadProfessionsType() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllProfessionType() {
            (data,message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    self.dataProfession.append(item)
                }
                
                self.mlblProfession.text = self.dataProfession[self.profession_id - 1]["profession_type"].stringValue
                self.pvBabyAddProfession.selectRow(self.profession_id - 1, inComponent: 0, animated: true)
                self.pvBabyAddProfession.reloadAllComponents()
                self.hideHud()
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.view.endEditing(true);
        return true;
    }
    
    @IBAction func btnShowPickerView(_ sender: Any) {
        dismissKeyboard()
        if checkPickerHiden == false {
            pvBabyAddProfession.isHidden = false
            checkPickerHiden = true
        }
        
    }
}

extension AddPhoneViewController: CNContactPickerDelegate {
    //MARK:- contact picker
    @IBAction func importPhoneNumber(_ sender: Any) {
        onClickPickContact()
    }
    
    func onClickPickContact(){
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
                , CNContactPhoneNumbersKey]
        self.present(contactPicker, animated: true, completion: nil)
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        // user name
        let firstName:String = contact.familyName
        let lastName:String = contact.givenName
        
        // user phone number
        let userPhoneNumbers:[CNLabeledValue<CNPhoneNumber>] = contact.phoneNumbers
        let firstPhoneNumber:CNPhoneNumber = userPhoneNumbers[0].value
        
        // user phone number string
        let primaryPhoneNumberStr:String = firstPhoneNumber.stringValue
        
        mlblPhone.text = primaryPhoneNumberStr
        mlblName.text = firstName + " " + lastName
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
}

extension AddPhoneViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataProfession.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataProfession[row]["profession_type"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    // Get the selected row
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        self.mlblProfession.text = self.dataProfession[selectedRow]["profession_type"].stringValue
        self.profession_id = self.dataProfession[selectedRow]["id"].intValue
        pvBabyAddProfession.isHidden = true
        checkPickerHiden = false
    }
}

extension AddPhoneViewController : UITextFieldDelegate {
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        let phoneNumber = textField.text
//        let convert = phoneNumber?.convertVietnamPhoneNumer()
//
//        if convert == nil {
//            self.alert("Bạn nhập sai định dạng số điện thoại", title: "Thông báo") { (action) in
//                textField.resignFirstResponder()
//            }
//        }
//    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        textField.textColor = .black
    }
}

