//
//  BabyNameViewController.swift
//  Pregnancy
//
//  Created by dady on 2/13/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class BabyNameViewController: BaseViewController {

    @IBOutlet weak var mCountry: UIImageView!
    @IBOutlet weak var mFavoriteName: UIButton!
    
    var hasFavoritename: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.addBackgroundImage("more-background-blur")
        self.mCountry.layer.cornerRadius = mCountry.width/2
        self.mCountry.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        self.mFavoriteName.addTarget(self, action: #selector(moveToFavoriteView), for: .touchUpInside)
        detectHasFavoriteName()
    }
    
    @objc func moveToFavoriteView(_ sender: UIButton) {
        if hasFavoritename {
            let vc = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "BabyFavoriteNameViewController") as! BabyFavoriteNameViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.alert("Chọn tên yêu thích đầu tiên trong danh sách")
        }
    }
    
    fileprivate func detectHasFavoriteName() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllBabyName(countryId: 1){
            (data, message, isSuccess) in
            if isSuccess {
                let userId = DatabaseManager.sharedManager.getLocalUserInfo()?.id ?? 0
                let data = data as? JSON
                
                for item in (data ?? "").arrayValue {
                    if item["user_id"].stringValue == "\(userId)" {
                        self.hasFavoritename = true
                        break
                    } else {
                        self.hasFavoritename = false
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
    }
}
