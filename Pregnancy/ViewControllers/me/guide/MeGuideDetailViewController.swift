//
//  MeGuideLeveTwoViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/10/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON

class MeGuideDetailViewController: BaseViewController {

    @IBOutlet weak var wvMeGuie: UIWebView!
    
    @IBOutlet weak var imgMeGuideWeb: UIImageView!
    var check = 0 // Lay thang cha qua
    var check2 = 0 // Lay thang ong qua
    var guideArray : [String] = [String]()
    var titleGuideDetail: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitle(title: titleGuideDetail!)
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = true
        super.viewWillAppear(animated)
    }
    
    func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetGuides(guideTypeId: check2) { (data, message, isSuccess) in
            let domain1 = "http://www.momtour.vn/"
            if (isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let guidesType : PregGuides = PregGuides()
                    guidesType.content = item["content"].stringValue
                    let url = domain1 + guidesType.content
                    self.guideArray.append(url)
                    print(self.guideArray)
                }
            }
            
            let url = URL(string: self.guideArray[self.check])
            let req = URLRequest(url: url!)
            self.wvMeGuie.loadRequest(req)
            
            self.hideHud()
        }
    }

}
