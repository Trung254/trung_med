//
//  MeGuideViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/10/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON

var indexGuide = 0

class MeGuideViewController: BaseViewController {
    
    @IBOutlet weak var tblMeGuide: UITableView!
    @IBOutlet var mViewGuides: UIView!
    
    var selectRow : Int?
    var titleTranfer : String?
    
    var guideArray : [PregGuidesType] = [PregGuidesType]()
    var meguides: [JSON] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.setTitle(title: "Cẩm nang hướng dẫn")
//        tblMeGuide.rowHeight = UITableView.automaticDimension
//        tblMeGuide.estimatedRowHeight = 43
        tblMeGuide.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        tblMeGuide.backgroundView = UIImageView(image: UIImage(named: "more-background-blur"))
        tblMeGuide.tableFooterView = UIView()
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 240.0))
        let image: UIImage = UIImage(named: "me-guides-header")!
        let headerImageView = UIImageView(image: image)
        headerImageView.frame = CGRect.init(x: 0, y: 0, width: headerView.bounds.width, height: headerView.bounds.height)
        headerView.addSubview(headerImageView)
        tblMeGuide.tableHeaderView = headerView
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = true
        super.viewWillAppear(animated)
    }
    
    func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllGuides { (items, message, isSuccess) in
            self.guideArray = items
            self.tblMeGuide.reloadData()
            self.hideHud()
        }
    }
}

extension MeGuideViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        indexGuide = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.selectRow = guideArray[indexPath.row].id
        self.titleTranfer = guideArray[indexPath.row].type
        performSegue(withIdentifier: "segue-id", sender: (Any).self)
        
//        if indexPath.row ==?
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MeGuideListViewController
        vc.check = self.selectRow!
        vc.titleGuide = self.titleTranfer
    }
    
}

extension MeGuideViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guideArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:GuideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
        cell.selectionStyle = .none
        
        cell.lblTitle.text = guideArray[indexPath.row].type
        
//        cell.mImageView?.image = UIImage(named: arrGuide[indexPath.row])
        
        return cell
    }
    
}
