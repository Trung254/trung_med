//
//  MeGuideLevelOneViewController.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/10/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class MeGuideListViewController: BaseViewController {
    
    var selectRow : Int?
    var check = 0 // Thang ong qua
    var titleGuide : String?
    var titleTranferDetail : String?
    
    
    @IBOutlet weak var mtblGuideList: UITableView!
    var guideArray : [PregGuides] = [PregGuides]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.title = arrGuide[indexGuide]
        mtblGuideList.backgroundView = UIImageView(image: UIImage(named: "me-background-blur"))
        mtblGuideList.tableFooterView = UIView()
        
        mtblGuideList.register(UINib.init(nibName: "GuideTableViewCell", bundle: nil), forCellReuseIdentifier: "guideCell")
        mtblGuideList.delegate = self
        mtblGuideList.dataSource = self
        self.loadData()
        mtblGuideList.rowHeight = UITableView.automaticDimension
        self.setTitle(title: titleGuide!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = true
        super.viewWillAppear(animated)
    }
    
    func loadData() {
        self.showHud()
        NetworkManager.shareInstance.apiGetGuides(guideTypeId: check) { (data, message, isSuccess) in
            if (isSuccess) {
                let result = JSON(data)
                for item in result.arrayValue {
                    let guidesType : PregGuides = PregGuides()
                    guidesType.guides_type_id = item["guides_type_id"].intValue
                    guidesType.page_id = item["page_id"].intValue
                    guidesType.guides_type = item["guides_type"].stringValue
                    guidesType.title = item["title"].stringValue
                    guidesType.content = item["content"].stringValue
                    guidesType.page_image = item["page_image"].stringValue
                    self.guideArray.append(guidesType)
                    print(self.guideArray)
                }
            }
            self.mtblGuideList.reloadData()
            self.hideHud()
        }
    }
}

extension MeGuideListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return arrGuide.count
        return self.guideArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:GuideTableViewCell = tableView.dequeueReusableCell(withIdentifier: "guideCell", for: indexPath) as! GuideTableViewCell
//        cell.selectionStyle = .none
        cell.lblTitle.text = guideArray[indexPath.row].title
        if let labelLeadingToContainViewConstraint = cell.labelLeadingToContainViewConstraint {
            if (cell.contentView.constraints.contains(labelLeadingToContainViewConstraint)) {
                cell.contentView.removeConstraint(labelLeadingToContainViewConstraint)
            }
        }
        
        
//        cell.labelLeadingToContainViewConstraint.priority = UILayoutPriority(rawValue: 250)
//        cell.labelLeadingToImageConstraint.priority = UILayoutPriority(rawValue: 500)
        
        
        let urlString = NetworkManager.rootDomain + guideArray[indexPath.row].page_image
        cell.mImageView.sd_cancelCurrentImageLoad()
        if let url = URL(string: urlString) {
            DispatchQueue.main.async {
                cell.mImageView.sd_setImage(with: url, placeholderImage: UIImage(named:""), options: SDWebImageOptions.highPriority, completed: nil)
//                cell.labelLeadingToContainViewConstraint.priority = UILayoutPriority(rawValue: 250)
            }
        }
        
        //cell.lblTitle.text = arrGuide[indexPath.row]
        
        return cell
    }
}

extension MeGuideListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
//        indexGuideLevelOne = indexPath.row
        
        self.selectRow = indexPath.row
        self.titleTranferDetail = guideArray[indexPath.row].title
        performSegue(withIdentifier: "segue-id-level-one", sender: (Any).self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! MeGuideDetailViewController
        vc.check = self.selectRow!
        vc.check2 = self.check
        vc.titleGuideDetail = self.titleTranferDetail
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
}
