//
//  WeightEntryViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/20/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class WeightEntryViewController: BaseViewController {
    
    @IBOutlet weak var mtblListWeight: UITableView!
    @IBOutlet weak var mSegWei: UISegmentedControl!
    
    var curSeg:String?
    var weightUnit:Int?
    var startWeight:Float?
    var unitString: String = "kg"
    var dataSet : JSON?
    var indexRow: Int = 0
    
    var dataMyWeight : [PregMyWeight] = []
    var dataWeiSort : [PregMyWeight] = []

    var parrentEntryVC : UIViewController?
//    var parrentVC : BaseViewController?
    var start_date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTitle(title: "Bảng cân nặng")
        
        // Do any additional setup after loading the view.
        mtblListWeight.backgroundView = UIImageView(image: UIImage(named: "background-my-weight"))
        
        mtblListWeight.register(UINib.init(nibName: "WeightEntryTableViewCell", bundle: nil), forCellReuseIdentifier: "weightEntryTableViewCell")
//        mSegWei.selectedSegmentIndex = weightUnit! - 1
        mtblListWeight.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        mtblListWeight.reloadData()
//        self.checkValueUnit()
        if dataMyWeight.isEmpty == false {
            dataMyWeight.removeAll()
        }
        if dataWeiSort.isEmpty == false {
            dataWeiSort.removeAll()
        }
        self.LoadDataFromAPI()
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let addWeightButton = UIButton(type: .custom)
        addWeightButton.setImage(UIImage(named: "add"), for: .normal)
        addWeightButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        addWeightButton.addTarget(self, action: #selector(doAddWeight), for: .touchUpInside)
        let weightButton = UIBarButtonItem(customView: addWeightButton)
//        return [barButton]
        
        let infoButton = UIButton(type: .custom)
        infoButton.setImage(UIImage(named: "info (1)"), for: .normal)
        infoButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        infoButton.addTarget(self, action: #selector(doDefaultInfo), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: infoButton)
        return [weightButton, barButton]
        
        
    }
    @objc func doAddWeight(_ sender : Any?) -> Void {
//        performSegue(withIdentifier: "weight_to_addWeight", sender: self)
        let mainSb = UIStoryboard.init(name: "Me", bundle: nil)
        let addWeighVC = mainSb.instantiateViewController(withIdentifier: "AddWeightViewController") as! AddWeightViewController
        addWeighVC.parrentAddVC = self
//        ddWeighVC.weightFromTableWeight = dataWeiSort[indexPath.row].current_weight
//        addWeighVC.dayFromTableWeight = dataWeiSort[indexPath.row].current_date
//        addWeighVC.dataDate = dataWeiSort[indexPath.row].current_date
        self.navigationController?.pushViewController(addWeighVC, animated: true)
    }
    
    
    func LoadDataFromAPI() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllMyWeights { (data, message, isSuccess) in
            self.dataMyWeight.removeAll()
            if(isSuccess) {
                
                let resutl = JSON(data)
                if let pregnancy = DatabaseManager.sharedManager.getLocalPregnancys() {
                    
                    self.start_date = pregnancy.start_date
                    
                    for items in resutl.arrayValue {
                        let item = PregMyWeight()
                        item.my_weight_type_id = items["my_weight_type_id"].intValue
                        item.current_weight = items["current_weight"].floatValue
                        item.week_id = items["week_id"].intValue
                        item.id = items["id"].intValue
                        
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        
//                        let startDate = items["start_date"].stringValue
//                        if startDate.isEmpty == false {
//                            if let date = dateFormatter.date(from: startDate) {
//
//                                if(/*date.compare(pregnancy.due_date) == .orderedDescending ||
//                                    date.compare(pregnancy.start_date) == .orderedAscending ||*/
//                                    Calendar.current.dateComponents([.day], from: date, to: pregnancy.start_date).day != 0){
//                                    continue
//                                }
//                                item.start_date  = date
//                            }
//                        }
                        item.start_date = pregnancy.start_date
                        
                        let crrDate = items["current_date"].stringValue
                        if crrDate.isEmpty == false {
                            if let date = dateFormatter.date(from: crrDate) {
//                                let dateCheck = date.addingTimeInterval(86400)
                                if(date.compare(pregnancy.due_date.addingTimeInterval(86399)) == .orderedDescending){
                                    continue
                                }
                                if(date.compare(pregnancy.start_date.addingTimeInterval(-86399)) == .orderedAscending) {
                                    continue
                                }
                                item.current_date  = date
                            }
                        }
                        self.dataMyWeight.append(item)
                    }
                    
                    self.dataMyWeight.sort(by: { (weight, nextWeight) -> Bool in
                    return weight.current_date.compare(nextWeight.current_date) != .orderedAscending
                    })
                    
                    self.dataWeiSort = self.dataMyWeight.sorted(by: { (weight, nextWeight) -> Bool in
                    return weight.current_date.compare(nextWeight.current_date) != .orderedAscending
                    })
                    
//                    self.mSegWei.selectedSegmentIndex = (self.dataMyWeight.last?.my_weight_type_id)! - 1
                }
                self.mtblListWeight.reloadData()
                self.hideHud()
            }
            self.mtblListWeight.reloadData()
            self.hideHud()
        }
        
//        self.updateUnitSetting()
        
    }
    
    @IBAction func clickChoiseUnit(_ sender: Any) {
//        let myWeiTypeId = mSegWei.selectedSegmentIndex + 1
//        self.dataMyWeight.last?.my_weight_type_id = myWeiTypeId
//        self.mtblListWeight.reloadData()
//        //Update my_weight_type_id len Server
//        let id = dataMyWeight.last?.id
//        NetworkManager.shareInstance.apiPutMyWeights(myWeightId: id!, myWeightTypeId: myWeiTypeId, currentDate: nil, curWeight: nil) { (data, message, isSuccess) in
//        }
    }
}
    
    
//    @IBAction func segChangeValueWeight(_ sender: Any) {
//        switch mSegWei.selectedSegmentIndex
//        {
//            case 0:
//                if(weightUnit == 2) {
//                    transferValueWeight(ratioWe: 0.454)
//                }
//                if(weightUnit == 3) {
//                    transferValueWeight(ratioWe: 6.350)
//                }
//                weightUnit = 1
//            case 1:
//                if(weightUnit == 1) {
//                    transferValueWeight(ratioWe: 2.205)
//                }
//                if(weightUnit == 3) {
//                    transferValueWeight(ratioWe: 14)
//                }
//                weightUnit = 2
//            case 2:
//                if(weightUnit == 1) {
//                    transferValueWeight(ratioWe: 0.157473)
//                }
//                if(weightUnit == 2) {
//                    transferValueWeight(ratioWe: 0.071)
//                }
//                weightUnit = 3
//            default:
//                break
//        }
//
//        self.updateUnitSetting()
//        mtblListWeight.reloadData()
//    }


extension WeightEntryViewController: UITableViewDataSource  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataWeiSort.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let dateWeiSort = dataMyWeight.sorted { (item1, item2) -> Bool in
//            item1.current_date > item2.current_date
//        }
        
        
        let cell : WeightEntryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "weightEntryTableViewCell", for: indexPath) as! WeightEntryTableViewCell
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "vi_VI")
        
        
        let date: Date = dataWeiSort[indexPath.row].current_date
        dateFormatter.dateFormat = "dd MMM"
        
        cell.lblWEDate.text = dateFormatter.string(from: date)
//        cell.lblWEWeek.text = String(dataWeiSort[indexPath.row].week_id)
        let component = Calendar.current.dateComponents([.weekOfYear], from: self.start_date, to: dataWeiSort[indexPath.row].current_date)
        var week = component.weekOfYear!
        
        if week > 40 {
            week = 40
        } else if week < 0 {
            week = 0
        }
        cell.lblWEWeek.text = String(week)
        
        
        switch mSegWei.selectedSegmentIndex {
        case 1:
            let weightLbs = dataWeiSort[indexPath.row].current_weight * 2.2046
            cell.lblWEWeight.text = String(format: "%.1f", weightLbs) + " lbs"
            
            let weightChange = dataWeiSort[indexPath.row].current_weight - dataWeiSort[0].current_weight
            cell.lblWEChange.text = String(format: "%.1f", weightChange) + " lbs"
            
            cell.imgWEChangeIcon.image = nil
            if weightChange > 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-top-right")
            }
            if weightChange < 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-down-right")
            }
            break
        case 2:
            let weightLbs = dataWeiSort[indexPath.row].current_weight * 0.15747
            cell.lblWEWeight.text = String(format: "%.1f", weightLbs) + " st"
            
            let weightChange = dataWeiSort[indexPath.row].current_weight - dataWeiSort[0].current_weight
            cell.lblWEChange.text = String(format: "%.1f", weightChange) + " st"
            cell.imgWEChangeIcon.image = nil
            if weightChange > 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-top-right")
            }
            if weightChange < 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-down-right")
            }
            break
            
        default:
            let weightLbs = dataWeiSort[indexPath.row].current_weight
            cell.lblWEWeight.text = String(format: "%.1f", weightLbs) + " kg"
            
            let weightChange = dataWeiSort[indexPath.row].current_weight - dataWeiSort.last!.current_weight
            cell.lblWEChange.text = String(format: "%.1f", weightChange) + " kg"
            cell.imgWEChangeIcon.image = nil
            if weightChange > 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-top-right")
            }
            if weightChange < 0.0 {
                cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-down-right")
            }
            break
            
        }

        cell.selectionStyle = .none
        return cell
    }
}




extension WeightEntryViewController: UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let myWeightId = dataWeiSort[indexPath.row].id
            print(myWeightId)
            
            NetworkManager.shareInstance.apiDeleteMyWeights(myWeightId: myWeightId) {
                (data, message, isSuccess) in
                self.dataWeiSort.remove(at: indexPath.row)
                self.mtblListWeight.reloadData()
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        indexRow = indexPath.row
//        performSegue(withIdentifier: "wei_entry_to_add_wei", sender: indexPath)
        
        let mainSb = UIStoryboard.init(name: "Me", bundle: nil)
        let addWeighVC = mainSb.instantiateViewController(withIdentifier: "AddWeightViewController") as! AddWeightViewController
        addWeighVC.weightFromTableWeight = dataWeiSort[indexPath.row].current_weight
        addWeighVC.dayFromTableWeight = dataWeiSort[indexPath.row].current_date
        addWeighVC.myWeightIdFromTableWeight = dataWeiSort[indexPath.row].id
//        addWeighVC.dataDate = dataWeiSort[indexPath.row].current_date
        addWeighVC.parrentAddVC = self
        self.navigationController?.pushViewController(addWeighVC, animated: true)
        
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let vc = segue.destination as! AddWeightViewController
//        vc.parrentAddVC = self.parrentEntryVC
//    }
}
