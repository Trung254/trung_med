//
//  WeightEntryTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class WeightEntryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblWEDate: UILabel!
    @IBOutlet weak var lblWEWeek: UILabel!
    @IBOutlet weak var lblWEWeight: UILabel!
    @IBOutlet weak var lblWEChange: UILabel!
    @IBOutlet weak var imgWEChangeIcon: UIImageView!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
