//
//  WeightEntriesViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class WeightEntriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblListWeightEntries: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        tblListWeightEntries.register(UINib.init(nibName: "WeightEntriesTableViewCell", bundle: nil), forCellReuseIdentifier: "wei_entries_cell")
        tblListWeightEntries.register(UINib.init(nibName: "WeightEntryTableViewCell", bundle: nil), forCellReuseIdentifier: "weightEntryTableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataWeiSam.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:WeightEntryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "weightEntryTableViewCell", for: indexPath) as! WeightEntryTableViewCell
        
        cell.lblWEDate.text = dataWeiSam[indexPath.row].dataCurDate
//        cell.lblWEWeek.text = "8"
//        cell.lblWEWeight.text = dataWeiSam[indexPath.row].dataCurWei
//        
//        let subWei = Float(dataWeiSam[indexPath.row].dataCurWei)! - Float(dataWeiSam[indexPath.row].dataCurDate)!
//        cell.lblWEChange.text = "\(subWei) \(dataWeiSam[indexPath.row].dataSegIndex)"
//        if subWei > 0 {
//            cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-top-right")
//        }
//        if subWei < 0 {
//            cell.imgWEChangeIcon.image = UIImage.init(named: "arrow-down-right")
//        }
        
        return cell
    }

}
