//
//  MeViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/12/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class MeViewController: MenuCardViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        self.addPadLockButton()
        super.viewWillAppear(animated)
    }
    
    // setup menu items 
    override func setupMenuItems() {
        let name = ["Cẩm nang hướng dẫn","Cân nặng","Hình ảnh bụng bầu","Kế hoạch sinh con","Lịch hẹn","Việc cần làm"]
        let nameImages = ["Guide","My Weight","My Belly","Birth Plan","Appointments","To Do"]
            for i in 0...(name.count - 1) {
                menuItems.append(MenuItem.init(name: name[i], imageName: nameImages[i] ))
            }
    }
    
    override func getViewTitle() -> String {
        return "Mẹ"
    }
    
    override func backgroundImageName() -> String {
        return "me-background"
    }
    
    override func segueForMenuItemAtIndex(_ index: Int) -> String {
        var segue_id = ""
        switch index {
        case 0:
            segue_id = "me_to_guide"
            break
        case 1:
            segue_id = "me_to_my_weight"
            break
        case 2:
            segue_id = "me_to_my_belly"
            break
        case 3:
            segue_id = "me_to_birthplan"
            break
        case 4:
            segue_id = "me_to_appointments"
//            alert("Chức năng đang phát triển")
            break
        case 5:
            segue_id = "me_to_to_do_list"
            break
        default:
            NSLog("Selected : \(index)")
        }
        return segue_id
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
