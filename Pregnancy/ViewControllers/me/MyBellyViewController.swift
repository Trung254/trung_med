//
//  MyBellyViewController.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/9/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import MBProgressHUD
import MobileCoreServices
import AVFoundation
import Photos
import RealmSwift
import SwiftyJSON
import SDWebImage
import CropViewController

class MyBellyCollectionViewCell : UICollectionViewCell{
    
    @IBOutlet weak var mAskForPhotoLbl: UILabel!
    @IBOutlet weak var mBellyImage: UIImageView!
    @IBOutlet weak var mAddBellyImage: UIImageView!
    @IBOutlet weak var mbtnAddBellyImage: UIButton!
    @IBOutlet weak var mBtnShare: UIButton!
    
}

class MyBellyViewController: BaseViewController, UIDocumentMenuDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((NSURL) -> Void)?
    var filePickedBlock: ((URL) -> Void)?
    
    var imagePicker = UIImagePickerController()
    var pickedImageProduct = UIImage()
    
    var pregMyBelly : Dictionary<String, PregMyBelly> = [:]
    
    enum AttachmentType: String{
        case camera, video, photoLibrary, deleteImage
    }
    
    struct Constants {
        static let actionFileTypeHeading = "Upload Avatar"
        static let actionFileTypeDescription = "Chọn file hoặc chụp ảnh"
        static let camera = "Camera"
        static let phoneLibrary = "Phone Library"
        static let deleteImage = "Delete Image"
        static let video = "Video"
        static let file = "File"
        
        
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        
        static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        
        
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
        
    }
    
    
    @IBOutlet weak var imageSegment: UISegmentedControl!
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    var isImage:Bool = true
    var cellWidth:CGFloat = 0
    var cellHeight:CGFloat = 0
    var lastContentOffset: CGFloat = 0
    var numberMonth:Int = 1
    var position:CGFloat = 0
    var photoUpload: UIImage?
    var mPhotoUpload:UIImageView!
    var month: Int?
    var mUpdateDataWhenAppear = true
    
//    var illustrateBelliesItems : [PregMyBelly] = [PregMyBelly]()
    var illustrateBelliesItems: Results<PregMyBelly>?
    var localBelliesItem : Dictionary<String, UIImage> = [:]
    var myBelliesItems : [PregMyBelly] = [PregMyBelly]()
    var myBelliesMonth : [PregMyBelly] = [PregMyBelly]()
    var listImage : Dictionary = [Int : UIImage]()
    
    
    var urlImageShare = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        self.changeStyleNavigationLabel()
        
        if mUpdateDataWhenAppear {
            self.loadIllustrateBellies()
            self.loadMybelliesBy()
            
            DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
                if let pregnancy = pregnancy {
                    var components = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date.addingTimeInterval(86400))
                    
                    if var day = components.day {
                        if (day < 0 ) {
                            day = 0
                        } else {
                            day = 280 - day
                        }
                        
                        self.weeks = day / 7
                        self.days = day - (self.weeks! * 7 )
                    } else {
                        self.days = 0
                        self.weeks = 0
                    }
                    if pregnancy.baby_already_born == 1 {
                        self.month = 0
                    }else {
                        if let days = self.days, let weeks = self.weeks {
                            var month = (days + weeks*7)/30 + 1
                            if month > 9 {
                                month = 9
                            }
                            self.month = month - 1
                            
                        }
                    }
                    
                    
                    let indexPath = IndexPath(item: self.month ?? 0 , section: 0)
                    self.mCollectionView.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: false)
                    self.mCollectionView.reloadData()
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
//        self.mIsShowRightButton = false
        super.viewWillAppear(animated)
        
    }
    
    private func loadIllustrateBellies() {
        self.showHud()
        DatabaseManager.sharedManager.getIllustrateBellyBy(month: nil) { (result, isTemp, isSuccess) in
            self.illustrateBelliesItems = result
            self.mCollectionView.reloadData()
            
            DispatchQueue.main.async {
                self.loadMyBellies()
            }
        }
    }
    
    private func loadMybelliesBy() {
        self.showHud()
        
        NetworkManager.shareInstance.apiGetMyBelliesBy { (data, message, isSuccess) in
            
            self.urlImageShare.removeAll()
            
            for i in 0..<data.count {
                self.urlImageShare.append(data[i].image)
            }
            
            
            
            self.pregMyBelly.removeAll()
            for myBelly in data {
                self.pregMyBelly[String(myBelly.month)] = myBelly
            }
            
            DispatchQueue.main.async {
                self.mCollectionView.reloadData()
                self.hideHud()
            }
        }
    }
    
    private func loadMyBellies() {
        for i in 0..<9 {
            if let image = AttachmentHandler.loadImageFromDiskWith(fileName: "Tháng \(i+1)") {
                self.localBelliesItem["Tháng \(i+1)"] = image
            }
        }
        
        DispatchQueue.main.async {
            self.hideHud()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
        numberMonth = page
        self.changeStyleNavigationLabel()
    }
    
    func changeStyleNavigationLabel(){
        var lbl : UILabel?
        if(navigationItem.titleView == nil) {
            lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.75, height: 44))
            navigationItem.titleView = lbl
            lbl?.textColor = .white
            lbl?.textAlignment = .center
            lbl?.font = lbl?.font.withSize(20.0)
        } else {
            lbl = navigationItem.titleView as? UILabel
        }
        lbl?.text = "Tháng \(numberMonth)"
    }
    
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "next_arrow"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
        rightButton.addTarget(self, action: #selector(self.nextImage), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: rightButton)
        return [barButton]
    }
//    func addNavigationItem(){
//        let rightButton = UIButton(type: .custom)
//        rightButton.setImage(UIImage(named: "arrow-pointing-to-right.png"), for: .normal)
//        rightButton.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
//        rightButton.addTarget(self, action: #selector(self.nextImage), for: .touchUpInside)
//        let barButton = UIBarButtonItem(customView: rightButton)
//        self.navigationItem.rightBarButtonItem = barButton
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isMyBelliesDisplay() == false {
            if let illustrateBelliesItems = self.illustrateBelliesItems {
                return illustrateBelliesItems.count
            }
            return 0
        } else {
            return 9
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyBellyCollectionViewCell", for: indexPath) as! MyBellyCollectionViewCell
        cell.mbtnAddBellyImage.removeTarget(nil, action: nil, for: .allEvents)
        if self.hudShowCount > 1 {
            self.hudShowCount = 1
            self.hideHud()
        }
        
        if isMyBelliesDisplay() == false {
            
            if let illustrateBelliesItems = self.illustrateBelliesItems {
                let urlString = illustrateBelliesItems[indexPath.row].image
                if let url = URL(string: urlString) {
                    self.showHud()
                    cell.mBellyImage.sd_setImage(with: url) { (image, error, cacheType, url) in
                        cell.mBellyImage.image = image
                        self.hudShowCount = 1
                        self.hideHud()
                    }
                }
            }
            cell.mBellyImage.isHidden = false
            cell.mAskForPhotoLbl.isHidden = true
            cell.mAddBellyImage.isHidden = true
            cell.mBtnShare.isHidden = true
            
//            cell.mBtnShare.isHidden = false
//            cell.mBtnShare.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
//            let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) +  " Chia sẻ"
//            cell.mBtnShare.setTitle(titleBtn, for: .normal)
//            cell.mBtnShare.addTarget(self, action: #selector(shareAllImage), for: .touchUpInside)
            
        }
        else {
            cell.mbtnAddBellyImage.addTarget(self, action: #selector(self.choosePhoto(_:)), for: .touchUpInside)
            cell.mAskForPhotoLbl.isHidden = false
            cell.mBellyImage.isHidden = true
            cell.mAddBellyImage.isHidden = false
            cell.mAddBellyImage.image = nil
            let month = String(indexPath.item + 1)
            if let myBelly = self.pregMyBelly[month] {
                
                cell.mBtnShare.isHidden = false
                cell.mBtnShare.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
                let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) +  " Chia sẻ toàn bộ ảnh"
                cell.mBtnShare.setTitle(titleBtn, for: .normal)
                cell.mBtnShare.addTarget(self, action: #selector(shareAllImage), for: .touchUpInside)
                
                let urlStr = myBelly.image
                let url = URL(string: urlStr)
                cell.mAddBellyImage.sd_cancelCurrentImageLoad()
                
                self.showHud()
                
                cell.mAddBellyImage.sd_setImage(with: url) { (image, error, cacheType, url) in
                    self.hideHud()
                }
            } else {
                cell.mBtnShare.isHidden = true
            }
        }
        
        return cell
    }
    
    private func isMyBelliesDisplay() -> Bool {
        return imageSegment.selectedSegmentIndex == 1
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    private func downloadImage(from url: URL, completion: @escaping (UIImage?) -> ()) {
        print("URL is: \(url)")
        self.getData(from: url) { (imageData, response, error) in
            guard let data = imageData, error == nil else {
                return
            }
            completion(UIImage.init(data: data))
        }
    }
    func createArrayImgShare(data: [String], callBack: @escaping ([UIImage]) -> ()) -> Void{
        var imgShare = [UIImage]()
        let imgView = UIImageView()
        
        for i in 0..<data.count {
            
            let urlStr = data[i]
            let url = URL(string: urlStr)
            imgView.sd_setImage(with: url, completed: nil)
            
            if let imgView = imgView.image {
                imgShare.append(imgView)
            }
        }
        
        callBack(imgShare)
    }
    
    @objc func shareAllImage() {
        
        print("share")
        self.createArrayImgShare(data: self.urlImageShare) { (arrImg) in
            
            print(arrImg.count)
            
            let activityController = UIActivityViewController(activityItems: arrImg, applicationActivities: nil)
            activityController.setValue("Bụng bầu của tôi", forKey: "subject")
            UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
        }
    }
    
    @objc func nextImage() {
        print(numberMonth)
        if (0 <= numberMonth) && (numberMonth < 9) {
            mCollectionView.scrollToItem(at: IndexPath.init(item: numberMonth, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
        } else {
            mCollectionView.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
        }
        
        
        
       
    }
    
    @objc func previousImage() {
        mCollectionView.scrollToItem(at: IndexPath.init(item: numberMonth-1, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
        
    }
    
    @IBAction func segmentControl(_ sender: Any) {
        mCollectionView.reloadData()
    }
    
    @objc func buttonAction(){}
    
    //    action upload photo
    @objc func choosePhoto(_ sender: Any) {
        self.showAttachmentActionSheet(vc: self)
    }
    
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc
        
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        if let pop = actionSheet.popoverPresentationController {
            pop.sourceView = vc.view
            pop.sourceRect = CGRect(x:vc.view.bounds.size.width / 2.0 , y: vc.view.bounds.size.height, width: 1.0, height: 1.0)
        }
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.allowsEditing = true
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.allowsEditing = false
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - FILE PICKER
    func documentPicker(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        currentVC?.present(importMenu, animated: true, completion: nil)
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
}

extension MyBellyViewController : UIDocumentPickerDelegate, UICollectionViewDelegateFlowLayout, CropViewControllerDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:true, completion: nil)
    }
    
   
    func saveImage(imageName: String, image: UIImage) {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
    }
    
    class func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.imagePickedBlock?(image)
        self.saveImage( imageName:"Tháng \(numberMonth)" , image: image)
        self.localBelliesItem["Tháng \(numberMonth)" ] = image
        self.showHud()
        self.hud?.mode = .determinateHorizontalBar
        self.hud?.progress = 0
        mUpdateDataWhenAppear = false
        NetworkManager.shareInstance.apiUploadImageMyBellies(month: numberMonth, image: image, updateProgress: {
            (progress) -> Void in
            self.hud?.progress = progress
            print(progress)
        }) { (data, message, isSuccess) in
            let jdata = JSON(data)
            if ( !isSuccess) {
                var message = jdata["message"].stringValue
                if message.count == 0 {
                    message = "Vui lòng thử lại sau"
                }
                self.alert(message)
                self.hideHud()
            }
            self.hudShowCount = 1
            self.mUpdateDataWhenAppear = true
            self.hideHud()
            self.loadMybelliesBy()
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            currentVC?.dismiss(animated: true, completion: nil)
            let image: UIImage = image//Load an image
            
            let cropViewController = CropViewController(image: image)
            cropViewController.delegate = self
            present(cropViewController, animated: true, completion: nil)
        } else{
            print("Something went wrong in  image")
        }
    }
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
    //MARK: - FILE IMPORT DELEGATE
    
    func documentMenubelly(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerbelly(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url", url)
        self.filePickedBlock?(url)
    }
    
    //    Method to handle cancel action.
    func documentPickerWasCancelledbelly(_ controller: UIDocumentPickerViewController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    
}
