//
//  SizeGuideTableViewCell.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 10/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class SizeGuideTableViewCell: UITableViewCell {

    @IBOutlet weak var imgSizeGuide: UIImageView!
    @IBOutlet weak var lblSizeGuide: UILabel!
    @IBOutlet weak var lbl1SizeGuide: UILabel!
    @IBOutlet weak var lbl2SizeGuide: UILabel!
    @IBOutlet weak var lbl3SizeGuide: UILabel!
//    @IBOutlet weak var lbl4SizeGuide: UILabel!
    
    @IBOutlet weak var noiDuoi: UIView!
    @IBOutlet weak var noiTren: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgSizeGuide.layer.cornerRadius = imgSizeGuide.frame.size.width / 2
        imgSizeGuide.clipsToBounds = true
        imgSizeGuide.layer.borderColor = #colorLiteral(red: 0.6988443732, green: 0.4203152657, blue: 0.08633015305, alpha: 1)
        imgSizeGuide.layer.borderWidth = 7
        
        lblSizeGuide.adjustsFontSizeToFitWidth = true
        lblSizeGuide.minimumScaleFactor = 0.2
        
        lbl1SizeGuide.adjustsFontSizeToFitWidth = true
        lbl1SizeGuide.minimumScaleFactor = 0.2
        
        lbl2SizeGuide.adjustsFontSizeToFitWidth = true
        lbl2SizeGuide.minimumScaleFactor = 0.2
        
        lbl3SizeGuide.adjustsFontSizeToFitWidth = true
        lbl3SizeGuide.minimumScaleFactor = 0.2
        
//        lbl4SizeGuide.adjustsFontSizeToFitWidth = true
//        lbl4SizeGuide.minimumScaleFactor = 0.2
        if IS_IPHONE_5 {
            lblSizeGuide.font = UIFont.boldSystemFont(ofSize: 17)
            lbl1SizeGuide.font = UIFont.boldSystemFont(ofSize: 17)
            lbl2SizeGuide.font = UIFont.systemFont(ofSize: 12)
            lbl3SizeGuide.font = UIFont.systemFont(ofSize: 12)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
