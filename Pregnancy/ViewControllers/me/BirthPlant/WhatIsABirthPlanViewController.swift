//
//  WhatIsABirthPlanViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/18/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import WebKit

class WhatIsABirthPlanViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var mWebVBirthPlan: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "info"), style: .done, target: self, action: #selector(call_Method))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.setTitle(title: "Kế hoạch sinh con là gì")
        
        let htmlPath = Bundle.main.path(forResource: "test", ofType: "html")
        let url = URL(fileURLWithPath: htmlPath!)
        let req = URLRequest(url: url)
        mWebVBirthPlan.loadRequest(req)
        mWebVBirthPlan.delegate = self
    }
    
    @objc func call_Method(sender: AnyObject) {
        self.performSegue(withIdentifier: "WhatIsABirthPlan_to_Info", sender: self)
    }
    


}
