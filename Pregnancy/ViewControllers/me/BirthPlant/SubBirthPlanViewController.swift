//
//  SubBirthPlanViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubBirthPlanViewController: BaseViewController {

    @IBOutlet weak var mSubBirthPlanTableView: UITableView!
    
//    struct BirthPlantCheck {
//        var birth_plan_type_id = 0
//        var userId = 0
//    }
    // Plans type truyền qua
    var birthPlanData : [PregMyBirthPlanType] = [PregMyBirthPlanType]()
    
    var titleBar : String = ""
    var brithPlantTypeID = 0
    var bithPlantItem = [PregMyBirthPlanItem]()
    var bithPlantCheckbox = [BirthPlansCheck]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableview()
        
        self.setTitle(title: titleBar)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if bithPlantItem.isEmpty == false {
            bithPlantItem.removeAll()
        }
        if bithPlantCheckbox.isEmpty == false {
            bithPlantCheckbox.removeAll()
        }
        self.loadBirthPlantItems()
        self.loadBirthPlan()
    }
    
    func setupTableview() {
        let nib = UINib(nibName: "TodoTableViewCell", bundle: Bundle.main)
        mSubBirthPlanTableView.register(nib, forCellReuseIdentifier: "TodoCell")
        
        mSubBirthPlanTableView.delegate = self
        mSubBirthPlanTableView.dataSource = self
        self.mSubBirthPlanTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.mSubBirthPlanTableView.rowHeight = UITableView.automaticDimension
        self.mSubBirthPlanTableView.estimatedRowHeight = 120
        
    }

    // Lấy thông tin plant item theo type
    private func loadBirthPlantItems() {
        
        if let pregnancy = DatabaseManager.sharedManager.getLocalUserInfo() {
            NetworkManager.shareInstance.apiGetMyBirthPlansItem(birthPlanTypeID: self.brithPlantTypeID) { (data, message, isSuccess) in
                self.bithPlantItem = data
                DispatchQueue.main.async {
                    self.mSubBirthPlanTableView.reloadData()
                }
            }
        } else {
            self.alert("Không lấy được dữ liệu người dùng. Vui lòng đăng nhập lại", title: "Thông báo") { (action) in
                DatabaseManager.sharedManager.logout {
                    
                }
            }
        }
    }
    
    // Lấy thông tin plant item mà người dùng đã tick
    func loadBirthPlan() {
        print(self.hudShowCount)
        self.showHud()
        NetworkManager.shareInstance.apiGetMyBirthPlans { (data, message, isSuccess) in
            if(isSuccess) {
                let result = JSON(data)
                for items in result.arrayValue {
                    var item  = BirthPlansCheck()
                    item.birth_plan_item_id = items["my_birth_plan_item_id"].intValue
                    item.userId = items["user_id"].intValue
                    self.bithPlantCheckbox.append(item)
                }
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mSubBirthPlanTableView.reloadData()
                }
                
            } else {
                DispatchQueue.main.async {
                    self.hideHud()
                    self.mSubBirthPlanTableView.reloadData()
                }
            }
        }
    }
    
    @objc func addButtonAction(){
        print("click button")
        self.performSegue(withIdentifier: "subBirthPlan_to_Addwish", sender: self)
    }
    
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        var barItems = super.createRightBarButtonItems()
        
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "add"), for: .normal)
        button1.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        button1.frame = CGRect(x: self.view.frame.size.width, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button1)
        
        barItems.insert(barButton, at: 0)
        return barItems
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "subBirthPlan_to_Addwish" {
            let viewController = segue.destination as! AddWishViewController
            viewController.passedTitle = titleBar
//            viewController.bithPlantItem = self.bithPlantItem
            viewController.birthPlanData = self.birthPlanData
        }
        
    }
    
    func postBirthPlans(item_id : Int) {
        NetworkManager.shareInstance.apiPostMyBirthPlans(my_birth_plan_item_id: item_id) { (data, message, isSuccess) in
            if (isSuccess) {
                print(isSuccess)
                print("postBirthPlans : \(item_id)")
                if self.bithPlantCheckbox.isEmpty == false {
                    self.bithPlantCheckbox.removeAll()
                }
                self.loadBirthPlan()
            }
            
        }
    }
    func deleteBirthPlans(item_id : Int) {
        NetworkManager.shareInstance.apiDeleteMyBirthPlans(itemId: item_id) { (data, message, isSuccess) in
            print(isSuccess)
            if (isSuccess) {
                print("deleteBirthPlans : \(item_id)")
                if self.bithPlantCheckbox.isEmpty == false {
                    self.bithPlantCheckbox.removeAll()
                }
                self.loadBirthPlan()
            }
            
        }
    }
}

extension SubBirthPlanViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.bithPlantItem.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.bithPlantItem.count == 0 {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoTableViewCell
        cell.selectionStyle = .none
        
        cell.toDolabel.text = self.bithPlantItem[indexPath.row].item_content
        
        cell.toDoImage.image = UIImage(named: "untick")
        
        for check in self.bithPlantCheckbox {
            if check.birth_plan_item_id == self.bithPlantItem[indexPath.row].id {
                print("tick-3")
                print(self.bithPlantItem[indexPath.row].id)
                cell.toDoImage.image = UIImage(named: "tick-3")
                break
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var checkValue = false
        for check in self.bithPlantCheckbox {
            if check.birth_plan_item_id == self.bithPlantItem[indexPath.row].id {
                checkValue = true
                break
            }
        }
        if checkValue == true {
            self.deleteBirthPlans(item_id: self.bithPlantItem[indexPath.row].id)
        } else {
            self.postBirthPlans(item_id: self.bithPlantItem[indexPath.row].id)
        }
    }
    
    
}
