//
//  BirthPlanTableViewCell.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/18/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class BirthPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var mImgBirthPlanCell: UIImageView!
    @IBOutlet weak var mLbBirthPlanCell: UILabel!
    @IBOutlet weak var mSubLbBirthPlanCell: UILabel!
    @IBOutlet weak var mBackground: UIView!
    @IBOutlet weak var mLineBottom: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
