//
//  TodoTableViewCell.swift
//  #10cellView
//
//  Created by Ngoc Dung on 04/12/2018.
//  Copyright © 2018 Ngoc Dung. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell {

    @IBOutlet weak var toDolabel: UILabel!
    @IBOutlet weak var toDoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        toDoImage.layer.cornerRadius = toDoImage.frame.size.width / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
