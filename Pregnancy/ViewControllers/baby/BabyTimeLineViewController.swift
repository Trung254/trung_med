//
//  BabyTimeLineViewController.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/24/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import WebKit
import SnapKit
import SwiftyJSON

class BabyTimeLineViewController: BaseViewController , UIScrollViewDelegate {

    @IBOutlet weak var mImage: UIImageView!
    @IBOutlet weak var mScrollView: UIScrollView!
    var ratio: Float?
    var imageWidth:Float?
    var startDate:Date?
    var cellWidth: Float?
    var paddingLeft: Float = 0
    @IBOutlet weak var mImageHeight: NSLayoutConstraint!
    var isScrollTL: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideTabBar(hidden: true, animated: false)
        self.view.setNeedsLayout()
        let barHeight = UIApplication.shared.statusBarFrame.size.height
        if let nav = self.navigationController, let window = UIApplication.shared.windows.first {
            let navHeight = nav.navigationBar.frame.height
            let screenHeight = window.frame.size.height
            self.mImageHeight.constant = screenHeight - barHeight - navHeight
            self.view.layoutIfNeeded()
            self.setImageDimension()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isScrollTL {
            self.setImageDimension()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func setImageDimension() {
        self.imageWidth = Float(self.mImageHeight.constant/790*2792)
        self.ratio = (imageWidth! / 2792)
        self.paddingLeft = 222 * self.ratio!
        let check = paddingLeft - Float(self.view.width/2)
        if check > 0 {
            self.cellWidth = (imageWidth! - paddingLeft * 2 )/41
//            self.cellWidth = 57 * ratio!
        }
        else {
            self.cellWidth = (imageWidth! - paddingLeft * 2 )/42
//            self.cellWidth = 57 * ratio!
        }
        self.loadDataStartDate()
        isScrollTL = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.hideTabBar(hidden: false, animated: false)
    }
    
    private func loadDataStartDate() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllPregnancyDueDate() {
            (data, message, isSuccess) in
            if(isSuccess) {
                let data = data as! JSON
                self.startDate = self.transferStringToDate(data[0]["start_date"].stringValue, "yyyy-MM-dd'T'HH:mm:ss")
                self.showCurDate(data[0]["show_week"].intValue)
            }
            else {
                self.mImage.image = UIImage(named: "timeline-1")
            }
            
            self.hideHud()
        }
    }
    
    private func showCurDate(_ show_week : Int) {
        if let pregnancy = DatabaseManager.sharedManager.getLocalPregnancys() {
            if pregnancy.baby_already_born == 1 {
                self.mScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
                self.scrollViewDidScroll(self.mScrollView)
                self.mImage.image = UIImage(named: "timeline-1")
            } else {
                let weeks = Calendar.current.dateComponents([.weekOfYear], from: startDate!, to: Date()).weekOfYear
                
                if show_week == 1{
                    let offsetX = Float(min(max(weeks! - 0, 0),40) ) * cellWidth!
                    self.mScrollView.setContentOffset(CGPoint(x: Int(offsetX), y: 0), animated: false)
                    if (offsetX == 0 ) {
                        self.scrollViewDidScroll(self.mScrollView)
                    }
                    self.mImage.image = UIImage(named: "timeline-1")
                } else {
                    let offsetX = Float(min(max(weeks! - 1, 0),40) ) * cellWidth!
                    self.mScrollView.setContentOffset(CGPoint(x: Int(offsetX), y: 0), animated: false)
                    if (offsetX == 0 ) {
                        self.scrollViewDidScroll(self.mScrollView)
                    }
                    self.mImage.image = UIImage(named: "timeline-1")
                }
            }
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetScroll = Float(scrollView.contentOffset.x)
        let week = ceilf((offsetScroll + cellWidth! ) / cellWidth!)
        self.title = "Tuần \(min(Int(week),41))"
    }
}
