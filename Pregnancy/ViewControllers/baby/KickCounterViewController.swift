//
//  KickCounterViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/15/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift



class KickCounter: Object {
    @objc dynamic var kick_time = "" // Thời gian đạp
    @objc dynamic var kick_results_id = 0
    @objc dynamic var eslapedTime = 0
    @objc dynamic var dataKickCounter = "" // Số lần đạp
    @objc dynamic var status = 0
    @objc dynamic var user_id = 0
    @objc dynamic var start_date = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    @objc dynamic var end_date = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
}




class KickCheckImageView: UIImageView {
    
    var mIsActive : Bool = false
    var mImage : UIImage = UIImage(named: "tick-2")!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(red: 242/255.0, green: 222/255.0, blue: 222/255.0, alpha: 1.0).cgColor
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
    
    func setActive(_ isActive: Bool) {
        if(isActive) {
            self.image = mImage
        } else {
            self.image = nil
        }
        self.mIsActive = isActive
    }
    
}

struct KickCounters {
    var kick_time: String
    var elapsed_time: Int
    
    // Constructor.
    init(kick_time: String, elapsed_time:Int) {
        self.kick_time = kick_time;
        self.elapsed_time = elapsed_time;
    }
}

class KickCounterViewController: BaseViewController {

    @IBOutlet weak var kickViewBiggest: UIView!
    @IBOutlet weak var mKickBtn: UIButton!
    @IBOutlet weak var mKickView: UIView!
    @IBOutlet weak var mEndBtnBackgroundView: UIView!
    @IBOutlet weak var mEndBtn: UIButton!
    @IBOutlet weak var mRestartBtnBackgroundView: UIView!
    @IBOutlet weak var mRestartBtn: UIButton!
    @IBOutlet weak var mStartView: UIView!
    @IBOutlet weak var mStartTimeLbl: UILabel!
    @IBOutlet weak var mCountDownLbl: UILabel!
    
    @IBOutlet weak var animationBackgroundView: UIView!
    
    @IBOutlet var mCircleImages: [KickCheckImageView]!
    
    var mIsCounting: Bool = false
    var mStartDate:Date?
    var mEndDate:Date?
    var mTimer: Timer?
    let dateFormatter: DateFormatter = DateFormatter()
    var eslapedTime = 0
    var dataKickCounter: [KickCounters] = []
    var kick_results_id = 0
    
    // Data xử lý thời gian lưu
    let realm = try! Realm()
    var kickList: Results<KickCounter>!
    
    var userID = 0
    var pushToAppDelegate = false
    
    
    // Share Circle
    let timeLeftShapeLayer = CAShapeLayer()
    let bgShapeLayer = CAShapeLayer()
    var timeLeft: TimeInterval = 7200 // Thời gian đếm lùi update cho animation
    var endTime: Date?
    var timer = Timer()
    let strokeIt = CABasicAnimation(keyPath: "strokeEnd")
    var startAngle = -90.degreesToRadians
    var endAngle = 270.degreesToRadians
    
    var stoptimer: Date?
    
    // Góc bắt đầu để vẽ :D
    var cornerBegin = 0
    let bgShapreBegin = CAShapeLayer()
    let shapreBegin = CAShapeLayer()
    
//    let shapeLayer = CAShapeLayer()
    
    var flagResumeCircle = false
    
    var secondsFromGMT: Int { return TimeZone.current.secondsFromGMT() }
    
    let currentTimeDate = Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        mKickView.layer.borderColor = UIColor(red: 139/255.0, green: 97/255.0, blue: 97/255.0, alpha: 1.0).cgColor
        mKickView.layer.borderWidth = 6
        mKickView.layer.cornerRadius = mKickView.frame.width / 2
        mKickBtn.layer.cornerRadius = mKickBtn.frame.width / 2
        animationBackgroundView.layer.cornerRadius = animationBackgroundView.frame.width / 2
        mKickView.backgroundColor = UIColor.clear
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        self.roundRectLayer(self.mEndBtnBackgroundView)
        self.roundRectLayer(self.mRestartBtnBackgroundView)
        self.addBackgroundImage("baby-background-blur")
//        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
//            if let pregnancy = pregnancy {
//                self.userID = pregnancy.id
//            }
//
//        }
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            self.userID = user.id
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        if (self.mIsCounting == false){
            self.stopCounting()
        }
        
//        if dataKickCounter.count >= 0{
//            if (self.mTimer == nil){
//                self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounter), userInfo:nil, repeats: true)
//            }
//        }
        
        bgShapreBegin.removeFromSuperlayer()
        shapreBegin.removeFromSuperlayer()
        timeLeftShapeLayer.removeFromSuperlayer()
        bgShapeLayer.removeFromSuperlayer()
        timeLeftShapeLayer.removeAllAnimations()
        bgShapeLayer.removeAllAnimations()
        
        
        if (DatabaseManager.sharedManager.isGuest()) {
            self.prompt("Thông báo", message: "Bạn cần phải đăng nhập", okHandler: { (alert) in
                DatabaseManager.sharedManager.logout {}
            }) { (alertAction) in
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        self.timeLeft = 7200
        self.kickList = self.realm.objects(KickCounter.self).filter("user_id == %@", self.userID)
        
        if kickList.isEmpty == false {
            
            self.mRestartBtn.isEnabled = false
            if (self.kickList.last?.kick_time) != nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
                self.mStartDate = dateFormatter.date(from: self.kickList.last!.kick_time)
            }
            self.dataKickCounter.removeAll()
            
            // Gán giá trị vào dataKickCounter
            if kickList.count >= 2 {
                self.mRestartBtn.isEnabled = true
                
                for i in 1..<kickList.count {
                    self.dataKickCounter.append(KickCounters(kick_time: kickList[i].kick_time, elapsed_time: kickList[i].eslapedTime))
                    
                    self.mCircleImages[i-1].setActive(true)
                }
            }
            
            self.mIsCounting = true
            self.mEndDate = kickList.first?.end_date
            self.mStartDate = kickList.first?.start_date
            self.kick_results_id = (kickList.first?.kick_results_id)!
            
            // Tính thời gian từ khi start kick và khi vào lại app
            
            var currentTimeDatelastest = (try?(Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))))!
            
            
            let time = Calendar.current.dateComponents([.second], from: self.mStartDate!, to: currentTimeDatelastest)
            
            if let time = time.second {
                self.cornerBegin = (time * 360) / 7200
                self.timeLeft = self.timeLeft - Double(time)
            }
        }
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if kickList.isEmpty == false {
            self.drawBeginShape()
            self.setupViewWhenHaveData()
            if (self.mTimer == nil){
                self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounter), userInfo:nil, repeats: true)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        deinit {
            NotificationCenter.default.removeObserver(self)
//        }
    }
    func drawBeginShape() {
        if self.cornerBegin != 0 {
            bgShapreBegin.path =  UIBezierPath(arcCenter: CGPoint(x: self.mKickBtn.frame.midX , y: self.mKickBtn.frame.midY), radius:
                36, startAngle: self.startAngle, endAngle: self.startAngle + self.cornerBegin.degreesToRadians, clockwise: true).cgPath
            bgShapreBegin.lineWidth = 68
            bgShapreBegin.fillColor = UIColor.clear.cgColor
            bgShapreBegin.strokeColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
            
            self.mKickView.layer.addSublayer(bgShapreBegin)
            
            // Tạo 1 shapre đến điểm bắt đầu của cornerShapre
            
//            let shapreBegin = CAShapeLayer()
            shapreBegin.path =  UIBezierPath(arcCenter: CGPoint(x: self.mKickView.frame.midX , y: self.mKickView.frame.midY), radius:
                77, startAngle: self.startAngle, endAngle:self.startAngle + self.cornerBegin.degreesToRadians, clockwise: true).cgPath
            shapreBegin.strokeColor = #colorLiteral(red: 0.02721658349, green: 0.5905672908, blue: 1, alpha: 1)
            shapreBegin.fillColor = UIColor.clear.cgColor
            shapreBegin.lineWidth = 6
            self.kickViewBiggest.layer.addSublayer(shapreBegin)
        }
    }
    
    func setupViewWhenHaveData() {
        
        if flagResumeCircle == false {
            self.createCircle()
            flagResumeCircle = true
        }
        
        self.dateFormatter.dateFormat = "HH:mm"
        self.mEndBtn.isEnabled = true
//        self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounter), userInfo:nil, repeats: true)
        self.drawBeginShape()
        self.mEndBtn.isEnabled = true
        let time = self.dateFormatter.string(from:self.mStartDate!)
        self.mStartTimeLbl.text = time
        self.mStartView.isHidden = false
        self.mRestartBtn.isEnabled = true
        if self.dataKickCounter.count == 0 {
            if self.mStartDate != nil {
                
                if self.pushToAppDelegate == false {
                        self.flagResumeCircle = true
                        self.checkCounter()
                }
            }
        }
        if self.dataKickCounter.count > 0 {
        if self.mStartDate != nil {
            
            if self.pushToAppDelegate == false {
                
                self.prompt("Thông báo", message: "Bạn có muốn tiếp tục ghi nhận lần đạp chân này", okTitle: "Dừng", okHandler: { (action) in
                    self.updateKickResults(self.kick_results_id, self.convertDateToString(), self.eslapedTime)
                    var count = 1
                    
                    if self.dataKickCounter.count != 0 {
                        for item in self.dataKickCounter {
                            self.pushKickResultDetail(self.kick_results_id, count, item.kick_time, item.elapsed_time)
                            count += 1
                        }
                    }
                    self.flagResumeCircle = false
                    self.pause(self.timeLeftShapeLayer)
                    self.pause(self.bgShapeLayer)
                    if count == self.dataKickCounter.count + 1 {
                        
                        // Xóa data trong Realm khi người dùng chấp nhận dừng
                        try! self.realm.write({
                            self.realm.delete(self.kickList.self)
                        })
//                        self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
                        self.pushToKickDetail()
                        self.stopCounting()
                    }
                }, cancelTitle: "Tiếp tục") { (action) in
                    self.flagResumeCircle = true
                    self.checkCounter()
                }
            }
        }
        }
    }
    
    @objc func applicationDidBecomeActive(_ notification: Notification) {
        if self.kickList.isEmpty == false {
            // Vẽ lại khi active lại app nếu có dữ liệu
            bgShapreBegin.removeFromSuperlayer()
            shapreBegin.removeFromSuperlayer()
            timeLeftShapeLayer.removeFromSuperlayer()
            bgShapeLayer.removeFromSuperlayer()
            timeLeftShapeLayer.removeAllAnimations()
            bgShapeLayer.removeAllAnimations()
            
            self.timeLeft = 7200
            
            self.mRestartBtn.isEnabled = false
            if (self.kickList.last?.kick_time) != nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
                self.mStartDate = dateFormatter.date(from: self.kickList.last!.kick_time)
            }
            self.dataKickCounter.removeAll()
            
            // Gán giá trị vào dataKickCounter
            if kickList.count >= 2 {
                self.mRestartBtn.isEnabled = true
                
                for i in 1..<kickList.count {
                    self.dataKickCounter.append(KickCounters(kick_time: kickList[i].kick_time, elapsed_time: kickList[i].eslapedTime))
                    
                    self.mCircleImages[i-1].setActive(true)
                }
            }
            
            self.mIsCounting = true
            self.mEndDate = kickList.first?.end_date
            self.mStartDate = kickList.first?.start_date
            self.kick_results_id = (kickList.first?.kick_results_id)!
            
            // Tính thời gian từ khi start kick và khi vào lại app
            var currentTimeDatelastest = (try?(Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))))!
            
            
            let time = Calendar.current.dateComponents([.second], from: self.mStartDate!, to: currentTimeDatelastest)
            
            if let time = time.second {
                self.cornerBegin = (time * 360) / 7200
                self.timeLeft = self.timeLeft - Double(time)
            }
            
            // -> Vẽ và chạy animation nào
            
            self.drawBeginShape()
            self.setupViewWhenHaveData()
            
            // Khi active lại app thì chạy vào đây, check xem có đúng vo
//            self.prompt("Thông báo", message: "Bạn có muốn tiếp tục ghi nhận lần đạp chân này", okTitle: "Dừng", okHandler: { (alert) in
//                self.updateKickResults(self.kick_results_id, self.convertDateToString(), self.eslapedTime)
//                if self.dataKickCounter.count != 0 {
//                    var count = 1
//                    for item in self.dataKickCounter {
//                        self.pushKickResultDetail(self.kick_results_id, count, item.kick_time, item.elapsed_time)
//                        count += 1
//                    }
//                    self.flagResumeCircle = false
//                    self.pause(self.timeLeftShapeLayer)
//                    self.pause(self.bgShapeLayer)
//                    if count == self.dataKickCounter.count + 1 {
//                        self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
//                    }
//                } else {
//                    self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
//                }
//                self.stopCounting()
//            }, cancelTitle: "Tiếp tục") { (alert) in
//                // Tiếp tục -> chạy tiếp animation trong này
//            }
        }
    }
    
    func roundRectLayer(_ view : UIView ) -> Void {
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor(red: 115/255.0, green: 76/255.0, blue: 47/255.0, alpha: 0.5).cgColor
        view.layer.masksToBounds = true
        view.layer.cornerRadius = view.frame.size.width/2
    }
    
    @IBAction func doKick(_ sender: Any) {
        if flagResumeCircle == false {
            self.createCircle()
            flagResumeCircle = true
        }
        
        if(self.mIsCounting) {
            self.increaseCount()
            if dataKickCounter.count < 11 {
                
                // Ghi kick counter vao realm
                let kick = KickCounter()
                kick.dataKickCounter = String(dataKickCounter.count + 1)
                kick.kick_time = convertDateToString()
                kick.status = 0
                kick.user_id = self.userID
                kick.eslapedTime = self.eslapedTime
                kick.kick_results_id = self.kick_results_id
                
                try! self.realm.write {
                    realm.add(kick)
                }
                

//                self.pushKickResultDetail(self.kick_results_id, dataKickCounter.count, convertDateToString(), eslapedTime)
                self.showHud()
                NetworkManager.shareInstance.apiPostKickResultsDetail(kick_result_id: self.kick_results_id, kick_order: dataKickCounter.count + 1, kick_time: convertDateToString(), elapsed_time: eslapedTime) {
                    (data, message, isSuccess) in
                    
                    if isSuccess {
                        DispatchQueue.main.async {
                            self.hideHud()
                            self.dataKickCounter.append(KickCounters(kick_time: self.convertDateToString(),elapsed_time: self.eslapedTime))
                        }
                    } else if (message == "Authorization has been denied for this request." ){
                        let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            DatabaseManager.sharedManager.logout {
                            }
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                            self.hideHud()
                        }))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.hideHud()
                        self.alert("Vui lòng kiểm tra lại kết nối")
                    }
                }
            }
        } else {
            self.mStartDate = Date.init().addingTimeInterval(TimeInterval(self.secondsFromGMT))
            self.mEndDate = self.mStartDate?.addingTimeInterval(7200)
            dateFormatter.dateFormat = "HH:mm"
            let time = dateFormatter.string(from:self.mStartDate!)
            self.mStartTimeLbl.text = time
            self.mStartView.isHidden = false
            
//            self.pushKickResults(kick_date: convertDateToString(), duration: 0)
            self.dataKickCounter = []
            
            self.startCountDown(kick_date: convertDateToString(), duration: 0)
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        UIDevice.vibrate()
    }
    
    func startCountDown(kick_date : String, duration : Float) -> Void {
        
        
        self.showHud()
        NetworkManager.shareInstance.apiPostKickResults(kick_date: kick_date, duration: duration) {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                self.kick_results_id = data["id"].intValue
                
                let kick = KickCounter()
                kick.dataKickCounter = String(self.dataKickCounter.count + 1)
                kick.kick_time = self.convertDateToString()
                kick.status = 0
                kick.user_id = self.userID
                kick.eslapedTime = self.eslapedTime
                kick.kick_results_id = self.kick_results_id
                if let start_date = self.mStartDate {
                    kick.start_date = start_date
                    kick.end_date = self.mEndDate ?? start_date.addingTimeInterval(7200)
                }
                
                try! self.realm.write {
                    self.realm.add(kick)
                }
                
                
                if(!self.mIsCounting) {
                    self.mIsCounting = true
                    self.mEndBtn.isEnabled = true
                    self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounter), userInfo:nil, repeats: true)
                    
                    
                }
                
                DispatchQueue.main.async {
                    self.hideHud()
                }
            } else if (message == "Authorization has been denied for this request." ){
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            }

            else {
                self.alert("Không tạo được Đạp chân em bé. Vui lòng thử lại sau", title: "Thông báo", handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                })
            }
            
            
        }
        
        
        
    }
    
    @objc func checkCounter() -> Void {
        if dataKickCounter.count >= 0{
            if (self.mTimer == nil){
                self.mTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkCounter), userInfo:nil, repeats: true)
            }
        }
        
            var time = self.mEndDate!.timeIntervalSince(Date.init().addingTimeInterval(TimeInterval(self.secondsFromGMT)))
        
        
        var currentTimeDatelastest = (try?(Date.init().addingTimeInterval(TimeInterval(TimeZone.current.secondsFromGMT()))))!
        
        
        let time1 = Calendar.current.dateComponents([.second], from: self.mStartDate!, to: currentTimeDatelastest)
        if let time2 = time1.second {
            self.cornerBegin = (time2 * 360) / 7200
            self.drawBeginShape()
            self.timeLeft = self.timeLeft - Double(time)
        }
        
        
        if(time < 0) {
            self.stopCounting()
            time = 7200
        }
        self.setCountDowntime(time)
        eslapedTime = Int((7200 - time) * 1000)
        if Int(time) <= 0 {
//            self.flagResumeCircle = false
            let alertController = UIAlertController(title: "Hết thời gian đạp của bé", message: "Con của bạn đã không di chuyển 10 lần trong 2 giờ. Đừng lo sợ.", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Kết thúc", style: UIAlertAction.Style.default) {
                UIAlertAction in
                
                self.updateKickResults(self.kick_results_id, self.convertDateToString(), 7200*1000)
//                if self.dataKickCounter.count != 0 {
//                    var count = 0
//                    self.showHud()
//                    for item in self.dataKickCounter {
////                        self.pushKickResultDetail(self.kick_results_id, count, item.kick_time, item.elapsed_time)
//                        count += 1
//                        NetworkManager.shareInstance.apiPostKickResultsDetail(kick_result_id: self.kick_results_id, kick_order: count, kick_time: item.kick_time, elapsed_time: item.elapsed_time) {
//                            (data, message, isSuccess) in
//                            if isSuccess {
//                                DispatchQueue.main.async {
//                                    if count == self.dataKickCounter.count {
//                                        self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
//                                        self.hideHud()
//                                    }
//                                }
//                            }
//                        }
//                    }
                try! self.realm.write({
                    self.realm.delete(self.kickList.self)
                })
                
//                self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
                self.pushToKickDetail()
                self.flagResumeCircle = false
                self.pause(self.timeLeftShapeLayer)
                self.pause(self.bgShapeLayer)
//                    if count == self.dataKickCounter.count + 1 {
//                        self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
//                    }
//                }
                
                self.stopCounting()
            }
            
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    func setCountDowntime(_ timeInterval:TimeInterval) -> Void {
        let timeString = self.stringFromTimeInterval(interval: timeInterval)
        self.mCountDownLbl.text = timeString
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    func stopCounting() {
        self.mTimer?.invalidate()
        self.mIsCounting = false
        for image in self.mCircleImages {
            image.setActive(false)
        }
        self.mStartView.isHidden = true
        self.setCountDowntime(7200)
        self.mEndBtn.isEnabled = false
        self.mRestartBtn.isEnabled = false
        self.cornerBegin = 0
        
        // Dừng thì xóa hết shapre và data trong realm
        bgShapreBegin.removeFromSuperlayer()
        shapreBegin.removeFromSuperlayer()
        timeLeftShapeLayer.removeFromSuperlayer()
        bgShapeLayer.removeFromSuperlayer()
        timeLeftShapeLayer.removeAllAnimations()
        bgShapeLayer.removeAllAnimations()
        if ((kickList.self) != nil){
        try! self.realm.write {
            self.realm.delete(kickList.self)
        }
        }
        
    }
    
    func stopCountingNew() {
        self.mTimer?.invalidate()
        self.mIsCounting = false
//        for image in self.mCircleImages {
//            image.setActive(false)
//        }
        self.mStartView.isHidden = false
//        self.setCountDowntime(7200)
        self.mEndBtn.isEnabled = false
        self.mRestartBtn.isEnabled = false
//        self.cornerBegin = 0
        
        bgShapreBegin.removeFromSuperlayer()
        shapreBegin.removeFromSuperlayer()
        timeLeftShapeLayer.removeFromSuperlayer()
        bgShapeLayer.removeFromSuperlayer()
        timeLeftShapeLayer.removeAllAnimations()
        bgShapeLayer.removeAllAnimations()
        if ((kickList.self) != nil){
            try! self.realm.write {
                self.realm.delete(kickList.self)
            }
        }
        
    }
    
    func increaseCount() -> Void {
        for image in self.mCircleImages {
            if (image.mIsActive == false) {
                image.setActive(true)
                self.mRestartBtn.isEnabled = true
                if(self.mCircleImages.lastIndex(of: image) == self.mCircleImages.count - 1) {
                    self.mTimer?.invalidate()
                    self.alert("Chúng tôi đã lưu được 10 lần đạp trong vòng 2 giờ. Bạn có thể bắt đầu 1 phiên mới bất kỳ lúc nào", title: "Đã lưu!") { (alert) in
//                        if self.dataKickCounter.count != 0 {
                        self.updateKickResults(self.kick_results_id, self.convertDateToString(), self.eslapedTime)
                        
                        //                            var count = 1
                        //                            for item in self.dataKickCounter {
                        //                                self.pushKickResultDetail(self.kick_results_id, count, item.kick_time, item.elapsed_time)
                        //                                count += 1
                        //                            }
                        // Xóa lưu kick counter list
                        try! self.realm.write({
                            self.realm.delete(self.kickList.self)
                        })

//                        }
                        
                        self.flagResumeCircle = false
                        self.pause(self.timeLeftShapeLayer)
                        self.pause(self.bgShapeLayer)
//                        self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
                        self.pushToKickDetail()
                        self.stopCounting()
                    }
                }
                break
            }
        }
    }
    
    func pushToKickDetail() {
        
        let sb = UIStoryboard.init(name: "Baby", bundle: nil)
        let detailVC = sb.instantiateViewController(withIdentifier: "kickDetailViewController") as! KickDetailViewController
        detailVC.kick_id = self.kick_results_id
        let historyVC = sb.instantiateViewController(withIdentifier: "kickHistoryViewController") as! KickHistoryViewController
        
        try! self.realm.write({
            self.realm.delete(self.kickList.self)
        })
        
        if let tabbar = ViewManager.getRootTabbarController() {
            if let vcs = tabbar.viewControllers{
                if let navC = vcs[2] as? RootNavigationController {
                    var desVcs = navC.viewControllers
                    desVcs.append(historyVC)
                    desVcs.append(detailVC)
                    navC.setViewControllers(desVcs, animated: true)
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "KickCounter_To_KickDetail" {
            
            try! self.realm.write({
                self.realm.delete(self.kickList.self)
            })
            
            let vc = segue.destination as! KickDetailViewController
            vc.kick_id = self.kick_results_id
        }
        if segue.identifier == "KickCounter_To_KickHistory"{
            let vc = segue.destination as! KickHistoryViewController
            vc.kickcouterCount = self.dataKickCounter.count
        }
    }
    
    @IBAction func doEndSession(_ sender: Any) {
        if self.dataKickCounter.count <= 0 {
//            let alert = UIAlertController(title: "Thông báo", message: "Bạn có muốn tiếp tục ghi nhận lần đạp chân này", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Dừng", style: .default, handler: { action in
//                self.stopCountingNew()
//            }))
//            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
//                self.hideHud()
//            }))
//            self.present(alert, animated: true, completion: nil)
            
            self.prompt("Thông báo", message: "Bạn có chắc chắn muốn kết thúc phiên này?", okTitle: "Dừng", okHandler: { (action) in
                self.updateKickResults(self.kick_results_id, self.convertDateToString(), self.eslapedTime)
                
                try! self.realm.write({
                    self.realm.delete(self.kickList.self)
                })
                
                self.stopCountingNew()
                self.flagResumeCircle = false
                self.pause(self.timeLeftShapeLayer)
                self.pause(self.bgShapeLayer)
                
            }, cancelTitle: "Tiếp tục") { (action) in
                self.flagResumeCircle = true
                self.checkCounter()
            }
        }
        
        self.prompt("Thông báo", message: "Bạn có chắc chắn muốn kết thúc phiên này?", okTitle: "Dừng", okHandler: { (action) in
            self.updateKickResults(self.kick_results_id, self.convertDateToString(), self.eslapedTime)
            //            if self.dataKickCounter.count != 0 {
            //                var count = 1
            //                for item in self.dataKickCounter {
            //                    self.pushKickResultDetail(self.kick_results_id, count, item.kick_time, item.elapsed_time)
            //                    count += 1
            //                }
            
            //            for item in self.kickList {
            //                try! self.realm.write({
            //                    self.realm.delete(item)
            //                })
            //            }
            try! self.realm.write({
                self.realm.delete(self.kickList.self)
            })
            
            
            
            self.stopCounting()
            self.flagResumeCircle = false
            self.pause(self.timeLeftShapeLayer)
            self.pause(self.bgShapeLayer)
            
//            self.performSegue(withIdentifier: "KickCounter_To_KickDetail", sender: self)
            self.pushToKickDetail()
            
        }, cancelTitle: "Tiếp tục") { (action) in
            self.flagResumeCircle = true
            self.checkCounter()
        }
    }
    
    @IBAction func doRestartSession(_ sender: Any) {
        
        
        self.prompt("Thông báo", message: "Bạn có chắc chắn muốn xóa lần đạp chân được ghi lại gần đây?", okTitle: "Xóa", okHandler: { (action) in
            NetworkManager.shareInstance.apiDeleteKickResultDetail(kick_result_id: self.kickList.first!.kick_results_id, kick_order: self.kickList.count - 1, callBack: { (data, message, isSuccess) in
                if (isSuccess) {
                    for image in self.mCircleImages.reversed() {
                        if(image.mIsActive) {
                            image.setActive(false)
                            break
                        }
                    }
                    self.dataKickCounter.removeLast()
                    
                    try! self.realm.write({
                        self.realm.delete(self.kickList.last!)
                    })
                    self.mRestartBtn.isEnabled = self.mCircleImages.first?.mIsActive ?? false
                    self.hideHud()
                } else {
                    self.hideHud()
                    self.alert("Không xóa được lần lưu gần nhất. Vui lòng thử lại sau")
                }
            })
        }, cancelTitle: "Không") { (action) in
            self.flagResumeCircle = true
            self.checkCounter()
        }
    }
    
    @objc func handleNavigationIcon(){
        performSegue(withIdentifier: "KickCounter_To_Info", sender: self)
        
    }
    @objc func addButtonAction(){
        performSegue(withIdentifier: "KickCounter_To_KickHistory", sender: self)
    }

    //MARK: this method is override
    override func createRightBarButtonItems() -> Array<UIBarButtonItem> {
        var barItems = super.createRightBarButtonItems()
        
        let addButton = UIButton(type: .custom)
        addButton.setImage(UIImage(named: "kick-history"), for: .normal)
        addButton.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        addButton.addTarget(self, action: #selector(addButtonAction), for: .touchUpInside)
        let barButton2 = UIBarButtonItem(customView: addButton)
        
        barItems.insert(barButton2, at: 0)
        return barItems
    }
    
    func convertDateToString() -> String {
        let Formatter = DateFormatter()
        Formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        Formatter.timeZone = TimeZone.init(abbreviation: "GMT+0:00")
        
        return Formatter.string(from: self.currentTimeDate)
    }
    
//    private func pushKickResults(kick_date: String, duration: Float) {
//        self.showHud()
//        NetworkManager.shareInstance.apiPostKickResults(kick_date: kick_date, duration: duration) {
//            (data, message, isSuccess) in
//            if isSuccess {
//                let data = data as! JSON
//                self.kick_results_id = data["id"].intValue
//            }
//
//            DispatchQueue.main.async {
//                self.hideHud()
//            }
//        }
//    }
    
    private func updateKickResults(_ kick_result_id: Int,_ kick_date: String,_ duration: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiPutKickResults(kick_result_id: kick_result_id, kick_date: kick_date, duration: duration) {
            (data, message, isSuccess) in
            if isSuccess {
            }
    
            else if ( message == "Authorization has been denied for this request."    ) {
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
    }
    
    private func pushKickResultDetail(_ kick_result_id: Int,_ kick_order: Int,_ kick_time: String,_ elapsed_time: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiPostKickResultsDetail(kick_result_id: kick_result_id, kick_order: kick_order, kick_time: kick_time, elapsed_time: elapsed_time) {
            (data, message, isSuccess) in
            if isSuccess {
            }
                
            else if ( message == "Authorization has been denied for this request."    ) {
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            DispatchQueue.main.async {
                self.hideHud()
            }
            
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
    }
    
    private func pushUserKickHistories(_ kick_result_id: Int,_ kick_date: String,_ duration: Int) {
        self.showHud()
        NetworkManager.shareInstance.apiPostUserKickHistories(kick_result_id: kick_result_id, kick_date: kick_date, duration: duration) {
            (data, message, isSuccess) in
            if isSuccess {
                 self.updateKickResults(kick_result_id, kick_date, duration)
            }
            
            else if ( message == "Authorization has been denied for this request."    ) {
                let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    DatabaseManager.sharedManager.logout {
                    }
                }))
                alert.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: { action in
                    self.hideHud()
                }))
                self.present(alert, animated: true, completion: nil)
            }
            
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
    }
}

// Create animation Kick counter
extension KickCounterViewController {
    
    func createCircle(){
        self.drawBeginShape()
////        drawTimeLeftShape()
////        drawBgShape()
//        strokeIt.fromValue = 0
//        strokeIt.toValue = 1
//        strokeIt.duration = timeLeft
//        timeLeftShapeLayer.add(strokeIt, forKey: nil)
//        bgShapeLayer.add(strokeIt, forKey: nil)
        
        endTime = self.currentTimeDate.addingTimeInterval(timeLeft)
//        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
//    func drawBgShape() {
////         Tạo shapre từ điểm bắt đầu đến hết
//        bgShapeLayer.removeFromSuperlayer()
//        let ovalPath = UIBezierPath(arcCenter: CGPoint(x: self.mKickBtn.frame.midX , y: self.mKickBtn.frame.midY), radius:
//            36, startAngle: self.startAngle + self.cornerBegin.degreesToRadians, endAngle: self.endAngle, clockwise: true)
//        bgShapeLayer.path = ovalPath.cgPath
//        bgShapeLayer.lineWidth = 68
//        bgShapeLayer.fillColor = UIColor.clear.cgColor
//        bgShapeLayer.strokeColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
//
//        self.mKickView.layer.addSublayer(bgShapeLayer)
//        self.mKickBtn.layer.bringToFront()
//    }
//
//    func drawTimeLeftShape() {
//        // Tạo shapre từ điểm bắt đầu đến hết
//        timeLeftShapeLayer.removeFromSuperlayer()
//
//        timeLeftShapeLayer.path = UIBezierPath(arcCenter: CGPoint(x: self.mKickView.frame.midX , y: self.mKickView.frame.midY), radius:
//            77, startAngle: self.startAngle + self.cornerBegin.degreesToRadians, endAngle: self.endAngle, clockwise: true).cgPath
//        timeLeftShapeLayer.strokeColor = #colorLiteral(red: 0.02721658349, green: 0.5905672908, blue: 1, alpha: 1)
//        timeLeftShapeLayer.fillColor = UIColor.clear.cgColor
//        timeLeftShapeLayer.lineWidth = 6
//        self.kickViewBiggest.layer.addSublayer(timeLeftShapeLayer)
//    }

    @objc func updateTime() {
        if timeLeft > 0 {
            timeLeft = endTime?.timeIntervalSinceNow ?? 0
        } else {
            timer.invalidate()
        }
    }
    
    func pause(_ layer: CAShapeLayer?) {
        let pausedTime: CFTimeInterval? = layer?.convertTime(CACurrentMediaTime(), from: nil)
        layer?.speed = 0.0
        layer?.timeOffset = pausedTime ?? 0
    }
    
    func resumeLayer(_ layer: CAShapeLayer?) {
        let pausedTime: CFTimeInterval? = layer?.timeOffset
        layer?.speed = 1.0
        layer?.timeOffset = 0.0
        layer?.beginTime = 0.0
        let timeSincePause: CFTimeInterval = (layer?.convertTime(CACurrentMediaTime(), from: nil) ?? 0) - (pausedTime ?? 0)
        layer?.beginTime = timeSincePause
    }

}

extension TimeInterval {
    var time: String {
        return String(format:"%02d:%02d", Int(self/60),  Int(ceil(truncatingRemainder(dividingBy: 60))) )
    }
}
extension Int {
    var degreesToRadians : CGFloat {
        return CGFloat(self) * .pi / 180
    }
}

