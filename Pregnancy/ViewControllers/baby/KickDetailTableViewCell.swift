//
//  KickDetailTableViewCell.swift
//  Pregnancy
//
//  Created by tien on 2/11/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class KickDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var mTimeKick: UILabel!
    @IBOutlet weak var mKick: UILabel!
    @IBOutlet weak var mElapsedTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
