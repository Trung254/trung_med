//
//  Circle2TableViewCell.swift
//  Pregnancy
//
//  Created by Bao Nguyen on 12/19/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class Circle2TableViewCell: UITableViewCell {
    
    @IBOutlet weak var mFirstCircle: UIView!
    @IBOutlet weak var mSecondCircle: UIView!
    @IBOutlet weak var mLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
