//
//  BabyViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class BabyViewController: MenuCardViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mIsShowRightButton = false
        self.addPadLockButton()
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        hideTabBar(hidden: false, animated: false)
    }
    
    // setup menu items
    override func setupMenuItems() {
        let name = ["Tin tức hằng ngày","Tin tức hàng tuần","Hình ảnh em bé","Kích cỡ em bé","Dòng thời gian","Đếm lần đạp chân"]
        let nameImage = ["Daily","Weekly","Images","Size","Timeline","Kick Counter"]
            for i in 0...(name.count - 1) {
                menuItems.append(MenuItem.init(name: name[i], imageName: nameImage[i]))
            }
    }
    
    override func getViewTitle() -> String {
        return "Em bé"
    }
    
    override func backgroundImageName() -> String {
        return "me-background"
    }
    
    override func segueForMenuItemAtIndex(_ index: Int) -> String {
        var segue_id = ""
        switch index {
        case 0:
            segue_id = "baby_to_daily"
            break
        case 1:
            segue_id = "baby_to_weekly"
            break
        case 2:
            segue_id = "baby_to_images"
            break
        case 3:
            segue_id = "baby_to_size_guide"
            break
        case 4:
            segue_id = "baby_to_wvtimeLine"
            break
        case 5:
            segue_id = "baby_to_kick_counter"
            break
        default:
            NSLog("Selected : \(index)")
        }
        return segue_id
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "baby_to_wvtimeLine") {
            let vc = segue.destination as! BabyTimeLineViewController
            vc.isScrollTL = true
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
