//
//  BabyImagesViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/14/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftyJSON
import RealmSwift

class BabyImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mBabyImages: UIImageView!
    @IBOutlet weak var mScrollView: UIScrollView!
}

class BabyImagesViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var m_Segment: UISegmentedControl!
    @IBOutlet weak var mSLD_BabyImages: UISlider!
    @IBOutlet weak var mCollectionViewBabyImg: UICollectionView!
    
    var checkRangeImage = 0
    
    var weekSlide = 1
    var dataImages : [String] = [String]()
    var numberWeek : Int = 1
    var data :Dictionary<String,Array<String>> = ["0":[], "1": [], "2":[]]
    var selectedWeek: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.m_Segment.selectedSegmentIndex = 1
        
        
        self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(weekSlide))
        //        self.sldBabyWeekly.minimumValue = 1
        self.mSLD_BabyImages.value = 1
        self.mSLD_BabyImages.minimumTrackTintColor = #colorLiteral(red: 0.1468058228, green: 0.704572618, blue: 0.9956740737, alpha: 1)
        // before load View we need checking and setting again all data for scress
        loadDataFromAPI()
        
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "info"), style: .done, target: self, action: #selector(call_Method))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        mCollectionViewBabyImg.delegate = self
        mCollectionViewBabyImg.dataSource = self
        
        self.navigationController?.navigationBar.tintColor = .white

//        self.changeStyleNavigationLabel()
        //self.addNavigationItem()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupSlider()
    }
    private func setupSlider(){
        self.mSLD_BabyImages.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                break
            case .moved:
                DispatchQueue.main.async {
                    let page = Int(self.mSLD_BabyImages.value)
                    self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(page))
                }
                break
            case .ended:
                let page = Int(self.mSLD_BabyImages.value)
                self.mCollectionViewBabyImg.scrollToItem(at: IndexPath(item: page - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
                self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(page))
                break
            default:
                break
            }
        }
    }
    
    @objc func call_Method(sender: AnyObject) {
        self.performSegue(withIdentifier: "BabyImagesToInfo", sender: self)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.m_Segment.selectedSegmentIndex {
        case 1:
            return (data["0"]?.count)!
        case 2:
            return (data["1"]?.count)!
        default:
            return (data["2"]?.count)!
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BabyImagesCollectionViewCell
        
        let sectionData = data[String(m_Segment.selectedSegmentIndex)]
        
        let url = URL(string: sectionData![indexPath.row])
        cell.mBabyImages.sd_setImage(with: url, completed: nil)
//        cell.mBabyImages!.sd_setImage(with: sectionData![indexPath.row], completed: nil)
        
//        if let url = URL(string: sectionData?[indexPath.item] ?? "") {
//            cell.mBabyImages.sd_setImage(with: url, placeholderImage: UIImage(named: "phoithai"), options: .highPriority, completed: nil)
//        } else {
//            cell.mBabyImages.image = UIImage(named: "phoithai")
//        }
        
        cell.mScrollView.setZoomScale(0.0, animated: true)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if weekSlide != 1 {
            self.mSLD_BabyImages.value = Float(weekSlide)
            self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(weekSlide))
            setTitle(title: "Tuần \(weekSlide)")
        }
        else {
            self.mSLD_BabyImages.value = Float(indexPath.row + 1)
            self.setThumbImageUISlider(slider: self.mSLD_BabyImages, text: String(indexPath.row + 1))
            setTitle(title: "Tuần \(indexPath.row + 1)")
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        let imnageView = scrollView.viewWithTag(10) as! UIImageView
        return imnageView
    }
    @IBAction func mSegment(_ sender: Any) {
        
        mCollectionViewBabyImg.reloadData()
    }
    
    func loadDataFromAPI() {
        // declare new thread
        self.showHud()
        // call API
        NetworkManager.shareInstance.apiGetAllFiles { (data, message, isSuccess) in
            // declare new thread
            self.hideHud()
            if isSuccess == true{
                self.data = data
//                self.imageData = data
            }
            self.mCollectionViewBabyImg.reloadData()
            self.loadSelectedWeek()
        }
    }
    
    func loadSelectedWeek() {
        if let week = selectedWeek {
            let indexPath = IndexPath(item: week - 1, section: 0)
            self.mCollectionViewBabyImg.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
        }
    }
    
//    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
//        numberWeek = page
//        self.changeStyleNavigationLabel()
//
//    }
    
//    func changeStyleNavigationLabel(){
//        var lbl : UILabel?
//        if(navigationItem.titleView == nil) {
//            lbl = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width * 0.75, height: 44))
//            navigationItem.titleView = lbl
//            lbl?.textColor = .white
//            lbl?.textAlignment = .center
//            lbl?.font = lbl?.font.withSize(20.0)
//        } else {
//            lbl = navigationItem.titleView as? UILabel
//        }
////        lbl?.text = "Tuần\(numberWeek)"
    
        
    }
    
    //    func addNavigationItem(){
    //        let button = UIButton(type: .custom)
    //        button.setImage(UIImage(named: "LeftArrow"), for: .normal)
    //        button.addTarget(self, action: #selector(doDefaultBack), for: .touchUpInside)
    //        button.frame = CGRect(x: 0, y: 0, width: 26, height: 24)
    //        let barButton = UIBarButtonItem(customView: button)
    //        self.navigationItem.leftBarButtonItem = barButton
    //    }
    
//    @objc func nextImage() {
//        if(numberWeek < 40) {
//            mCollectionViewBabyImg.scrollToItem(at: IndexPath.init(item: numberWeek, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
//        }
//    }

    //    @IBAction func doDefaultBack(_ : UIButton){
    //        self.navigationController?.popViewController(animated: true)
    //    }
    
//}

extension BabyImagesViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
