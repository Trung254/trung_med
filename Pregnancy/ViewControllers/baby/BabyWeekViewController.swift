//
//  BabyWeekViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 12/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SnapKit
import WebKit
import Foundation
import MobileCoreServices
import AVFoundation
import Photos
import RealmSwift
import SwiftyJSON
import CropViewController

class BabyWeekViewController: BaseViewController, UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((NSURL) -> Void)?
    var filePickedBlock: ((URL) -> Void)?
    
    var imagePicker = UIImagePickerController()
    var pickedImageProduct = UIImage()
    
    
    enum AttachmentType: String{
        case camera, video, photoLibrary
    }
    
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Upload Avatar"
        static let actionFileTypeDescription = "Chọn file hoặc chụp ảnh"
        static let camera = "Camera"
        static let phoneLibrary = "Phone Library"
        static let video = "Video"
        static let file = "File"
        
        
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        
        static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        
        
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
        
    }
    
    @IBOutlet weak var sldBabyWeekly: UISlider!
    @IBOutlet weak var weekCollectionView: UICollectionView!
    
    var mUpdateDataWhenAppear = true
    var weekData : [PregWeek] = [PregWeek]()
//    var tap: UITapGestureRecognizer?
    
    let step: Float = 1
    var pointWeekSlider : CGFloat = 0 //point của slider ban đầu
    var checkCreateShapre = false
    let shapeLayer = CAShapeLayer()
    
    let minThumbColor = #colorLiteral(red: 0.1468058228, green: 0.704572618, blue: 0.9956740737, alpha: 1)
    let maxThumbColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    var tapUiSlider : UITapGestureRecognizer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sldBabyWeekly.minimumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.sldBabyWeekly.maximumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.sldBabyWeekly.thumbTintColor = self.minThumbColor
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "info"), style: .done, target: self, action: #selector(call_Method))
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (mUpdateDataWhenAppear) {
            mUpdateDataWhenAppear = false
            loadDataFromAPI()
            setupSlider()
        }
    }

    func setDataPreg(_ pregnancy: PregPregnancy?) -> Void {
        
        if pregnancy != nil {

            var components = Calendar.current.dateComponents([.weekOfYear], from: pregnancy!.start_date, to: Date())
            var thisweek = 1
            if pregnancy?.baby_already_born == 1 {
                thisweek = 0
            } else {
                if let week = components.weekOfYear {
                    thisweek = week
                    if pregnancy?.show_week == 1 {
                        thisweek = thisweek + 1
                    }
                    if thisweek > 0 {
                        thisweek = thisweek - 1
                    }
                }
            }
            
            if self.weekData.count > thisweek {
//                self.weekCollectionView.reloadData()
                self.sldBabyWeekly.setValue(Float(thisweek + 1), animated: true)
                self.sldBabyWeekly.thumbTintColor = self.minThumbColor
                self.setTitle(title: "Tuần \(thisweek + 1)")
                
                let trackRect: CGRect = self.sldBabyWeekly.trackRect(forBounds: self.sldBabyWeekly.bounds)
                let thumbRect: CGRect = self.sldBabyWeekly.thumbRect(forBounds: self.sldBabyWeekly.bounds, trackRect: trackRect, value: self.sldBabyWeekly.value)
                
                self.pointWeekSlider = thumbRect.origin.x // Lưu vị trí x ban đầu
                
                self.createShape(self.sldBabyWeekly, startPoint: self.pointWeekSlider) // Vẽ lần đầu
                
                self.weekCollectionView.scrollToItem(at: IndexPath.init(item: thisweek, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)

            }
        } else {
            print("Pregnancy is empty")
        }
        
        
    }
    
    func loadDataFromAPI() {
        
        self.showHud()
        NetworkManager.shareInstance.apiGetAllWeeks { (dataPregWeek, message, isSuccess) in
           
            self.weekData.removeAll()
            self.weekData = dataPregWeek
            
            DispatchQueue.main.async {
                self.weekCollectionView.reloadData()
                self.sldBabyWeekly.maximumValue = Float(self.weekData.count)
                DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
                    self.setDataPreg(pregnancy)
                }
                self.hideHud()
            }
        }
    }
    
    func updateReadWeek(week: Int) {
        NetworkManager.shareInstance.apiReadWeeks(weekId: week) {
            (data, message, isSuccess) in
            
        }
    }
    
    private func setupSlider(){
        self.sldBabyWeekly.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        // Thêm tab vào slider -> update lại value
        self.tapUiSlider = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.sldBabyWeekly.addGestureRecognizer(self.tapUiSlider)
    }
    
    // Set tab slider và di chuyển Thumb, update lại giá trị
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        
        let positionOfSlider: CGPoint = self.sldBabyWeekly.frame.origin
        let widthOfSlider: CGFloat = self.sldBabyWeekly.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.sldBabyWeekly.maximumValue) / widthOfSlider)
        
        self.sldBabyWeekly.setValue(roundf(Float(newValue)), animated: true)
        
        
        let trackRect: CGRect = self.sldBabyWeekly.trackRect(forBounds: self.sldBabyWeekly.bounds) // Lấy giá trị của uislider
        let thumbRect: CGRect = self.sldBabyWeekly.thumbRect(forBounds: self.sldBabyWeekly.bounds, trackRect: trackRect, value: self.sldBabyWeekly.value) // Lấy giá trị của thumb uislider
        //        print(thumbRect)
        if self.pointWeekSlider < thumbRect.origin.x {
            self.sldBabyWeekly.thumbTintColor = self.maxThumbColor
        } else {
            self.sldBabyWeekly.thumbTintColor = self.minThumbColor
        }
        
        
        self.weekCollectionView.scrollToItem(at: IndexPath(item: Int(self.sldBabyWeekly.value - 1), section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
        
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        self.sldBabyWeekly.value = roundf(self.sldBabyWeekly.value)
        if let touchEvent = event.allTouches?.first {
            
            var sliderValue = slider.value
            if sliderValue < 1 {
                sliderValue  = 1
            }
            
            switch touchEvent.phase {
            case .began:
                view.endEditing(true)
                self.sldBabyWeekly.removeGestureRecognizer(self.tapUiSlider)
                break
            case .moved:
                
                let roundedValue = round(sliderValue / step) * step
                slider.value = roundedValue
                
                // Lấy giá trị của thumb image để đổi màu
                let trackRect: CGRect = self.sldBabyWeekly.trackRect(forBounds: self.sldBabyWeekly.bounds)
                let thumbRect: CGRect = self.sldBabyWeekly.thumbRect(forBounds: self.sldBabyWeekly.bounds, trackRect: trackRect, value: self.sldBabyWeekly.value)

                if self.pointWeekSlider < thumbRect.origin.x {
                    self.sldBabyWeekly.thumbTintColor = self.maxThumbColor
                } else {
                    self.sldBabyWeekly.thumbTintColor = self.minThumbColor
                }
                break
            case .ended:

                let page = Int(self.sldBabyWeekly.value)
                self.weekCollectionView.scrollToItem(at: IndexPath(item: page - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
                
                let trackRect: CGRect = self.sldBabyWeekly.trackRect(forBounds: self.sldBabyWeekly.bounds)
                let thumbRect: CGRect = self.sldBabyWeekly.thumbRect(forBounds: self.sldBabyWeekly.bounds, trackRect: trackRect, value: self.sldBabyWeekly.value)

                if self.pointWeekSlider < thumbRect.origin.x{
                    self.sldBabyWeekly.thumbTintColor = self.maxThumbColor
                } else {
                    self.sldBabyWeekly.thumbTintColor = self.minThumbColor
                }
                
                self.sldBabyWeekly.addGestureRecognizer(self.tapUiSlider)
                break
            default:
                break
            }
        }
    }
    
    
    @objc func call_Method(sender: AnyObject) {
        self.performSegue(withIdentifier: "BabyDailyToInfo", sender: self)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: weekCollectionView.contentOffset, size: weekCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = weekCollectionView.indexPathForItem(at: visiblePoint)
    
        if let indexPath = indexPath {
            self.sldBabyWeekly.value = Float(indexPath.item + 1)
            
            let trackRect: CGRect = self.sldBabyWeekly.trackRect(forBounds: self.sldBabyWeekly.bounds)
            let thumbRect: CGRect = self.sldBabyWeekly.thumbRect(forBounds: self.sldBabyWeekly.bounds, trackRect: trackRect, value: self.sldBabyWeekly.value)
            
            if self.pointWeekSlider < thumbRect.origin.x {
                self.sldBabyWeekly.thumbTintColor = self.maxThumbColor
            } else {
                self.sldBabyWeekly.thumbTintColor = self.minThumbColor
            }
            setTitle(title: "Tuần \(indexPath.item + 1)")
        }
    }
}

extension BabyWeekViewController: UICollectionViewDelegateFlowLayout {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:true, completion: nil)
    }
    
    
    func saveImage(imageName: String, image: UIImage) {
        
        
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        
        let fileName = imageName
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return }
        
        //Checks if file exists, removes it if so.
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
        } catch let error {
            print("error saving file with error", error)
        }
        
    }
    
    
    
    class func loadImageFromDiskWith(fileName: String) -> UIImage? {
        
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        
        return nil
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
    self.imagePickedBlock?(image)
    self.saveImage( imageName:"Tuần \(Int(self.sldBabyWeekly.value))" , image: image)
    self.showHud()
    self.hud?.mode = .determinateHorizontalBar
    self.hud?.progress = 0
    self.mUpdateDataWhenAppear = false
    NetworkManager.shareInstance.apiPostWeeklyInteract(weekID: Int(self.sldBabyWeekly.value), comment: nil) { (data, message, isSuccess) in
    NetworkManager.shareInstance.apiUploadWeeklyImageInteract(weekID: Int(self.sldBabyWeekly.value), image: image, updateProgress: { (progress) -> Void? in
    self.hud?.progress = progress
    }) { (data, message, isSuccess) in
    let jdata = JSON(data)
    if ( !isSuccess) {
    var message = jdata["message"].stringValue
    if message.count == 0 {
    message = "Vui lòng thử lại sau"
    }
    self.alert(message)
    } else {
    if let first = jdata.arrayValue.first {
    let urlStr = NetworkManager.rootDomain + first.stringValue
    if let cell = self.weekCollectionView.visibleCells.first as? WeeklyViewCell {
    let ratio = image.size.height / image.size.width
    cell.mWeekView.mHeightImageVIew.constant = cell.mWeekView.mPhotoUpload.width * ratio
    cell.mWeekView.mPhotoUpload.sd_setImage(with: URL(string:urlStr), placeholderImage: image, options: .highPriority, completed: { (image, error, cacheType, url) in
    if let image = image {
    
    let ratio = image.size.height / image.size.width
    cell.mWeekView.mHeightImageVIew.constant = cell.mWeekView.mPhotoUpload.width * ratio
    cell.mWeekView.mPhotoUpload.image = image
    cell.mWeekView.mCameraImg.isHidden = true
    cell.mWeekView.mAddImgLbl.isHidden = true
    
    } else {
    cell.mWeekView.mCameraImg.isHidden = false
    cell.mWeekView.mAddImgLbl.isHidden = false
    cell.mWeekView.mPhotoUpload.image = image
    }
    })
    }
    } else if let cell = self.weekCollectionView.visibleCells.first as? WeeklyViewCell {
    let ratio = image.size.height / image.size.width
    cell.mWeekView.mHeightImageVIew.constant = cell.mWeekView.mPhotoUpload.width * ratio
    cell.mWeekView.mPhotoUpload.image = image
    }
    
    }
    self.hudShowCount = 1
    self.hideHud()
    self.mUpdateDataWhenAppear = true
    }
    }
        currentVC?.dismiss(animated: true, completion: nil)

    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        

        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            currentVC?.dismiss(animated: true, completion: nil)
            let image = imageOriginal.fixOrientation()
            if let image = image {
                
                let cropViewController = CropViewController(image: image)
                cropViewController.delegate = self
                present(cropViewController, animated: true, completion: nil)
            }
        } else{
            print("Something went wrong in  image")
        }
        
    }

    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL) {
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
    func documentPickerbelly(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        self.filePickedBlock?(url)
    }
    
    //    Method to handle cancel action.
    func documentPickerWasCancelledbelly(_ controller: UIDocumentPickerViewController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weekData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        self.sldBabyWeekly.value = Float(indexPath.row)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeeklyViewCell", for: indexPath) as! WeeklyViewCell
        
        updateReadWeek(week: indexPath.item + 1)
        
        cell.mWeekView.mLength.text = String(format: "%.1f", self.weekData[indexPath.row].length) + " cm"
        cell.mWeekView.mWeight.text = String(format: "%.1f", self.weekData[indexPath.row].weight) + " gram"
        
        let fore = UIImage(named: String(format: "%dw", indexPath.row+1))
        
        cell.mWeekView.mImgBaby.image = fore
        if (indexPath.row >= 0 && indexPath.row < 3 ) || (indexPath.row > 39) {
            cell.mWeekView.mImgBaby.image = #imageLiteral(resourceName: "default-baby")
        }
        
//        cell.mWeekView.mTextViewAdd.tag = indexPath.row
//        cell.mWeekView.mTextViewAdd.addta
        if self.weekData[indexPath.row].comment.isEmpty == false {
            cell.mWeekView.mTextViewAdd.text = self.weekData[indexPath.row].comment
            cell.mWeekView.mTextViewAdd.textColor = .black
        } else {
            cell.mWeekView.mTextViewAdd.text = "Thêm ghi chú tại đây..."
            cell.mWeekView.mTextViewAdd.textColor = UIColor.lightGray
        }
        
        DispatchQueue.main.async {
            cell.mWeekView.setDataWeekly(htmlContent: URL(string: self.weekData[indexPath.row].week_description)!)
            cell.mWeekView.setcurrentWeekNumber(currentWeek: String(indexPath.row + 1))
            cell.mWeekView.mAddImageBtn.tag = indexPath.row
            
//            cell.mWeekView.mAddImageBtn.contentMode = .scaleAspectFit
            
            cell.mWeekView.mTextViewAdd.delegate = self
            cell.mWeekView.mTextViewAdd.tag = indexPath.item
            
            cell.mWeekView.mPhotoUpload.image = BabyWeekViewController.loadImageFromDiskWith(fileName: "Tuần \(String(indexPath.row + 1))")
            cell.mWeekView.mAddImageBtn.removeTarget(nil, action: nil, for: UIControl.Event.allEvents)
            cell.mWeekView.mAddImageBtn.addTarget(self, action: #selector(self.addimage(_:)), for: UIControl.Event.touchUpInside)
//            self.activityIndicator(
            
            if self.weekData[indexPath.row].photo.isEmpty == false {
                let url = URL(string: self.weekData[indexPath.row].photo)
                cell.mWeekView.mPhotoUpload.sd_setImage(with: url, completed: { (image, error, cacheType, url) in
                    if let image = image {
                        let ratio = image.size.height / image.size.width
                        cell.mWeekView.mHeightImageVIew.constant = cell.mWeekView.mPhotoUpload.width * ratio
                        cell.mWeekView.mPhotoUpload.image = image
                        cell.mWeekView.mCameraImg.isHidden = true
                        cell.mWeekView.mAddImgLbl.isHidden = true
                    } else {
                        cell.mWeekView.mPhotoUpload.image = image
                        cell.mWeekView.mCameraImg.isHidden = true
                        cell.mWeekView.mAddImgLbl.isHidden = true
                    }
                })
            } else {
                cell.mWeekView.mCameraImg.isHidden = false
                cell.mWeekView.mAddImgLbl.isHidden = false
                cell.mWeekView.mPhotoUpload.image = nil
            }
            
            
            //Click share button
            cell.mWeekView.mShareNoteAndImage.tag = indexPath.item
            cell.mWeekView.mShareNoteAndImage.removeTarget(nil, action: nil, for: UIControl.Event.allEvents)
            cell.mWeekView.mShareNoteAndImage.addTarget(self, action: #selector(self.ShareNoteAndImage(_:)), for: .touchUpInside)
            //Click share Weekly Infomation
            cell.mWeekView.mShareWeeklyInfor.removeTarget(nil, action: nil, for: UIControl.Event.allEvents)
            cell.mWeekView.mShareWeeklyInfor.addTarget(self, action: #selector(self.ShareWeeklyInfor(_:)), for: .touchUpInside)
            
            
            cell.mWeekView.mDeleteNote.tag = indexPath.item
            cell.mWeekView.mDeleteNote.removeTarget(nil, action: nil, for: UIControl.Event.allEvents)
            cell.mWeekView.mDeleteNote.addTarget(self, action: #selector(self.deleteNote(_:)), for: .touchUpInside)
        }
        
        return cell
    }
    
    @objc func ShareNoteAndImage(_ sender : UIButton) {
        
        let week = self.weekData[sender.tag].id
        let weekShare = "Tuần thai thứ \(week)"
        let linkAppInstall = "https://medlatec.vn/"
        
        
        if let cell = self.weekCollectionView.visibleCells.first as? WeeklyViewCell {
            
            if cell.mWeekView.mPhotoUpload.image != nil {
                let  noteShare = cell.mWeekView.mTextViewAdd.text
                
                let imgShare = self.CreateImageShare(cell.mWeekView.mPhotoUpload.image!, linkAppInstall, noteShare ?? "", weekShare)
                
                let activityController = UIActivityViewController(activityItems: [imgShare], applicationActivities: nil)
                activityController.setValue("Tuần thai thứ \(weekShare)", forKey: "subject")
                UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
            } else {
                self.alert("Ảnh của bạn chưa tồn tại, mời bạn thêm ảnh")
            }
        }
    }
    
    func CreateImageShare(_ image : UIImage,_ appInstall : String,_ note : String,_ week: String) -> UIImage{
        
        let ratio = image.size.height / image.size.width
//        cell.mWeekView.mHeightImageVIew.constant = cell.mWeekView.mPhotoUpload.width * ratio
        
        let viewShare = ImgShareWeekly()
        
        viewShare.imageView.image = image
        viewShare.lblAppInstall.text = appInstall
        viewShare.lblWeeklyShare.text = week
        viewShare.txtNote.text = note
        viewShare.txtNote.numberOfLines = 0
        viewShare.txtNote.sizeToFit()
        
//        viewShare.heightImageView.constant = viewShare.imageView.width * ratio
        viewShare.imageView.height = viewShare.imageView.width * ratio
        let textHeight = viewShare.txtNote.height
        viewShare.viewSecond.height = 220 + textHeight
//        viewShare.heightViewSecond.constant = 100 + textHeight
        viewShare.viewImageShare.height = viewShare.viewSecond.height + viewShare.imageView.height
//        viewShare.heightParrentView.constant = viewShare.heightViewSecond.constant + viewShare.heightImageView.constant
        viewShare.viewSecond.y = viewShare.imageView.height + 30
        viewShare.lblAppInstall.y = viewShare.viewSecond.height - 100
        return UIImage.init(view: viewShare.viewImageShare)
    }
    
    @objc func ShareWeeklyInfor(_ sender : UIButton) {
        // Shre Weekly Information
        if let cell = self.weekCollectionView.visibleCells.first as? WeeklyViewCell {
            let activityController = UIActivityViewController(activityItems: [cell.mWeekView.webViewBabyWeekly.viewPrintFormatter()], applicationActivities: nil)
            UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
        }
    }
    
    
    @objc func addimage(_ sender:UIButton) {
        let index = sender.tag // showing day = index +1
        self.showAttachmentActionSheet(vc: self)
    }
    
    
    @objc func deleteNote(_ sender: UIButton) {
        if let cell = self.weekCollectionView.visibleCells.first as? WeeklyViewCell {
            
            NetworkManager.shareInstance.apiPutWeeklyInteract(weekID: self.weekData[sender.tag].id, comment: "") { (data, message, isSuccess) in
                if (isSuccess) {
                    cell.mWeekView.mTextViewAdd.text = "Thêm ghi chú tại đây..."
                    cell.mWeekView.mTextViewAdd.textColor = UIColor.lightGray
                    cell.mWeekView.mTextViewAdd.resignFirstResponder()
                } else {
                    cell.mWeekView.mTextViewAdd.text = "Thêm ghi chú tại đây..."
                    cell.mWeekView.mTextViewAdd.textColor = UIColor.lightGray
                    cell.mWeekView.mTextViewAdd.resignFirstResponder()
                }
            }
        }
    }
    
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
//            myPickerController.allowsEditing = true
            myPickerController.sourceType = .camera
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
//            myPickerController.allowsEditing = true
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: - FILE PICKER
//    func documentPicker(){
//        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
//        importMenu.delegate = self
//        importMenu.modalPresentationStyle = .formSheet
//        currentVC?.present(importMenu, animated: true, completion: nil)
//    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }

    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc as? BaseViewController
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                openCamera()
            }
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                photoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.camera{
                        self.openCamera()
                    }
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.photoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    func showAttachmentActionSheet(vc: UIViewController) {
        currentVC = vc as? BaseViewController
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
        }))
        
        //        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
        //            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
        //
        //        }))
        //
        //        actionSheet.addAction(UIAlertAction(title: Constants.file, style: .default, handler: { (action) -> Void in
        //            self.documentPicker()
        //        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        if let pop = actionSheet.popoverPresentationController {
            pop.sourceView = vc.view
            pop.sourceRect = CGRect(x:vc.view.bounds.size.width / 2.0 , y: vc.view.bounds.size.height, width: 1.0, height: 1.0)
        }
        vc.present(actionSheet, animated: true, completion: nil)
    }
    



    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.weekCollectionView.width, height: self.weekCollectionView.height)
    }
}

extension BabyWeekViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = .black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.weekData[textView.tag].comment = textView.text
        NetworkManager.shareInstance.apiPutWeeklyInteract(weekID: self.weekData[textView.tag].id, comment: textView.text) { (data, message, isSuccess) in
            
            print("apiPutWeeklyInteract: ----\(isSuccess)")
            
            NetworkManager.shareInstance.apiPostWeeklyInteract(weekID: self.weekData[textView.tag].id, comment: textView.text) { (data, message, isSuccess) in
                print("apiPostWeeklyInteract: ----\(isSuccess)")
            }
        }
        
        if textView.text.isEmpty {
            textView.text = "Thêm ghi chú tại đây..."
            textView.textColor = UIColor.lightGray
        } else {
            textView.textColor = UIColor.black
        }
    }
}

extension UIImage {
    
    func fixOrientation() -> UIImage? {
        
        if (imageOrientation == .up) { return self }
        
        var transform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0.0)
            transform = transform.rotated(by: .pi / 2.0)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0.0, y: size.height)
            transform = transform.rotated(by: -.pi / 2.0)
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: .pi)
        default:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: 0.0)
            transform = transform.scaledBy(x: -1.0, y: 1.0)
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: size.height, y: 0.0)
            transform = transform.scaledBy(x: -1.0, y: 1.0)
        default:
            break
        }
        
        guard let cgImg = cgImage else { return nil }
        
        if let context = CGContext(data: nil,
                                   width: Int(size.width), height: Int(size.height),
                                   bitsPerComponent: cgImg.bitsPerComponent,
                                   bytesPerRow: 0, space: cgImg.colorSpace!,
                                   bitmapInfo: cgImg.bitmapInfo.rawValue) {
            
            context.concatenate(transform)
            
            if imageOrientation == .left || imageOrientation == .leftMirrored ||
                imageOrientation == .right || imageOrientation == .rightMirrored {
                context.draw(cgImg, in: CGRect(x: 0.0, y: 0.0, width: size.height, height: size.width))
            } else {
                context.draw(cgImg, in: CGRect(x: 0.0 , y: 0.0, width: size.width, height: size.height))
            }
            
            if let contextImage = context.makeImage() {
                return UIImage(cgImage: contextImage)
            }
            
        }
        
        return nil
    }
    
}
// tạo
extension BabyWeekViewController {
    private func createShape(_ slider : UISlider, startPoint: CGFloat) {
        
        let trackRect: CGRect = slider.trackRect(forBounds: slider.bounds)
        
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: trackRect.origin.x + 0.5, y: trackRect.origin.y + trackRect.height / 2))
        path.addLine(to: CGPoint(x: startPoint + 2, y: trackRect.origin.y + trackRect.height / 2))
        
        //        let shapeLayer = CAShapeLayer()
        self.shapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        self.shapeLayer.strokeColor = minThumbColor.cgColor
        self.shapeLayer.lineWidth = 4
        self.shapeLayer.path = path.cgPath
        
        slider.layer.addSublayer(self.shapeLayer)
        self.shapeLayer.bringToFront()
        
    }
    private func removeShapre() {
        self.shapeLayer.removeFromSuperlayer()
    }
}


class WeeklyViewCell: UICollectionViewCell {
    @IBOutlet weak var mWeekView: WeeklyView!
}
