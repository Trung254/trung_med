//
//  WeeklyView.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 1/19/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SnapKit
import WebKit

class WeeklyView: UIView {
    @IBOutlet var weekContentView: UIView!
    @IBOutlet weak var webViewBabyWeeklyView: UIView!
    @IBOutlet weak var mWebViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblWeek: UILabel!
    @IBOutlet weak var mTextViewAdd: UITextView!
    @IBOutlet weak var btnShareNote: UIButton!
    @IBOutlet weak var mPhotoUpload: UIImageView!
    @IBOutlet weak var mAddImageBtn: UIButton!
    @IBOutlet weak var mAvatarNote: UIView!
    @IBOutlet weak var mAvatarNoteBg: UIView!
    @IBOutlet weak var mAvatarNoteImage: UIImageView!
    @IBOutlet weak var mShareNoteAndImage: UIButton!
    @IBOutlet weak var mShareWeeklyInfor: UIButton!
    @IBOutlet weak var mHeightImageVIew: NSLayoutConstraint!
    
    @IBOutlet weak var mScrollView: UIScrollView!
    
    @IBOutlet weak var viewNote: UIView!
    
    @IBOutlet weak var mDeleteNote: UIButton!
    
    @IBOutlet weak var mImgBaby: UIImageView!
    @IBOutlet weak var mLength: UILabel!
    @IBOutlet weak var mWeight: UILabel!
    
    @IBOutlet weak var mCameraImg: UIImageView!
    
    @IBOutlet weak var mAddImgLbl: UILabel!
    
    var webViewBabyWeekly: WKWebView!
    var photoUpload: UIImage?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mAvatarNote.layer.cornerRadius = mAvatarNote.layer.frame.width / 2
        self.mAvatarNoteBg.layer.cornerRadius = mAvatarNoteBg.layer.frame.width / 2
        self.mAvatarNoteImage.layer.cornerRadius = mAvatarNoteImage.layer.frame.height / 2
        
        self.mTextViewAdd.textColor = UIColor.lightGray
        self.mShareWeeklyInfor.isHidden = true
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("WeeklyView", owner: self, options: nil)
        addSubview(weekContentView)
        weekContentView.frame = self.bounds
        mWebViewHeight.constant = 1000
        
        weekContentView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview()
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
        }
        self.setupWebviewIfNeed()
        self.setupShowHideKeyboard()
    }
    
    func setupWebviewIfNeed() -> Void {
        if(self.webViewBabyWeekly == nil) {
            let webConfiguration = WKWebViewConfiguration()
            webViewBabyWeekly = WKWebView(frame: .zero, configuration: webConfiguration)
            webViewBabyWeekly.uiDelegate = self
            webViewBabyWeekly.navigationDelegate = self
            
            webViewBabyWeekly.isUserInteractionEnabled = false
            
            //            webViewBabyWeekly.scrollView.bounces = true
            
            
            self.webViewBabyWeeklyView.addSubview(self.webViewBabyWeekly)
            self.webViewBabyWeeklyView.translatesAutoresizingMaskIntoConstraints = false
            self.webViewBabyWeekly.snp.makeConstraints { (maker) in
                maker.top.equalToSuperview()
                maker.leading.equalToSuperview()
                maker.bottom.equalToSuperview()
                maker.trailing.equalToSuperview()
            }
        }
    }
    
    func setDataWeekly(htmlContent:URL) -> Void {
        self.setupWebviewIfNeed()
        self.mWebViewHeight.constant = 0
        self.webViewBabyWeekly.load(URLRequest(url: URL(string:"about:blank")!))
        self.webViewBabyWeekly.load(URLRequest(url: htmlContent))
        //        self.webViewBabyDaily.loadHTMLString(htmlContent, baseURL: nil)
        
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            self.mAvatarNoteImage.layer.cornerRadius = self.mAvatarNoteImage.layer.frame.size.width / 2
            self.mAvatarNoteImage.layer.masksToBounds = true
            var avatar = NetworkManager.rootDomain
            if user.avatar.hasPrefix("http") {
                avatar = user.avatar
            } else {
                avatar.append(user.avatar)
            }
            self.mAvatarNoteImage.sd_setImage(with: URL(string:avatar)) { (image, error, cacheType, url) in
                if let image = image {
                    self.mAvatarNoteImage.image = image
                } else {
                   self.mAvatarNoteImage.image = UIImage(named: "user_placeholder")
                }
            }
        } else {
            self.mAvatarNoteImage.image = UIImage(named: "user_placeholder")
        }
        
        
    }
    func setcurrentWeekNumber(currentWeek: String){
        self.lblWeek.text = currentWeek
    }
    @IBAction func deleteNote(_ sender: Any) {
//        mTextViewAdd.text = "Thêm ghi chú tại đây..."
//        mTextViewAdd.textColor = UIColor.lightGray
//        mTextViewAdd.resignFirstResponder()
    }
    
    @IBAction func clickBtnShare(_ sender: Any) {
//        let activityController = UIActivityViewController(activityItems: [mTextViewAdd.text], applicationActivities: nil)
//        UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
    }
    @IBAction func clickSharePerWeek(_ sender: Any) {
//        let activityController = UIActivityViewController(activityItems: [mTextViewAdd.text], applicationActivities: nil)
//        UIApplication.shared.keyWindow?.rootViewController?.present(activityController, animated: true, completion: nil)
    }
    
    @IBAction func clickAddPhoto(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Dữ liệu ảnh", message: "Mời bạn chọn Ảnh", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Máy ảnh", style: .default, handler: {(action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                UIApplication.shared.keyWindow?.rootViewController?.present(imagePickerController,animated: true,completion: nil)
            }
            else {
                print("camera not available")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Thư viện ảnh", style: .default, handler: {(action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            UIApplication.shared.keyWindow?.rootViewController?.present(imagePickerController,animated: true,completion: nil)
        }))
        
        if photoUpload != nil {
            actionSheet.addAction(UIAlertAction(title: "Xóa Ảnh", style: .default, handler:
                {(action:UIAlertAction) in
                    self.photoUpload = nil
                    self.mPhotoUpload.image = nil
            }))
        }else {
            actionSheet.addAction(UIAlertAction(title: "Hủy", style: .default, handler:nil))
        }
        
        UIApplication.shared.keyWindow?.rootViewController?.present(actionSheet, animated: true, completion: {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlertController))
            actionSheet.view.superview?.subviews[0].addGestureRecognizer(tapGesture)
        })
    }
    @objc func dismissAlertController(){
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
}



extension WeeklyView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.photoUpload = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        var photoSize:Float!
        photoSize = Float(mPhotoUpload.size.width)/(Float((self.photoUpload?.size.width)!) / Float((self.photoUpload?.size.height)!))
        mPhotoUpload.frame = CGRect(origin: .zero, size: CGSize(width: mPhotoUpload.size.width, height: CGFloat(photoSize)));
        
        mPhotoUpload.image = photoUpload
        
        picker.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:true, completion: nil)
    }
    
}

extension WeeklyView: WKUIDelegate {
    
}

extension WeeklyView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webViewBabyWeekly.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                
                self.webViewBabyWeekly.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    self.mWebViewHeight.constant = height as! CGFloat
                })
            }
            
        })
    }
}

extension WeeklyView {
    func setupShowHideKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        print("keyboard show")
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.convert(keyboardFrame, from: nil)
        var contentInset:UIEdgeInsets = mScrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        mScrollView.contentInset = contentInset
        mScrollView.scrollIndicatorInsets = contentInset
        
        
        
        let yCount = (self.viewNote.y + self.viewNote.height) - (mScrollView.height - keyboardFrame.height) - 30
        print(mScrollView.contentSize.height)
        
//        self.mScrollView.setContentOffset(CGPoint(x: 0, y: yCount), animated: true)
        
        mScrollView.contentOffset.y = yCount
        
        
//        if let rect = self.mTextViewAdd.superview?.convert(self.mTextViewAdd.frame, to: self.mTextViewAdd.superview?.superview) {
//            let test = UIScreen.main.bounds.height - (rect.size.height + rect.origin.y + self.mScrollView.contentInset.top) - 60.0
//            let different = keyboardFrame.size.height - test
//            if different > 0 {
//                self.mScrollView.contentOffset.y = different - self.mScrollView.contentInset.top
//            }
//        }

    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        var contentInset:UIEdgeInsets = mScrollView.contentInset
        contentInset.bottom = 0
        mScrollView.contentInset = contentInset
        mScrollView.scrollIndicatorInsets = contentInset
    }
}
