//
//  BabyDailyViewController.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 10/12/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

class BabyDailyViewController: BaseViewController, UIWebViewDelegate {

    @IBOutlet weak var sld_BabyDaily: UISlider!
    @IBOutlet weak var mCollectionView: UICollectionView!
//    var htmlString:String!
    
    var dailyDes : [String] = [String]()
    var valueSlide = 100
    var pregnancy: PregPregnancy?
    var dataTodos: [JSON] = []
    var dataUserTodos: [JSON] = []
    
    var statusLike = 0
    var statusDisLike = 0
    var curStatus = false
    
    // 0 : Cả 2 nút đều trắng
    // 1 : like
    // 2: dislike
    var pregDaily : [PregDaily] = []
    let step: Float = 1
    
    var pointWeekSlider : CGFloat = 0 //point của slider ban đầu
    var checkCreateShapre = false
    let shapeLayer = CAShapeLayer()
    
    let minThumbColor = #colorLiteral(red: 0.1468058228, green: 0.704572618, blue: 0.9956740737, alpha: 1)
    let maxThumbColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    var tapUiSlider : UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDataFromAPI()
        self.sld_BabyDaily.minimumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.sld_BabyDaily.maximumTrackTintColor = #colorLiteral(red: 0.8513443845, green: 0.8513443845, blue: 0.8513443845, alpha: 1)
        self.sld_BabyDaily.thumbTintColor = self.minThumbColor
        
    }
    
    func setData(_ pregnancy: PregPregnancy?) -> Void {
        self.pregnancy = pregnancy
    }
    
    func loadToday() {
        
        if let pregnancy = self.pregnancy {
            
            var components = Calendar.current.dateComponents([.day], from: pregnancy.start_date, to: Date())
            var today = 0
            if pregnancy.baby_already_born == 1 {
                today = 0
            } else {
                if let day = components.day {
                    today = day
                    if today > 0 {
                        today = today - 1
                    }
                }
            }
            if self.dailyDes.count > today {
                self.mCollectionView.reloadData()
                self.sld_BabyDaily.maximumValue = Float(self.dailyDes.count)
                self.sld_BabyDaily.setValue(Float(today + 1), animated: false)
                self.sld_BabyDaily.thumbTintColor = self.minThumbColor
                self.setTitle(title: "Ngày \(today + 1)")
                
                let trackRect: CGRect = self.sld_BabyDaily.trackRect(forBounds: self.sld_BabyDaily.bounds)
                let thumbRect: CGRect = self.sld_BabyDaily.thumbRect(forBounds: self.sld_BabyDaily.bounds, trackRect: trackRect, value: self.sld_BabyDaily.value)
                
                self.pointWeekSlider = thumbRect.origin.x // Lưu vị trí x ban đầu
                
                self.createShape(self.sld_BabyDaily, startPoint: self.pointWeekSlider) // Vẽ lần đầu
                
                self.mCollectionView.scrollToItem(at: IndexPath.init(item: today, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadDataTodos()
        super.viewDidAppear(animated)
        setupSlider()
    }
    

    func loadDataFromAPI() {
        self.showHud()
        DispatchQueue.main.async {
            self.sld_BabyDaily.maximumValue = Float(self.dailyDes.count)
            DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
                self.setData(pregnancy)
                self.loadToday()
                self.mCollectionView.reloadData()
            }
            self.hideHud()
        }
        self.showHud()
        NetworkManager.shareInstance.apiGetAllDaillies { (dataPregDaily, message, isSuccess) in
            
            self.pregDaily = dataPregDaily
            
            let domain1 = NetworkManager.rootService
            for data in dataPregDaily {
                let url = domain1 + data.daily_description
                self.dailyDes.append(url)
            }
            self.loadToday()
            self.mCollectionView.reloadData()
            self.hideHud()
            
        }
    }
    
    func updateReadDaily(day: Int) {
        NetworkManager.shareInstance.apiReadDaily(dailyId: day) {
            (data, message, isSuccess) in
            
        }
    }
    
    private func setupSlider(){
        self.sld_BabyDaily.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        
        // Thêm tab vào slider -> update lại value
        self.tapUiSlider = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        self.sld_BabyDaily.addGestureRecognizer(self.tapUiSlider)
    }
    
    // Set tab slider và di chuyển Thumb, update lại giá trị
    @objc func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.view)
        
        let positionOfSlider: CGPoint = self.sld_BabyDaily.frame.origin
        let widthOfSlider: CGFloat = self.sld_BabyDaily.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.sld_BabyDaily.maximumValue) / widthOfSlider)
        
        self.sld_BabyDaily.setValue(roundf(Float(newValue)), animated: true)
        
        
        let trackRect: CGRect = self.sld_BabyDaily.trackRect(forBounds: self.sld_BabyDaily.bounds) // Lấy giá trị của uislider
        let thumbRect: CGRect = self.sld_BabyDaily.thumbRect(forBounds: self.sld_BabyDaily.bounds, trackRect: trackRect, value: self.sld_BabyDaily.value) // Lấy giá trị của thumb uislider
        //        print(thumbRect)
        if self.pointWeekSlider < thumbRect.origin.x {
            self.sld_BabyDaily.thumbTintColor = self.maxThumbColor
        } else {
            self.sld_BabyDaily.thumbTintColor = self.minThumbColor
        }
        
        
        self.mCollectionView.scrollToItem(at: IndexPath(item: Int(self.sld_BabyDaily.value - 1), section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)
        
    }
    
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            var sliderValue = slider.value
            if sliderValue < 1 {
                sliderValue  = 1
            }
            switch touchEvent.phase {
            case .began:
                self.sld_BabyDaily.removeGestureRecognizer(self.tapUiSlider)
                break
            case .moved:
                
                let roundedValue = round(sliderValue / step) * step
                slider.value = roundedValue
                
                // Lấy giá trị của thumb image để đổi màu
                let trackRect: CGRect = self.sld_BabyDaily.trackRect(forBounds: self.sld_BabyDaily.bounds)
                let thumbRect: CGRect = self.sld_BabyDaily.thumbRect(forBounds: self.sld_BabyDaily.bounds, trackRect: trackRect, value: self.sld_BabyDaily.value)
                if self.pointWeekSlider < thumbRect.origin.x {

                    self.sld_BabyDaily.thumbTintColor = self.maxThumbColor
                } else {
                    self.sld_BabyDaily.thumbTintColor = self.minThumbColor
                }
                break
            case .ended:
                print("ended")
                let page = Int(sliderValue)
                self.mCollectionView.scrollToItem(at: IndexPath(item: page - 1, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: false)

                // Lấy giá trị của thumb image để đổi màu
                let trackRect: CGRect = self.sld_BabyDaily.trackRect(forBounds: self.sld_BabyDaily.bounds)
                let thumbRect: CGRect = self.sld_BabyDaily.thumbRect(forBounds: self.sld_BabyDaily.bounds, trackRect: trackRect, value: self.sld_BabyDaily.value)
                if self.pointWeekSlider < thumbRect.origin.x {
                    self.sld_BabyDaily.thumbTintColor = self.maxThumbColor
                } else {
                    self.sld_BabyDaily.thumbTintColor = self.minThumbColor
                }
                setTitle(title: "Ngày \(page)")
                
                self.sld_BabyDaily.addGestureRecognizer(self.tapUiSlider)
                break
            default:
                break
            }
        }
    }
    
    @objc func call_Method(sender: AnyObject) {
        self.performSegue(withIdentifier: "BabyDailyToInfo", sender: self)
    }
    
    // api add todo list
    
    private func loadDataTodos() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    self.dataTodos.append(item)
                }
                DispatchQueue.main.async {
                    self.loadUserTodos()
                }
            }
            if (isSuccess == false) {
                if (message == "Authorization has been denied for this request." ){
                    let alert = UIAlertController(title: "Có lỗi xảy ra", message: "Xin vui lòng đăng nhập lại", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        DatabaseManager.sharedManager.logout {
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Huỷ", style: .default, handler: { action in
                        self.hideHud()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func loadUserTodos() {
        self.dataUserTodos = []
        NetworkManager.shareInstance.apiGetUserToDos() {
            (data, message, isSuccess) in
            if isSuccess {
                let data = data as! JSON
                for item in data.arrayValue {
                    self.dataUserTodos.append(item)
                    
                }
                
                DispatchQueue.main.async {
                    self.mCollectionView.reloadData()
                    self.hideHud()
                }
            } else {
                self.hideHud()
            }
        }
    }
    
    // Check scroll thì kiểm tra index của item hiển thị để gán giá trị
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.mCollectionView.contentOffset, size: self.mCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let indexPath = self.mCollectionView.indexPathForItem(at: visiblePoint)
        if let indexPath = indexPath {
            self.sld_BabyDaily.value = Float(indexPath.item + 1)
            
            let trackRect: CGRect = self.sld_BabyDaily.trackRect(forBounds: self.sld_BabyDaily.bounds)
            let thumbRect: CGRect = self.sld_BabyDaily.thumbRect(forBounds: self.sld_BabyDaily.bounds, trackRect: trackRect, value: self.sld_BabyDaily.value)
            if self.pointWeekSlider < thumbRect.origin.x {
                self.sld_BabyDaily.thumbTintColor = self.maxThumbColor
            } else {
                self.sld_BabyDaily.thumbTintColor = self.minThumbColor
            }
            setTitle(title: "Ngày \(indexPath.item + 1)")
        }
    }
}
//
extension BabyDailyViewController: UICollectionViewDelegate {
    
}

extension BabyDailyViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dailyDes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DailyViewCell", for: indexPath) as! DailyViewCell
        
        updateReadDaily(day: indexPath.item + 1)
        cell.mDailyView.mbtnLike.tag = indexPath.item
        cell.mDailyView.mbtnLike.addTarget(self, action: #selector(clickLike(_:)), for: .touchUpInside)
        cell.mDailyView.mbtnDisLike.tag = indexPath.item
        cell.mDailyView.mbtnDisLike.addTarget(self, action: #selector(clickDisLike(_:)), for: .touchUpInside)
        cell.mDailyView.mbtnAddto.tag = indexPath.item
         cell.mDailyView.mbtnAddto.setImage(UIImage(named: "untick"), for: .normal)
        
        if indexPath.item < dataTodos.count {
            if dataTodos[indexPath.item]["title"].stringValue != ""  {
                cell.mDailyView.mlblTodos.text = dataTodos[indexPath.item]["title"].stringValue
                for item in dataUserTodos {
                    if item["todo_id"].intValue == dataTodos[indexPath.item]["id"].intValue {
                        cell.mDailyView.mbtnAddto.setImage(UIImage(named: "tick-3"), for: .normal)
                    }
                }
                
                let textHeight = cell.mDailyView.mlblAddText.height + 30
                cell.mDailyView.mHeightTodos.constant = textHeight
                cell.mDailyView.mSpaceToBottom.constant =  cell.mDailyView.mViewGroupTodo.height
                cell.mDailyView.mbtnAddto.isHidden = false
                cell.mDailyView.mlblAddText.isHidden = false
                cell.mDailyView.mOneLine.isHidden = false
                cell.mDailyView.mViewGroupTodo.isHidden = false
            }
            
            else {
                cell.mDailyView.mHeightTodos.constant = 0
                cell.mDailyView.mbtnAddto.isHidden = true
                cell.mDailyView.mlblTodos.text = ""
                cell.mDailyView.mlblAddText.isHidden = true
                cell.mDailyView.mOneLine.isHidden = true
                cell.mDailyView.mViewGroupTodo.isHidden = true
                cell.mDailyView.mLineTop.isHidden = true
                cell.mDailyView.mHeightViewTodo.constant = 10
            }
        }
        else {
            cell.mDailyView.mHeightTodos.constant = 0
            cell.mDailyView.mbtnAddto.isHidden = true
            cell.mDailyView.mlblTodos.text = ""
            cell.mDailyView.mlblAddText.isHidden = true
            cell.mDailyView.mOneLine.isHidden = true
            cell.mDailyView.mViewGroupTodo.isHidden = true
            cell.mDailyView.mLineTop.isHidden = true
            cell.mDailyView.mHeightViewTodo.constant = 10
        }
        
        if self.pregDaily.isEmpty == false {
            
            let check = self.pregDaily[indexPath.item].like
            cell.mDailyView.checkReaction(check: check)
        }

        
        let url = URL(string: self.dailyDes[indexPath.item])
        
        DispatchQueue.main.async {
            cell.mDailyView.setData(htmlContent: url!)
        }

        return cell
    }
    
    
    // Hành động click like
    
    // 4 trạng thái :
    // 0 . chưa cõ dữ liệu false - false -> post lên
    // 1 . false - false
    // 2 . true - false
    // 3 . false - true
    
    
    @objc func clickLike(_ sender : UIButton) {
        let row = sender.tag
        
//        statusDisLike = 0
        if self.pregDaily[row].like == 0 {
            CreateStatusLike(row: row, dailyID: self.pregDaily[row].id, like: 1)
        } else if self.pregDaily[row].like == 1 {
//            UpdateStatusLike(row: row, dailyID: self.pregDaily[row].id, like: 3)
            deleteStatusLike(row: row, dailyId: self.pregDaily[row].id)
        } else {
            UpdateStatusLike(row: row, dailyID: self.pregDaily[row].id, like: 1)
        }
        
    }
    // Hành động click Dislike
    @objc func clickDisLike(_ sender : UIButton) {
        let row = sender.tag
        
        if self.pregDaily[row].like == 0 {
            CreateStatusLike(row: row, dailyID: self.pregDaily[row].id, like: 2)
        } else if self.pregDaily[row].like == 2 {
            deleteStatusLike(row: row, dailyId: self.pregDaily[row].id)
        } else {
            UpdateStatusLike(row: row, dailyID: self.pregDaily[row].id, like: 2)
        }
    }

    func CreateStatusLike(row:Int, dailyID : Int, like : Int) {
        let backup = self.pregDaily[row].like
        self.pregDaily[row].like = like
        if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
            cell.mDailyView.checkReaction(check: like)
        } else {
            self.mCollectionView.reloadData()
        }
        NetworkManager.shareInstance.apiPostDailyInteract(dailyID: dailyID, like: like) { (data, message, isSuccess) in
            if !isSuccess {
                self.pregDaily[row].like = backup
                if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
                    cell.mDailyView.checkReaction(check: backup)
                } else {
                    self.mCollectionView.reloadData()
                }
            }
        }
    }
    
    func UpdateStatusLike(row:Int, dailyID : Int, like : Int) {
        let backup = self.pregDaily[row].like
        self.pregDaily[row].like = like
        if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
            cell.mDailyView.checkReaction(check: like)
        } else {
            self.mCollectionView.reloadData()
        }
        NetworkManager.shareInstance.apiPutDailyInteract(dailyID: dailyID, like: like) { (data, message, isSuccess) in
            if !isSuccess {
                self.pregDaily[row].like = backup
                if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
                    cell.mDailyView.checkReaction(check: backup)
                } else {
                    self.mCollectionView.reloadData()
                }
            }
        }
    }
    
    func deleteStatusLike(row:Int, dailyId: Int ) {
        let backup = self.pregDaily[row].like
        self.pregDaily[row].like = 0
        if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
            cell.mDailyView.checkReaction(check: 0)
        } else {
            self.mCollectionView.reloadData()
        }
        NetworkManager.shareInstance.apiDeleteDailyInteract(dailyID: dailyId) { (data, message, isSuccess) in
            if !isSuccess {
                self.pregDaily[row].like = backup
                if let cell = self.mCollectionView.visibleCells.first as? DailyViewCell {
                    cell.mDailyView.checkReaction(check: backup)
                } else {
                    self.mCollectionView.reloadData()
                }
            }
        }
    }
    
    
}

extension BabyDailyViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.mCollectionView.width, height: self.mCollectionView.height)
    }
}

extension BabyDailyViewController {
    private func createShape(_ slider : UISlider, startPoint: CGFloat) {
        
        let trackRect: CGRect = slider.trackRect(forBounds: slider.bounds)
        
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: trackRect.origin.x + 0.5, y: trackRect.origin.y + trackRect.height / 2))
        path.addLine(to: CGPoint(x: startPoint + 2, y: trackRect.origin.y + trackRect.height / 2))
        
        //        let shapeLayer = CAShapeLayer()
        self.shapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        self.shapeLayer.strokeColor = minThumbColor.cgColor
        self.shapeLayer.lineWidth = 4
        self.shapeLayer.path = path.cgPath
        
        slider.layer.addSublayer(self.shapeLayer)
        self.shapeLayer.bringToFront()
        
    }
    private func removeShapre() {
        self.shapeLayer.removeFromSuperlayer()
    }
}

class DailyViewCell: UICollectionViewCell {
    @IBOutlet weak var mDailyView: DailyView!
    
}


