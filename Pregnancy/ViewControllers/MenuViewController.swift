//
//  MenuViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/20/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
enum MenuItemIndex: Int {
    case avartar
    case today = 1
    case myAccount
    case pregnancyAndDueDate
    case appSetting
//    case otherApps
    //case shareAndExport
    case tellAFriend
    case pleaseRateUsHere
    case aboutUs
    case contactUs
    //case FAQs
}

class MenuViewController: BaseViewController {
    
    @IBOutlet weak var mLeftLayout: NSLayoutConstraint!
    var mIsFirstTime: Bool = true
    @IBOutlet var mPanGesture: UIPanGestureRecognizer!
    @IBOutlet var mTapGesture: UITapGestureRecognizer!
    var mLastPanPoint: CGPoint?
    
    var currVC : BaseViewController?
    var rowHighlight : Int?
    
    var mangAccount : [dataAccount] = [
        dataAccount(hinh: "", lb: ""),
        dataAccount(hinh: "Acc1", lb: "Trang chủ"),
        dataAccount(hinh: "Acc2", lb: "Tài khoản của tôi"),
        dataAccount(hinh: "Acc3", lb: "Thời gian dự sinh"),
        dataAccount(hinh: "Acc4", lb: "Cài đặt"),
//        dataAccount(hinh: "Acc5", lb: "Ứng dụng khác"),
        //dataAccount(hinh: "share-1", lb: "Share data")
        dataAccount(hinh: "Acc8", lb: "Chia sẻ"),
        dataAccount(hinh: "Acc9", lb: "Đánh giá"),
        dataAccount(hinh: "Acc10", lb: "Giới thiệu"),
        dataAccount(hinh: "Acc11", lb: "Liên hệ"),
        //dataAccount(hinh: "Acc12", lb: "FAQs"),
        
    ]
    var viewController : UIViewController!
    
    @IBOutlet weak var mMenuView: UIView!
    @IBOutlet weak var mTableView: UITableView!
    var headerView:UIView!
    var user: PregUser?
    var completion: ((_ indexPath: IndexPath) -> Void)? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUIs()
        
    }
    
    private func setupUIs() {
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.user = DatabaseManager.sharedManager.getLocalUserInfo()
        if (self.mIsFirstTime) {
            self.mLeftLayout.constant =  -(UIScreen.main.bounds.size.width)
        }
        
        self.checkViewControllerDisplay()
    }
    func checkViewControllerDisplay() {
        if currVC != nil {
            if currVC?.isKind(of: TodayViewController.self) == true {
                rowHighlight = 1
            } else if currVC?.isKind(of: MyAccountLoginViewController.self) == true {
                rowHighlight = 2
            } else if currVC?.isKind(of: DueDateViewController.self) == true {
                rowHighlight = 3
            } else if currVC?.isKind(of: AppSettingViewController.self) == true {
                rowHighlight = 4
            } else if currVC?.isKind(of: MenuRateUsViewController.self) == true {
                rowHighlight = 6
            } else if currVC?.isKind(of: AboutUsViewController.self) == true {
                rowHighlight = 7
            } else if currVC?.isKind(of: MenuTellFriendViewController.self) == true {
                rowHighlight = 8
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, animations: {
            self.mLeftLayout.constant = 0
            self.view.layoutIfNeeded()
        }) { (success) in
            self.mIsFirstTime = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    private func setupTableView() {
        self.mTableView.register(UINib.init(nibName: "AccountTableViewCell", bundle: nil), forCellReuseIdentifier: "AccountTableViewCell")
        self.mTableView.register(UINib.init(nibName: "AccountImageTableViewCell", bundle: nil), forCellReuseIdentifier: "AccountImage")
        self.mTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.mTableView.estimatedRowHeight = 200
        self.mTableView.rowHeight = UITableView.automaticDimension
        
        self.mTableView.separatorColor = UIColor.clear
    }
    
    @objc func closeMenu(completion: (() -> Void)? = nil) -> Void {
        self.closeMenu(false, completion: nil)
    }
    
    @objc func closeMenu(_ animation:Bool, completion: (() -> Void)? = nil) -> Void {
        if (animation) {
            UIView.animate(withDuration: 0.3, animations: {
                self.mLeftLayout.constant = -(UIScreen.main.bounds.size.width)
                self.view.layoutIfNeeded()
            }) { (success) in
                self.mIsFirstTime = true
                
                // Set lại current VC khi dismis menu
                if self.currVC != nil {
                    ViewManager.setCurrentViewController(self.currVC)
                }
                
                self.dismissMenu(completion: completion)
            }
        } else {
            self.mIsFirstTime = true
            
            // Set lại current VC khi dismis menu
            if self.currVC != nil {
                ViewManager.setCurrentViewController(self.currVC)
            }
            
            self.dismissMenu(completion: completion)
        }
    }
    
    @IBAction func didTapOutside(_ sender: Any) {
        self.closeMenu(true, completion: nil)
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (!gestureRecognizer.isEqual(self.mPanGesture)) {
            return touch.view == gestureRecognizer.view
        }
        
        return true
        
    }
    
    @IBAction func handlePan(_ sender: UIPanGestureRecognizer) {
//        print("pan gesture : \(sender.location(in: self.view))" )
//        print("velocty : \(sender.velocity(in: self.view))")
        let touch = sender.location(in: self.view)
        if touch.x < self.mTableView.width {
            let diff = touch.x - self.mTableView.width
            let constant = min(0, diff)
            self.mLeftLayout.constant = constant
            self.view.layoutIfNeeded()
        }
        if sender.state == .ended  ||
            sender.state == .cancelled ||
            sender.state == .failed {
            print("end velocty : \(sender.velocity(in: self.view))")
            if (self.mLeftLayout.constant < -(self.mTableView.width / 2)  ||
                (self.mLeftLayout.constant < 0 &&
                    sender.velocity(in: self.view).x < -100)) {
                self.closeMenu()
            } else {
                self.mLeftLayout.constant = 0
                self.view.layoutIfNeeded()
            }
        }
        
    }
    
}



extension MenuViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mangAccount.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountImage", for: indexPath) as! AccountImageTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.loadAvatarFromDisk()
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccountTableViewCell", for: indexPath) as! AccountTableViewCell
            
            cell.mTitleLabel.text = mangAccount[indexPath.row].lblAccount
            cell.mImageView.image = UIImage(named: mangAccount[indexPath.row].imgAccount)
            switch indexPath.row {
            case 1:
                cell.mViewCell.isHidden = false
            case 4:
                cell.mViewCell.isHidden = false
            case 5:
                cell.mViewCell.isHidden = false
            case 8:
                cell.mViewCell.isHidden = false
            default:
                cell.mViewCell.isHidden = true
            }
            
            cell.backgroundColor = UIColor.clear
            
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            cell.selectionStyle = .none
            
            if self.rowHighlight != nil {
                if indexPath.row == self.rowHighlight {
                    cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.4)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(self.headerView == nil){
            let label = UILabel()
            if let user = self.user {
                label.text = user.last_name  + " " + user.first_name
            }
            
            label.font = UIFont(name: "System", size: 35)
            label.textAlignment = .center
            label.textColor = UIColor.white
            label.sizeToFit()
            self.headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.width, height: 44))
            self.headerView.addSubview(label)
            label.snp.makeConstraints { (make) in
                make.centerX.equalToSuperview()
                make.centerY.equalToSuperview()
            }}
        
        return self.headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
}



extension MenuViewController:UITableViewDelegate {
    
    @objc func updateAvatar(){
      self.mTableView.reloadData()
        if (self.viewController .isKind(of: TodayViewController.self)) {
            let vc = self.viewController as? TodayViewController
            vc?.tblToday.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
  
        if(indexPath.row == MenuItemIndex.avartar.rawValue) {
            AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
            return
        }
        if(indexPath.row == MenuItemIndex.today.rawValue) {
            self.closeMenu(true) {
                self.viewController.navigationController?.popToRootViewController(animated: true)
                ViewManager.getRootTabbarController()?.selectedIndex = 0
            }
            
            return
        }
        if(indexPath.row == MenuItemIndex.myAccount.rawValue) {
            if (!self.viewController.isKind(of: MyAccountLoginViewController.self)) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountLoginViewController") as! MyAccountLoginViewController
                
                self.viewController.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else if indexPath.row == MenuItemIndex.pregnancyAndDueDate.rawValue {
            if (!self.viewController.isKind(of: DueDateViewController.self)) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "DueDateViewController") as! DueDateViewController
                self.viewController.navigationController?.pushViewController(vc, animated: true)
            }
            
        }else if (indexPath.row == MenuItemIndex.appSetting.rawValue) {
            if(!self.viewController.isKind(of: AppSettingViewController.self)) {
                let setting = self.storyboard?.instantiateViewController(withIdentifier: "AppSettingViewController") as! AppSettingViewController
                self.viewController.navigationController?.pushViewController(setting, animated: true)
            }
            
        } else if (indexPath.row == MenuItemIndex.pleaseRateUsHere.rawValue) {
            if(!self.viewController.isKind(of: MenuRateUsViewController.self)) {
                let popRateUs = self.storyboard?.instantiateViewController(withIdentifier: "MenuRateUsViewController") as! MenuRateUsViewController
                
                self.viewController.addChild(popRateUs)
                popRateUs.view.frame = self.viewController.view.frame
                self.viewController.view.addSubview(popRateUs.view)
                popRateUs.didMove(toParent: self.viewController)
                
//                self.viewController.presentMenu(popRateUs)
            }
            
        }/* else if (indexPath.row == MenuItemIndex.otherApps.rawValue) {
            if (!self.viewController.isKind(of: AppointmentListViewController.self)) {
                let moreStoryboard = UIStoryboard.init(name: "More", bundle: nil)
                let otherApps = moreStoryboard.instantiateViewController(withIdentifier: "OtherAppsViewController") as! AppointmentListViewController
                ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: otherApps, at: 4, backTo:ViewManager.shareInstance.rootTabbarController?.selectedIndex )
            }
            
            print("Click row 5")
        }*/ else if (indexPath.row == MenuItemIndex.contactUs.rawValue) {
//            if(!self.viewController.isKind(of: MenuContactUsViewController.self)) {
//                let contactUs = self.storyboard?.instantiateViewController(withIdentifier: "MenuContactUsViewController") as! MenuContactUsViewController
//
//                self.viewController.presentMenu(contactUs)
//            }
            let activityController = UIActivityViewController(activityItems: [""], applicationActivities: nil)
            activityController.setValue("Phản ảnh lại với chúng tôi", forKey: "subject")
            self.viewController.present(activityController, animated: true, completion: nil)
            
        } else if indexPath.row == MenuItemIndex.aboutUs.rawValue {
            if(!self.viewController.isKind(of: AboutUsViewController.self)) {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as! AboutUsViewController
                self.viewController.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else if indexPath.row == MenuItemIndex.tellAFriend.rawValue {
            if(!self.viewController.isKind(of: MenuTellFriendViewController.self)) {
                let tellFriendVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuTellFriendViewController") as! MenuTellFriendViewController
                
                self.viewController.addChild(tellFriendVC)
                tellFriendVC.view.frame = self.viewController.view.frame
                self.viewController.view.addSubview(tellFriendVC.view)
                tellFriendVC.didMove(toParent: self.viewController)
                
//                self.viewController.presentMenu(vc)
            }
            
        }
        
        self.closeMenu()
        
    }
}
