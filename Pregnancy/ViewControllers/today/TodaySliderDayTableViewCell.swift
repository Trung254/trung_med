
//
//  TodaySliderDayTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodaySliderDayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var uvBigSlide: UIView!
    @IBOutlet weak var uvSmallSlide: UIView!
    @IBOutlet weak var mlblDays: UILabel!
    @IBOutlet weak var mlblWeeks: UILabel!
    @IBOutlet weak var mWidthSlider: NSLayoutConstraint!
    @IBOutlet weak var mTrimesterLbl: UILabel!
    @IBOutlet weak var mDay: UILabel!
    @IBOutlet weak var mWeek: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        uvBigSlide.layer.cornerRadius = 5
        
        uvSmallSlide.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
