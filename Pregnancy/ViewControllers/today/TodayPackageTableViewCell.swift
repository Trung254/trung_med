//
//  TodayPackageTableViewCell.swift
//  Pregnancy
//
//  Created by dady on 2/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class TodayPackageTableViewCell: UITableViewCell {
    @IBOutlet weak var mPackageView: UIView!
    @IBOutlet weak var mCornerView: UIView!
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mPackageView.layer.cornerRadius = 5
        mCornerView.layer.cornerRadius = mCornerView.layer.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
