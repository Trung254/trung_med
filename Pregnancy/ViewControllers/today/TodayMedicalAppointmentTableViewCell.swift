//
//  TodayMedicalAppointmentTableViewCell.swift
//  Pregnancy
//
//  Created by dady on 2/23/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class TodayMedicalAppointmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mAppointment: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mAppointment.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
