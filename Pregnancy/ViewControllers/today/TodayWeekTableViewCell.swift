//
//  TodayWeekTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayWeekTableViewCell: UITableViewCell {

    @IBOutlet weak var uvWeekIcon: UIView!
    @IBOutlet weak var uvWeekBox: UIView!
    @IBOutlet weak var mlblWeek: UILabel!
    @IBOutlet weak var mlblWeek1: UILabel!
    @IBOutlet weak var mTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvWeekBox.layer.cornerRadius = 5
         uvWeekIcon.layer.cornerRadius = uvWeekIcon.layer.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
