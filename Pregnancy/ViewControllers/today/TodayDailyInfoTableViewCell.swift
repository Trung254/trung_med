//
//  TodayDailyInfoTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayDailyInfoTableViewCell: UITableViewCell {

    
    @IBOutlet weak var uvDailyInfoBox: UIView!
    @IBOutlet weak var uvDailyIcon: UIView!
    @IBOutlet weak var mlblDays: UILabel!
    @IBOutlet weak var mImgDailly: UIImageView!
    @IBOutlet weak var mlblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
        uvDailyInfoBox.layer.cornerRadius = 5
        uvDailyIcon.layer.cornerRadius = uvDailyIcon.layer.frame.size.width / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
