//
//  TodayBabyNameTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayBabyNameTableViewCell: UITableViewCell {

    @IBOutlet weak var uvBabyNameBox: UIView!
    @IBOutlet weak var mSecondView: UIView!
    @IBOutlet weak var mFirstView: UIView!
    @IBOutlet weak var mlblBoy: UILabel!
    @IBOutlet weak var mlblGirl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.uvBabyNameBox.layer.cornerRadius = 5
        self.mSecondView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
