//
//  TodayMonthWeightTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Charts

class TodayMonthWeightTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCurrentWeight: UILabel!
    @IBOutlet weak var uvMonthWeightBox: UIView!
    @IBOutlet weak var btnMyBelly: UIButton!
//    @IBOutlet weak var btnMyWeight: UIButton!
    @IBOutlet weak var mlblMonths: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    
    @IBOutlet weak var clickToMyWeight: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvMonthWeightBox.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
