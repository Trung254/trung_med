//
//  TodayViewController.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/11/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import RealmSwift
import Charts

var checkReload = false
var time:Timer?

class TodayViewController: BaseViewController {
    
    @IBOutlet weak var tblToday: UITableView!
    
    var dueDate: Date?
    var startDate = ""
    var widthSlider:Float = 0.0
    var endWeek = 0
    var endDays = 0
    var totalDays: Float = 280.0
    var username = ""
    var mRunTextAnimation = true
    var imageResult: Results<PregImage>?
    var illustrateBellyResult: Results<PregMyBelly>?
    
    var pregMonth: PregMyBelly?
    var firstName = ""
    var pregSizeGuide: [PregSizeGuide]?
    var sizeGuide: PregSizeGuide?
    
    var usertodos: [JSON] = []
    var todos: [JSON] = []
    var userbabynames: [JSON] = []
    var babynames: [JSON] = []
    var daillies: [PregDaily] = []
    var products: Array<JSON> = []
    var yourPackage: [String] = []
    
    var curentWeight = ""
    var checkDataChartEmpty = 0
    
    var dataChart : [ChartDataEntry]?
    var nameboy = ""
    var namegirl = ""
    var babyAlredyBorn = false
    var muleday: Int?
    var title_weeks: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblToday.estimatedRowHeight = 300
        self.tblToday.rowHeight = UITableView.automaticDimension
        
        tblToday.backgroundView = UIImageView(image: UIImage(named: "background-today"))
        self.setLeftMainMenu()
        self.setupCell()
        self.setTitle(title: "Trang chủ")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.tblToday.scrollToRow(at: indexPath, at: .top, animated: false)
        }
        tblToday.reloadData()
        
        var count = 0
        checkReload = false
        
        self.mIsShowRightButton = false
        self.addPadLockButton()
        super.viewWillAppear(animated)
        
        DrawChartPreg.shareInstance.CreateDataChart { (dataPreMyWeight) in
            if dataPreMyWeight.isEmpty == false {
                self.checkDataChartEmpty = 1
                self.curentWeight = String(format: "%.1f", (dataPreMyWeight.last?.current_weight)!)
                
                DrawChartPreg.shareInstance.setDataMePush(dataWei: dataPreMyWeight, callBack: { (chartDataEntry) in
                    self.dataChart = chartDataEntry
                })
            } else {
                self.checkDataChartEmpty = 2
                self.dataChart = (0..<9).map { (i) -> ChartDataEntry in
                    return ChartDataEntry(x: Double(i), y: Double(i))
                }
            }
        }
        
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                switch pregnancy.baby_already_born {
                case 1 :
                    self.babyAlredyBorn = true
                    self.weeks = 0
                default :
                    self.babyAlredyBorn = false
                    let dateFormatter = DateFormatter()
                    
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    
                    let date = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date)
                    
                    if date.day! < 0 {
                        self.babyAlredyBorn = true
                        self.weeks = 0
                        DatabaseManager.sharedManager.putPregnancy(showWeek: 1, babyGender: pregnancy.baby_gender, dueDate: nil, startDate: nil, dateOfBirth: dateFormatter.string(from: Date()), babyAlreadyBorn: 1, weightBeforePregnant: nil) { (pregUser, isTemp, isSuccess) in
                            if (isSuccess == false) {
                            }
                            if(!isTemp) {
//                                NetworkManager.shareInstance.apiDeleteAllMyWeights(callBack: { (data, message, isSuccess) in
//
//                                    NotificationManager().scheduleNotification()
//
//                                })
                            }
                        }
                    }
                }
                
                
                //
                var components = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date.addingTimeInterval(86400))
                
                if var day = components.day {
                    self.muleday = day
                    if (day < 0 ) {
                        day = 0
                    } else {
                        day = 280 - day
                    }
                    self.weeks = day / 7
                    if (pregnancy.show_week == 1){
                        self.weeks = day / 7 + 1
                    }
                    self.days = day - (self.weeks! * 7 )
                } else {
                    self.days = 0
                    self.weeks = 0
                }
                
                self.dueDate = pregnancy.due_date
                
                if count < 1 {
                    self.tblToday.reloadData()
                    count += 1
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.mRunTextAnimation = true
        if let time = time {
            time.invalidate()
        }
        
    }
    
    private func getAllApi() {
        self.showHud()
        NetworkManager.shareInstance.apiGetAllSizeguides { (items, message, isSuccess) in
            self.pregSizeGuide = items
            self.sizeGuide = nil
            if checkReload == true {
                self.tblToday.reloadData()
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetUserToDos() {
            (data,message,isSuccess) in
            let message = message
            if isSuccess {
                let data = data as? JSON
                self.usertodos = []
                if data?.arrayValue.count != 0 {
                    for item in (data?.arrayValue)! {
                        if item["status"].intValue == 0 {
                            self.usertodos.append(item)
                        }
                    }
                }
            }
            if checkReload == true {
                self.tblToday.reloadData()
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetAllToDos() {
            (data,message,isSuccess) in
            if isSuccess {
                let data = data as? JSON
                self.todos = (data?.arrayValue)!
            }
            if checkReload == true {
                self.tblToday.reloadData()
            }
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetAllBabyName(countryId: 1) {
            (data,message,isSuccess) in
            self.babynames = []
            if isSuccess {
                let data = data as? JSON
                self.babynames = (data?.arrayValue)!
                
                NetworkManager.shareInstance.apiGetAllUserBabyName() {
                    (data,message,isSuccess) in
                    self.userbabynames = []
                    if isSuccess {
                        let data = data as? JSON
                        self.userbabynames = (data?.arrayValue)!
                        
                        self.nameboy = ""
                        self.namegirl = ""
                        if self.userbabynames.isEmpty == false {
                            var countboy = 0
                            var countgirl = 0
                            for i in self.babynames {
                                for j in self.userbabynames {
                                    if j["baby_name_id"].intValue == i["id"].intValue {
                                        if i["gender_id"].intValue == 1 && countboy < 2 {
                                            self.nameboy += "\(i["name"].stringValue)\n"
                                            countboy += 1
                                            
                                            if countboy == 2 {
                                                self.nameboy += "....."
                                            }
                                        }
                                        else if i["gender_id"].intValue == 2 && countgirl < 2 {
                                            self.namegirl += "\(i["name"].stringValue)\n"
                                            countgirl += 1
                                            
                                            if countgirl == 2 {
                                                self.namegirl += "....."
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.tblToday.reloadData()
                        self.hideHud()
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.hideHud()
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetAllDaillies {
            (dataPregDaily, message, isSuccess) in
            if isSuccess {
//                let domain = NetworkManager.rootDomain
                for data in dataPregDaily {
                    let url = data.hingline_image
                    data.hingline_image = url
                    
                    self.daillies.append(data)
                }
            }
            
            DispatchQueue.main.async {
                if checkReload == true {
                    self.tblToday.reloadData()
                }
                self.hideHud()
            }
        }
        
        if var weekId = self.weeks {
            if weekId == 0 {
                weekId = 1
            }
            self.showHud()
            NetworkManager.shareInstance.apiGetWeeks(weekId: weekId) { (data, message, isSuccess) in
                if isSuccess {
                    let data = data as? JSON
                    if (message == "Data not found."){
                        self.title_weeks = ""
                    } else {
                        self.title_weeks = data?.arrayValue[0]["title"].stringValue ?? ""
                    }
                }
                
                DispatchQueue.main.async {
                    if checkReload == true {
                        self.tblToday.reloadData()
                    }
                    self.hideHud()
                }
            }
        }
        
        self.showHud()
        NetworkManager.shareInstance.apiGetAppointmentProducts(id: "18") { (data, message, isSuccess) in
            if (isSuccess) {
                self.yourPackage.removeAll()
                
                if let data = data as? JSON {
                    if let value = data.dictionary {
                        if let products = value["Products"]?.array {
                            self.products = products
                            self.products.sort{$0["Id"].intValue < $1["Id"].intValue}

                            for item in products {
                                let id = item["Id"].intValue
                                let name = item["Name"].stringValue
                                let url_image = item["DefaultPictureModel"]["FullSizeImageUrl"].stringValue

                                NetworkManager.shareInstance.apiGetProducts(id_product: id, callBack: { (data, message, isSuccess) in
                                    if isSuccess {
                                        if let data = data as? JSON {
                                            if let value = data.dictionary {
                                                if let products = value["products"]?.array {
                                                    let attibutes = products[0]["attributes"].arrayValue
                                                    let result = attibutes.filter({ $0["product_attribute_name"].stringValue == "Gợi ý"})
                                                    if result.count != 0 {
                                                        let first_week = result[0]["attribute_values"][0]["weight_adjustment"].intValue
                                                        let last_week = result[0]["attribute_values"][1]["weight_adjustment"].intValue
                                                        if let week = self.weeks {
                                                            if first_week <= week, last_week >= week {
                                                                self.yourPackage.append("\(id)")
                                                                self.yourPackage.append(name)
                                                                self.yourPackage.append(url_image)
                                                                DispatchQueue.main.async {
                                                                    self.tblToday.reloadData()
                                                                }
                                                                
                                                                return
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    DispatchQueue.main.async {
                                        self.hideHud()
                                    }
                                })
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    if checkReload == true {
                        self.tblToday.reloadData()
                    }
                    self.hideHud()
                }
            }
        }
        
        self.getIllustrateBellyBy()
        self.getBelly(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.tblToday.contentOffset.y = 0
//        self.tblToday.setContentOffset(.zero, animated: false)
    }
    
    override func hideTabBar(hidden: Bool, animated: Bool) {
        super.hideTabBar(hidden: hidden, animated: animated)
    }
    
    func getIllustrateBellyBy(_ filter:Bool = false) {
        DatabaseManager.sharedManager.getImages(type: 1) { (result, isTemp, isSuccess) in
            if(isSuccess) {
                
                if let week = self.weeks, filter {
                    if week == 0 {
                        self.imageResult = result?.filter("week_id == \(week+4)")
                    }
                    else if week > 0 {
                        self.imageResult = result?.filter("week_id == \(week)")
                    }
                } else {
                    self.imageResult = result
                }
                if checkReload == true {
                    self.tblToday.reloadData()
                }
            }
        }
    }
    
    func getBelly(_ filter:Bool = false) {
        DatabaseManager.sharedManager.getIllustrateBellyBy(month: nil) { (result, isTemp, isSuccess) in
            if(isSuccess) {
                if let day = self.days, let week = self.weeks ,filter {
                    let month = min((day + week * 7) / 30 + 1, 9)
                    self.illustrateBellyResult = result?.filter("month == \(month)")
                } else {
                    self.illustrateBellyResult = result
                }
                if checkReload == true {
                    self.tblToday.reloadData()
                }
            }
        }
    }
    
    override func tabBar(_ tabbar: UITabBar, rect: CGRect, hidden: Bool) {
        super.tabBar(tabbar, rect: rect, hidden: hidden)
        if #available(iOS 11.0, *) {
            var edge: UIEdgeInsets = self.tblToday.adjustedContentInset
            if (hidden) {
                edge.bottom = -edge.bottom
            } else {
                edge.bottom = 0
            }
            self.tblToday.contentInset = edge
            
        }
        
    }
    
    override func isUpdateTabBarWhenScrolling() -> Bool {
        return true
    }
    
    func setupCell() {
        tblToday.register(UINib.init(nibName: "TodayAvatarTableViewCell", bundle: nil), forCellReuseIdentifier: "todayAvatarTableViewCell")
        tblToday.register(UINib.init(nibName: "TodaySliderDayTableViewCell", bundle: nil), forCellReuseIdentifier: "todaySliderDayTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayDailyInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "todayDailyInfoTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayDueDateTableViewCell", bundle: nil), forCellReuseIdentifier: "todayDueDateTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayPackageTableViewCell", bundle: nil), forCellReuseIdentifier: "TodayPackageTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayMedicalAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "TodayMedicalAppointmentTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayWeekTableViewCell", bundle: nil), forCellReuseIdentifier: "todayWeekTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayWeekBabySizeTableViewCell", bundle: nil), forCellReuseIdentifier: "todayWeekBabySizeTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayMonthWeightTableViewCell", bundle: nil), forCellReuseIdentifier: "todayMonthWeightTableViewCell")
        //        tblToday.register(UINib.init(nibName: "TodayAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "todayAppointmentTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayTaskTableViewCell", bundle: nil), forCellReuseIdentifier: "todayTaskTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayBabyNameTableViewCell", bundle: nil), forCellReuseIdentifier: "todayBabyNameTableViewCell")
//        tblToday.register(UINib.init(nibName: "TodayPremiumTableViewCell", bundle: nil), forCellReuseIdentifier: "todayPremiumTableViewCell")
        tblToday.register(UINib.init(nibName: "TodayFacebookTableViewCell", bundle: nil), forCellReuseIdentifier: "todayFacebookTableViewCell")
    }
    
    override func handleLeftMenu() {
        self.showMainMenu()
    }
    
    // notification when change first name
    override func onDidUpdateUserInformations(_ notification: Notification) {
        if checkReload == true {
            self.tblToday.reloadData()
        }
    }
    
    @objc func btnShowBabyImages(_ sender : UIButton){
        let mBabyImagesViewController = UIStoryboard(name: "Baby", bundle: nil).instantiateViewController(withIdentifier: "BabyImagesViewController") as! BabyImagesViewController
        ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mBabyImagesViewController, at: 2, backTo: 0)
    }
    
    @objc func btnShowBabySize(_ sender : UIButton){
        let mBabySize = UIStoryboard(name: "Baby", bundle: nil).instantiateViewController(withIdentifier: "SizeGuideViewController") as! SizeGuideViewController
        ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mBabySize, at: 2, backTo: 0)
    }
    
    @objc func btnShowMyBelly(_ sender : UIButton){
        let mMyBelly = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "MyBelly") as! MyBellyViewController
        //        mMyBelly.curMonth = (days + weeks*7)/30 + 1
        ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mMyBelly, at: 3, backTo: 0)
    }
    
    @objc func btnShowMyWeight(_ sender : UIButton){
        let mMyWeight = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "MyWeightViewController") as! MyWeightViewController
        mMyWeight.checkDataFromToday = true
        ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mMyWeight, at: 3, backTo: 0)
    }
    
    @objc func btnShowMyAccount(_ sender : UIButton){
        let mMyAccount = UIStoryboard(name: "menu", bundle: nil).instantiateViewController(withIdentifier: "MyAccountLoginViewController") as! MyAccountLoginViewController
        self.navigationController?.pushViewController(mMyAccount, animated: true)
    }
    
    private func convertDateToString(_ dataDate: Date,_ typeFormat: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = typeFormat
        dateFormatter.locale = Locale(identifier: "vi_VI")
        let dateString = dateFormatter.string(from: dataDate)
        
        return dateString
    }
}

extension TodayViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
//    func clearData() {
//        if self.usertodos.isEmpty == false {
//            self.usertodos.removeAll()
//        }
//        if self.todos.isEmpty == false {
//            self.todos.removeAll()
//        }
//        self.getAllApi()
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "defaultCell")
        
        let user = DatabaseManager.sharedManager.getLocalUserInfo()
        if indexPath.row == 0 {
            let todayAvatarCell = tableView.dequeueReusableCell(withIdentifier: "todayAvatarTableViewCell", for: indexPath) as! TodayAvatarTableViewCell
            todayAvatarCell.setavatarimage()
            todayAvatarCell.mbtnAvatar.removeTarget(nil, action: nil, for: .allEvents)
            todayAvatarCell.mbtnAvatar.addTarget(self, action: #selector(self.btnShowMyAccount(_:)), for: .touchUpInside)
            
            var text: String
            var username: String = ""
            if let user = user {
                if user.first_name.count > 0 {
                    username = " \(user.first_name)"
                }
            }
            
            if self.babyAlredyBorn == true {
                text = "Xin chào\(username),chào mừng sự ra đời của con bạn"
            } else {
                if let days = self.days, days > 0 {
                    text = "Xin chào\(username), bạn mang thai được \(weeks ?? 0) tuần và \(days) ngày"
                }
                else {
                    let weeksPresent = weeks! - 1
                    let daysPresent = days! + 7
                    text = "Xin chào\(username), bạn mang thai được \(weeksPresent) tuần và \(daysPresent) ngày"
                }
            }
            
            if(self.mRunTextAnimation) {
                todayAvatarCell.mTextView.text = ""
                self.mRunTextAnimation = false
                todayAvatarCell.mTextView.typeOn(text, 0.08)
                RunLoop.current.add(time!, forMode: RunLoop.Mode.common)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.08 * Double(text.count) + 0.1) {
                    checkReload = true
                    self.getAllApi()
//                    self.clearData()
                }
            }
            
            cell = todayAvatarCell
        }
            
        else if indexPath.row == 1 {
            let todaySliderCell = tableView.dequeueReusableCell(withIdentifier: "todaySliderDayTableViewCell", for: indexPath) as! TodaySliderDayTableViewCell
            
            if self.babyAlredyBorn == true {
                todaySliderCell.mlblDays.text = ""
                todaySliderCell.mlblWeeks.text = ""
                todaySliderCell.mTrimesterLbl.text = ""
                todaySliderCell.uvSmallSlide.isHidden = true
                todaySliderCell.mDay.isHidden = true
                todaySliderCell.mWeek.isHidden = true
            } else {
                todaySliderCell.mDay.isHidden = false
                todaySliderCell.mWeek.isHidden = false
                todaySliderCell.uvSmallSlide.isHidden = false
                if let days = self.days, let weeks = self.weeks {
                    var month = (days + weeks*7)/30 + 1
                    
                    if month > 9 {
                        month = 9
                    }
                    
                    if month <= 3  {
                        todaySliderCell.mTrimesterLbl.text = "3 tháng đầu thai kỳ"
                    } else if month <= 6 {
                        todaySliderCell.mTrimesterLbl.text = "3 tháng giữa thai kỳ"
                    } else if month <= 9 {
                        todaySliderCell.mTrimesterLbl.text = "3 tháng cuối thai kỳ"
                    }
                    
                    todaySliderCell.mlblDays.text = "\(days + weeks*7)"
                    todaySliderCell.mlblWeeks.text = "\(weeks)"
                    
                    let totalWidth = Float(todaySliderCell.uvBigSlide.size.width)
                    let curDays = Float(days + weeks*7)
                    self.widthSlider = (curDays/totalDays * totalWidth)
                    todaySliderCell.mWidthSlider.constant = CGFloat(widthSlider)
                }
            }
            cell = todaySliderCell
            
        }
            
        else if indexPath.row == 2 {
            let todayDailyCell = tableView.dequeueReusableCell(withIdentifier: "todayDailyInfoTableViewCell", for: indexPath) as! TodayDailyInfoTableViewCell
            
            if self.babyAlredyBorn == true {
                todayDailyCell.mImgDailly.isHidden = true
                todayDailyCell.mlblDays.text = "..."
                todayDailyCell.mlblTitle.text = "Thời gian dự sinh đang tắt"
            } else {
                todayDailyCell.mImgDailly.isHidden = false
                if var days = self.days, let weeks = self.weeks {
                if (days == 0 && weeks == 0) {
                    days = 1
                    todayDailyCell.mlblDays.text = "\(0 + weeks*7)"
                }else {
                    todayDailyCell.mlblDays.text = "\(days + weeks*7)"
                    }
                
                var day = days + weeks*7
                if daillies != [] && (day) < daillies.count {
                    let daily = daillies.first { (daily) -> Bool in
                        if day == 0 {
                            day = 1
                        }
                        return daily.id == day
                    }
                    if let daily = daily {
                        let title = daily.title
                        if title != "" {
                            todayDailyCell.mlblTitle.text = title
                        }
                        todayDailyCell.mImgDailly.image = nil
                        let urlStr = daily.hingline_image
                        if urlStr != (NetworkManager.rootDomain) {
                            if let url = URL(string: urlStr) {
                                todayDailyCell.mImgDailly.sd_setImage(with: url, placeholderImage: UIImage(named: ""), options: .highPriority) { (image, eror, cacheType, loadedUrl) in
                                    todayDailyCell.mImgDailly.image = image
                                }
                            }
                        }
                    } else {
                        todayDailyCell.mlblTitle.text = ""
                        todayDailyCell.mImgDailly.image = nil
                    }
                    
                }
            }
            }
            cell = todayDailyCell
        }
            
        else if indexPath.row == 3 {
            let todayDueCell = tableView.dequeueReusableCell(withIdentifier: "todayDueDateTableViewCell", for: indexPath) as! TodayDueDateTableViewCell
            if self.babyAlredyBorn == true {
                todayDueCell.mlblDueDate.text = "Thời gian dự sinh đang tắt"
                todayDueCell.mlblMonth.text = ""
                todayDueCell.mlblDate.text = ""
                todayDueCell.mlblSetup.text = "Đã sinh"
            } else {
                if muleday ?? 0 < 0{
                    muleday = 0
                }
                todayDueCell.mlblSetup.text = "\(muleday ?? 0) ngày còn lại"
                
                if let date = self.dueDate {
                    
                    //                let date = self.transferStringToDate(dueDate, "yyyy-MM-dd'T'HH:mm:ss")
                    todayDueCell.mlblDueDate.text = "Ngày dự sinh: \(self.convertDateToString(date, "dd MMMM, yyyy" ))"
                    
                    todayDueCell.mlblMonth.text = "\(self.convertDateToString(date, "MMMM" ))"
                    todayDueCell.mlblDate.text = "\(self.convertDateToString(date, "dd"))"
                }
            }
            cell = todayDueCell
        }
            
        else if indexPath.row == 4 {
            let todayPackage = tableView.dequeueReusableCell(withIdentifier: "TodayPackageTableViewCell", for: indexPath) as! TodayPackageTableViewCell

            if yourPackage.count != 0 {
                if yourPackage[1] != "" {
                    todayPackage.mName.text = yourPackage[1]
                }
                
                if yourPackage[2] != "" {
                    if let url = URL(string: yourPackage[2]) {
                        if let data = try? Data(contentsOf: url) {
                            todayPackage.mImage.image = UIImage(data: data)
                        }
                    }
                }
            } else {
                todayPackage.mName.text = "Không có gói khám"
                todayPackage.mImage.image = #imageLiteral(resourceName: "today-info")
            }
            
            cell = todayPackage
        }

        else if indexPath.row == 5 {
            let todayMedicalAppointment = tableView.dequeueReusableCell(withIdentifier: "TodayMedicalAppointmentTableViewCell", for: indexPath) as! TodayMedicalAppointmentTableViewCell

            cell = todayMedicalAppointment
        }
            
        else if indexPath.row == 6 {
            let todayWeekCell = tableView.dequeueReusableCell(withIdentifier: "todayWeekTableViewCell", for: indexPath) as! TodayWeekTableViewCell
            if self.babyAlredyBorn == true {
                todayWeekCell.mlblWeek.text = "..."
                todayWeekCell.mlblWeek1.text = "Cài đặt thời gian dự sinh của bạn"
                todayWeekCell.mTitle.text = "Thời gian dự sinh đang tắt"
            } else {
                todayWeekCell.mTitle.text = title_weeks
                if var weeks = self.weeks {
//                    if (weeks == 0) {
////                        weeks = 1
//                    }
                    todayWeekCell.mlblWeek.text = "\(weeks)"
                    todayWeekCell.mlblWeek1.text = "Tìm hiểu về tuần mang thai thứ \(weeks)"
                }
            }
            cell = todayWeekCell
        }
            
        else if indexPath.row == 7{
            let todayWeekBabyCell = tableView.dequeueReusableCell(withIdentifier: "todayWeekBabySizeTableViewCell", for: indexPath) as! TodayWeekBabySizeTableViewCell
            
            todayWeekBabyCell.btnBabyImages.removeTarget(nil, action: nil, for: .allEvents)
            todayWeekBabyCell.btnBabyImages.addTarget(self, action: #selector(self.btnShowBabyImages(_:)), for: .touchUpInside)
            
            
            if self.babyAlredyBorn == true {
                todayWeekBabyCell.mlblWeek.text = "Thời gian dự sinh đang tắt"
            } else {
                if var week =  self.weeks {
                    if (week == 0 ) {
                        week = 1
                        todayWeekBabyCell.mlblWeek.text = "Ảnh tuần \(0)"
                    }else {
                        todayWeekBabyCell.mlblWeek.text = "Ảnh tuần \(week)"
                    }
                }
            }
            
            todayWeekBabyCell.btnBabyImages.sd_cancelImageLoad(for: UIControl.State.normal)
            todayWeekBabyCell.btnBabyImages.imageView?.contentMode = .scaleAspectFill
            
            if let result = self.imageResult, let week = self.weeks {
                if self.babyAlredyBorn == true {
                    if let data = result.first {
                        todayWeekBabyCell.btnBabyImages.sd_setImage(with: URL(string: data.image), for: UIControl.State.normal, completed:nil)
                    }
                } else {
                for data in result {
                    if data.week_id == week {
                      todayWeekBabyCell.btnBabyImages.sd_setImage(with: URL(string: data.image), for: UIControl.State.normal, completed:nil)
                        }
                    }
                }
            }
            
            todayWeekBabyCell.btnBabySize.removeTarget(nil, action: nil, for: .allEvents)
            todayWeekBabyCell.btnBabySize.addTarget(self, action: #selector(self.btnShowBabySize(_:)), for: .touchUpInside)
            todayWeekBabyCell.btnBabySize.imageView?.contentMode = .scaleAspectFit
            
            todayWeekBabyCell.btnBabySize.setImage(nil, for: .normal)
            todayWeekBabyCell.mSizeGuideLbl.text = "Kích cỡ em bé: "
            
            if var week = self.weeks {
                if (week < 1) {
                    week = 1
                }
                if let sizeGuide = self.sizeGuide, sizeGuide.week_id == week {
                    if let url = URL(string:NetworkManager.rootDomain + sizeGuide.image) {
                        todayWeekBabyCell.btnBabySize.sd_setImage(with: url, for: .normal, completed: nil)
                    }
                    todayWeekBabyCell.mSizeGuideLbl.text = "Kích cỡ em bé: \(sizeGuide.title)"
                    
                } else {
                    self.sizeGuide = nil
                    if let pregSizeGuide = self.pregSizeGuide, var week = self.weeks {
                        if self.babyAlredyBorn == true {
                            week = 4
                        }
                        if week < 4 {
                        let weeksizeguide = 4
                        for size in pregSizeGuide {
                            if(size.week_id == weeksizeguide) {
                                self.sizeGuide = size
                                break
                            }
                            }
                        } else {
                            for size in pregSizeGuide {
                                if(size.week_id == week) {
                                    self.sizeGuide = size
                                    break
                                }
                            }
                        }
                        if let sizeGuide = self.sizeGuide {
                            if let url = URL(string:NetworkManager.rootDomain + sizeGuide.image) {
                                todayWeekBabyCell.btnBabySize.sd_setImage(with: url, for: .normal, completed: nil)
                                todayWeekBabyCell.mSizeGuideLbl.text = "Kích cỡ em bé: \(sizeGuide.title)"
                            }
                        }
                    }
                }
            }
            
            if self.babyAlredyBorn == true {
                todayWeekBabyCell.mSizeGuideLbl.text = "Thời gian dự sinh đang tắt"
            }
            
            cell = todayWeekBabyCell
        }
            
        else if indexPath.row == 8 {
            let todayMonthWeightCell = tableView.dequeueReusableCell(withIdentifier: "todayMonthWeightTableViewCell", for: indexPath) as! TodayMonthWeightTableViewCell
            
            todayMonthWeightCell.btnMyBelly.removeTarget(nil, action: nil, for: .allEvents)
            todayMonthWeightCell.btnMyBelly.addTarget(self, action: #selector(self.btnShowMyBelly(_:)), for: .touchUpInside)
            
            todayMonthWeightCell.clickToMyWeight.removeTarget(nil, action: nil, for: .allEvents)
            todayMonthWeightCell.clickToMyWeight.addTarget(self, action: #selector(self.btnShowMyWeight(_:)), for: .touchUpInside)
            
            var cur_weight = self.curentWeight
            if cur_weight == "50.0" {
                let index = cur_weight.index(of: ".")!
                cur_weight = String(cur_weight[..<index])
            }
            todayMonthWeightCell.lblCurrentWeight.text = "Cân nặng hiện tại: " + cur_weight
            
            let chartView = todayMonthWeightCell.lineChartView
            
            if let dataChart = self.dataChart {
                if self.checkDataChartEmpty == 1 {
                    DrawChartPreg.shareInstance.drawChart(lineChartView: chartView!, dataEntry: dataChart)
                } else if self.checkDataChartEmpty == 2 {
                    todayMonthWeightCell.lblCurrentWeight.text = "Nhập cân nặng"
                    DrawChartPreg.shareInstance.drawChart(lineChartView: chartView!, dataEntry: dataChart)
                }
            }
            
            //            ChartData.shareInstance.setDataCount(lineChartView: chartView!, dataEntry: self.dataChart!)
            //            ChartData.shareInstance.setDataCount(lineChartView: chartView, dataEntry: self.dataChart)
            
            if let days = self.days, let weeks = self.weeks {
                var month = (days + weeks*7)/30 + 1
                if month > 9 {
                    month = 9
                }
                
                if self.babyAlredyBorn == true {
                    todayMonthWeightCell.mlblMonths.text = "Thời gian dự sinh đang tắt"
                    month = 1
                } else {
                    todayMonthWeightCell.mlblMonths.text = "Tháng thứ \(month) mang bầu"
                }
            }
            
            todayMonthWeightCell.btnMyBelly.sd_cancelImageLoad(for: UIControl.State.normal)
            todayMonthWeightCell.btnMyBelly.imageView?.contentMode = .scaleAspectFit
            
            if let result = self.illustrateBellyResult {
                if let data = result.first {
                    todayMonthWeightCell.btnMyBelly.sd_setImage(with: URL(string: data.image), for: UIControl.State.normal, completed: { (image, error, cacheType, url) in
                        if let error = error {
                            print(error.localizedDescription)
                        }
                    })
                } else {
                    todayMonthWeightCell.btnMyBelly.setImage(nil, for: .normal)
                }
            } else {
                todayMonthWeightCell.btnMyBelly.setImage(nil, for: .normal)
            }
            cell = todayMonthWeightCell
        }
            //        else if indexPath.row == 7{
            //            cell = tableView.dequeueReusableCell(withIdentifier: "todayAppointmentTableViewCell", for: indexPath) as! TodayAppointmentTableViewCell
            //        }
        else if indexPath.row == 9 {
            let todayTaskCell = tableView.dequeueReusableCell(withIdentifier: "todayTaskTableViewCell", for: indexPath) as! TodayTaskTableViewCell
            
            if usertodos != [] {
                todayTaskCell.mlblNumberTodo.text = "Có \(usertodos.count) việc cần làm trong danh sách"
                todayTaskCell.mViewCount.isHidden = false
                todayTaskCell.mlblCount.text = "\(usertodos.count)"
                DispatchQueue.global(qos: .background).async {
                    var name = ""
                    for i in self.todos {
                        for j in self.usertodos {
                            if j["todo_id"].intValue == i["id"].intValue {
                                name += "\(i["title"].stringValue), "
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        todayTaskCell.mlblNameTodo.text = name
                    }
                }
            } else {
                todayTaskCell.mlblNumberTodo.text = "không có công việc nào được chọn"
                todayTaskCell.mViewCount.isHidden = true
                todayTaskCell.mlblNameTodo.text = "Chọn những việc cần làm ở đây"
            }
            cell = todayTaskCell
        }
            
        else if indexPath.row == 10 {
            let todayCell = tableView.dequeueReusableCell(withIdentifier: "todayBabyNameTableViewCell", for: indexPath) as! TodayBabyNameTableViewCell
            cell = todayCell
            if userbabynames.isEmpty == false {
                todayCell.mFirstView.isHidden = true
                todayCell.mSecondView.isHidden = false
                
                todayCell.mlblBoy.text = self.nameboy
                todayCell.mlblGirl.text = self.namegirl
            } else {
                todayCell.mFirstView.isHidden = false
                todayCell.mSecondView.isHidden = true
                
                todayCell.mlblBoy.text = ""
                todayCell.mlblGirl.text = ""
            }
        }
//        else if indexPath.row == 9{
//            cell = tableView.dequeueReusableCell(withIdentifier: "todayPremiumTableViewCell", for: indexPath) as! TodayPremiumTableViewCell
//        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "todayFacebookTableViewCell", for: indexPath) as! TodayFacebookTableViewCell
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}


extension TodayViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 2:
            if self.babyAlredyBorn == true {
                let mDueDate = UIStoryboard(name: "menu", bundle: nil).instantiateViewController(withIdentifier: "DueDateViewController") as! DueDateViewController
                self.navigationController?.pushViewController(mDueDate, animated: true)
            } else {
                let mDailyInfo = UIStoryboard(name: "Baby", bundle: nil).instantiateViewController(withIdentifier: "BabyDailyViewController") as! BabyDailyViewController
                ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mDailyInfo, at: 2, backTo: 0)
            }
        case 3:
            let mDueDate = UIStoryboard(name: "menu", bundle: nil).instantiateViewController(withIdentifier: "DueDateViewController") as! DueDateViewController
            self.navigationController?.pushViewController(mDueDate, animated: true)
        case 4:
            if yourPackage.count != 0 {
                let mPackage = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "DetailPackageViewController") as! DetailPackageViewController
                mPackage.id = yourPackage[0]
                mPackage.titleDetail = yourPackage[1]
                mPackage.products = self.products
                var index = 0
                for item in products {
                    if yourPackage[0] == item["Id"].stringValue {
                        mPackage.mIgnoredIndex = index
                        self.navigationController?.pushViewController(mPackage, animated: true)
                    }
                    
                    index += 1
                }
            }
        case 5:
            let mAppHis = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "AppointmentsHistoryViewController") as! AppointmentsHistoryViewController
            self.navigationController?.pushViewController(mAppHis, animated: true)
        case 6:
            if self.babyAlredyBorn == true {
                let mDueDate = UIStoryboard(name: "menu", bundle: nil).instantiateViewController(withIdentifier: "DueDateViewController") as! DueDateViewController
                self.navigationController?.pushViewController(mDueDate, animated: true)
            } else {
                let mBabyWeek = UIStoryboard(name: "Baby", bundle: nil).instantiateViewController(withIdentifier: "BabyWeekViewController") as! BabyWeekViewController
                //            mBabyWeek.weekSlide = self.weeks
                ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mBabyWeek, at: 2, backTo: 0)
            }
            //        case 7:
            //            let mMyAppointment = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "MyAppointmentsViewController") as! MyAppointmentsViewController
            //            self.navigationController?.pushViewController(mMyAppointment, animated: true)
            
        case 9:
            let mTodoList = UIStoryboard(name: "Me", bundle: nil).instantiateViewController(withIdentifier: "TodoListViewController") as! TodoListViewController
            ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mTodoList, at: 3, backTo: 0)
            
        case 10:
            let mBabyName = UIStoryboard(name: "More", bundle: nil).instantiateViewController(withIdentifier: "BabyNameViewController") as! BabyNameViewController
            ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: mBabyName, at: 4, backTo: 0)
        case 11:
            let mPremiumUpgrade = UIStoryboard(name: "PremiumUpgrade", bundle: nil).instantiateViewController(withIdentifier: "PremiumUpgradeViewController") as! PremiumUpgradeViewController
            self.navigationController?.pushViewController(mPremiumUpgrade, animated: true)
        case 12:
            let url = URL(string: "https://www.facebook.com/momtour.hanhtrinhlamme/")!
            if(UIApplication.shared.canOpenURL(url)) {
                UIApplication.shared.openURL(url)
            }
        default:
            break
        }
    }
}

class UIAnimationLabel: UILabel {
    
    var isAnimateEnable:Bool = true
    
    func animate(newText: String, characterDelay: TimeInterval) {
        
        DispatchQueue.main.async {
            
            self.text = ""
            
            for (index, character) in newText.enumerated() {
                if(self.isAnimateEnable) {
                    DispatchQueue.main.asyncAfter(deadline: .now() + characterDelay * Double(index)) {
                        if(self.isAnimateEnable) {
                            self.text = "\(self.text ?? "" )\(character)"
                        }
                    }
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + characterDelay * Double(newText.count) + 0.5) {
                checkReload = true
            }
        }
    }
}

extension String {
    var characterArray: [Character]{
        var characterArray = [Character]()
        for character in self.characters {
            characterArray.append(character)
        }
        return characterArray
    }
}

extension UITextView {
    
    func typeOn(_ string: String, _ timeDelay: Float) {
        let characterArray = string.characterArray
        var characterIndex = 0
        if #available(iOS 10.0, *) {
            time = Timer.scheduledTimer(withTimeInterval: TimeInterval(timeDelay), repeats: true) { (timer) in
                self.text.append(characterArray[characterIndex])
                characterIndex += 1
                if characterIndex == characterArray.count {
                    timer.invalidate()
                }
            }
        }
    }
}
