//
//  TodayWeekBabySizeTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class TodayWeekBabySizeTableViewCell: UITableViewCell {

    @IBOutlet weak var uvWeekBabyBox: UIView!
    @IBOutlet weak var btnBabyImages: UIButton!
    @IBOutlet weak var btnBabySize: UIButton!
    @IBOutlet weak var mlblWeek: UILabel!
    @IBOutlet weak var mSizeGuideLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uvWeekBabyBox.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
