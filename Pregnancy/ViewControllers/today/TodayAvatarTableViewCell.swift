//
//  TodayAvatarTableViewCell.swift
//  Pregnancy
//
//  Created by mai kim tai  on 12/26/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import SDWebImage


class TodayAvatarTableViewCell: UITableViewCell  {

    @IBOutlet weak var uvAvatarBorder: UIView!
    @IBOutlet weak var uvAvatarImage: UIImageView!
    @IBOutlet weak var uvAvatarBackground: UIView!
    @IBOutlet weak var mbtnAvatar: UIButton!
//    @IBOutlet weak var mlblWelcome: UIAnimationLabel!
    @IBOutlet weak var mTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.uvAvatarBorder.layer.cornerRadius = uvAvatarBorder.layer.frame.size.width / 2
        self.uvAvatarImage.layer.cornerRadius = uvAvatarImage.layer.frame.size.width / 2
        self.uvAvatarBackground.layer.cornerRadius = uvAvatarBackground.layer.frame.size.width / 2
        self.uvAvatarImage.layer.masksToBounds = true
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setavatarimage () {
        
        DatabaseManager.sharedManager.getUserInfo(callBack: { (user, isTemp, isSuccess) in
            if let user = user {
                var avatar = NetworkManager.rootDomain
                if user.avatar.hasPrefix("http") {
                    avatar = user.avatar
                } else {
                    avatar.append(user.avatar)
                }
                self.uvAvatarImage.sd_setImage(with: URL(string:avatar), completed: {(image, error, cacheType, url) in
                    if let image = image {
                        self.uvAvatarImage.image = image
                    } else {
                        self.uvAvatarImage.image = UIImage(named: "user_placeholder")
                    }
                })
            } else {
                self.uvAvatarImage.image = UIImage(named: "user_placeholder")
            }
        })
        
    }
    
    private func fetchUserData(_ callBack: @escaping (_ image:UIImage?)-> ()) {
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            if user.social_type_id == "1" {
                let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
                graphRequest?.start(completionHandler: { (connection, result, error) in
                    if error != nil {
                        print("Error",error!.localizedDescription)
                        callBack(UIImage(named:"user_placeholder"))
                    }
                    else{
                        print(result!)
                        let field = result! as? [String:Any]
                        if let imageURL = ((field!["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                            let url = URL(string: imageURL)
                            self.uvAvatarImage.sd_setImage(with: url, placeholderImage: UIImage(named: "user_placeholder"), options: .highPriority, completed: { (image, erro, cacheType, url) in
                                callBack(image)
                                if let image = image {
                                    AttachmentHandler.shared.saveImage(imageName: "avartarimage\(user.id)", image: image)
                                }
                                
                            })
                        }
                    }
                })
            }
        }
        
        
    }
    
}
