//
//  SkyFloatingLabelTextFieldExtension.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/6/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

extension SkyFloatingLabelTextField{
    
    // Thêm lựa chọn trong Storyboard: title mặc định hay title viết hoa
    @IBInspectable var changeSelectedTitleFormat: Bool{
        get{
            return self.changeSelectedTitleFormat
        }
        set (isChange) {
            if isChange{
                fix(textField: self)
            }
        }
    }
    
    func fix(textField: SkyFloatingLabelTextField) {
        textField.titleFormatter = { $0 }
    }
}
