//
//  StringExtension.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 3/8/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

extension String {
    // Cắt string thành chuỗi nhỏ hơn
    func substring(fromIndex: Int, toIndex: Int) -> String? {
        if fromIndex < toIndex && toIndex < self.count /*use string.characters.count for swift3*/{
            let startIndex = self.index(self.startIndex, offsetBy: fromIndex)
            let endIndex = self.index(self.startIndex, offsetBy: toIndex)
            return String(self[startIndex..<endIndex])
        }else{
            return nil
        }
    }
}
