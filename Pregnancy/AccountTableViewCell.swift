//
//  AccountTableViewCell.swift
//  Reusable Template
//
//  Created by mai kim tai  on 12/4/18.
//  Copyright © 2018 mai kim tai . All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {
    @IBOutlet weak var mTitleLabel: UILabel!
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var mViewCell: UIView!
    
    @IBOutlet weak var mContentView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mViewCell.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
