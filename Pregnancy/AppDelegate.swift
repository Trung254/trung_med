//
//  AppDelegate.swift
//  Pregnancy
//
//  Created by Lan Tran Duc on 12/4/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn
import UserNotifications
import RealmSwift

@UIApplicationMain
    class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var weeks: Float?
    
    var checkKickCounterFirstActive = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.migrationRealm()
        let realm = try! Realm()
        print(realm.configuration.fileURL)
        NotificationManager().scheduleNotification()
     
        // Override point for customization after application launch.
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = "40928340303-72eo2oea61uafeif7dio974efn96pmnp.apps.googleusercontent.com"
        GIDSignIn.sharedInstance()?.shouldFetchBasicProfile = true
//        AuthManager.shared.renewToken()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        self.checkAlredyBorn()

        return true
    }
    
    private func checkAlredyBorn() {
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTemp, isSuccess, rawData) in
           
        }
    }
    
    @objc func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if(url.absoluteString.hasPrefix("fb")){
            return (FBSDKApplicationDelegate.sharedInstance()?.application(_:application, open:url, sourceApplication:sourceApplication, annotation:annotation))!
        }
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    private func migrationRealm() {
        // Inside your application(application:didFinishLaunchingWithOptions:)
        
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 1,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        
        // Now that we've told Realm how to handle the schema change, opening the file
        // will automatically perform the migration
        let realm = try! Realm()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//        AuthManager.shared.renewToken()
        self.checkDataKickCounter()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        AuthManager.shared.renewToken()
        
        // Chỉ cho phép chạy 1 lần khi sử dụng app
        if self.checkKickCounterFirstActive == false {
            self.checkDataKickCounter()
            self.checkKickCounterFirstActive = true
        }
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func checkDataKickCounter() {
        let realm = try! Realm()
        
        if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
            let kickListCheck = realm.objects(KickCounter.self).filter("user_id == %@", user.id)
            
            if kickListCheck.isEmpty == false {
                // Có dữ liệu -> bắn ra thông báo
                // Check xem có phải thằng KickCounter không?
                let currVC = ViewManager.getCurrentViewController()
                
                if (currVC?.isKind(of: KickCounterViewController.self))! {
                    // Là thằng Kick counter -> không bắn thông báo, cũng ko chuyển vào view của nó -> không load lại thằng nào cả nên cần set lại animation các kiểu :D)
                } else {
                    // Action 2 khi người dùng muốn hủy lần đạp chân này
                    let alertController2 = UIAlertController(title: "Cảnh báo", message: "Bạn có muốn kết thúc lần đạp chân này không?", preferredStyle: .alert)
                    let cancelAction2 = UIAlertAction(title: "Không", style: .default) { (action) in
                        // Không thì không làm gì
                    }
                    let okAction2 = UIAlertAction(title: "Có", style: .default) { (action) in
                        // kết thúc thì xóa hết trong realm
                        try! realm.write {
                            realm.delete(kickListCheck)
                        }
                    }
                    // Không phải thằng kickcounter -> nhảy vào -> Load nên ko cần set lại cho nó
                    let alertController = UIAlertController(title: "Đạp chân em bé đang được thực hiện", message: "Bạn có muốn tiếp tục không?", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Dừng", style: .default) {
                        UIAlertAction in
                        // Không thì bắn alert tiếp
                        alertController2.addAction(cancelAction2)
                        alertController2.addAction(okAction2)
                        self.window?.rootViewController?.present(alertController2, animated: true, completion: nil)
                    }
                    let cancelAction = UIAlertAction(title: "Tiếp tục", style: .default) {
                        UIAlertAction in
                        let kickCounterVC = UIStoryboard(name: "Baby", bundle: nil).instantiateViewController(withIdentifier: "KickCounterViewController") as! KickCounterViewController
                        kickCounterVC.pushToAppDelegate = true
                        ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: kickCounterVC, at: 2, backTo: 0)
                    }
                    alertController.addAction(okAction)
                    alertController.addAction(cancelAction)
                    
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    
                }
            }
        }
    }


}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Baby", bundle: nil)
//        let presentViewController = storyBoard.instantiateViewController(withIdentifier: "BabyWeekViewController") as! BabyWeekViewController
////                let week = NotificationManager().currentweek
////
////                presentViewController.sldBabyWeekly.value = 54
//
//        ViewManager.getCurrentViewController()?.navigationController?.pushViewController(presentViewController, animated: true)

        completionHandler([.alert, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Baby", bundle: nil)
        let presentViewController = storyBoard.instantiateViewController(withIdentifier: "BabyWeekViewController") as! BabyWeekViewController
        DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
            if let pregnancy = pregnancy {
                var components = Calendar.current.dateComponents([.weekOfYear, .weekday], from: pregnancy.start_date, to: Date())
                
//                if let week = components.weekOfYear, week > 0 {
//                    presentViewController.sldBabyWeekly.value = week
//                } else {
//                    presentViewController.sldBabyWeekly.value = 0
//                }
            }
        }
        
//        if let week = self.weeks {
//            presentViewController.sldBabyWeekly.value = Float(week)
//        }
        
        
        
       ViewManager.shareInstance.rootTabbarController?.showViewController(viewController: presentViewController, at: 2, backTo: nil)
        
        completionHandler()
    }
    
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    
}
