//
//  ImgShareWeekly.swift
//  Pregnancy
//
//  Created by Nguyễn Thanh on 2/19/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class ImgShareWeekly: UIView {
    
    @IBOutlet weak var viewSecond: UIStackView!
    @IBOutlet weak var viewImageShare: UIView!
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var txtNote: UILabel!
    @IBOutlet weak var lblWeeklyShare: UILabel!
    @IBOutlet weak var lblAppInstall: UILabel!
    
    @IBOutlet weak var heightImageView: NSLayoutConstraint!
    @IBOutlet weak var heightNote: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewSecond: NSLayoutConstraint!
    @IBOutlet weak var heightParrentView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ImgShareWeekly", owner: self, options: nil)
//        addSubview(weekContentView)
//        weekContentView.frame = self.bounds
//        mWebViewHeight.constant = 1000
//
//        weekContentView.snp.makeConstraints { (maker) in
//            maker.top.equalToSuperview()
//            maker.bottom.equalToSuperview()
//            maker.left.equalToSuperview()
//            maker.right.equalToSuperview()
//        }
//        self.setupWebviewIfNeed()
    }
}
