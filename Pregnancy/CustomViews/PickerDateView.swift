//
//  PickerDateView.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 1/22/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

protocol PickerDateViewDelegate: class {
    func didSelectedDateString(dateString: String)
    func doneTapped(_ sender: PickerDateView)
}

class PickerDateView: UIView, UIGestureRecognizerDelegate {
    
    var currentChoise : Date?
    @IBOutlet weak var datePicker: UIDatePicker!
    weak var delegate: PickerDateViewDelegate?
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    class func initView() -> PickerDateView{
        let pickerDateView = UINib(nibName: "PickerDateView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PickerDateView
        pickerDateView.frame = UIScreen.main.bounds
        
        return pickerDateView
    }
    
    @IBAction override func removeFromSuperview() {
        UIView.animate(withDuration: 0.5, animations: {
            self.layer.opacity  = 0
        }) { (processed) in
            if processed {
                super.removeFromSuperview()
            }
        }
    }
    
    @IBAction func btnDoneTouchUpInside(_ sender: Any) {
        self.removeFromSuperview()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: datePicker.date)
        currentChoise = datePicker.date
        let realm = try! Realm()
        try! realm.write {
            var user = DatabaseManager.sharedManager.getLocalPregnancys()
            user?.due_date = datePicker.date
            
//            realm.add(user!)
        }
        
        if let delegate = delegate {
            delegate.didSelectedDateString(dateString: strDate)
        }
        
        delegate?.doneTapped(self)
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        var user = DatabaseManager.sharedManager.getLocalPregnancys()
        let dateStr1 = user?["due_date"]
        let babyalreadyborn = user?.baby_already_born
        if (babyalreadyborn == 0 ) {
            if (user?.check_screen_calculator == 0){
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                //        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                let strDate1 = dateFormatter.string(from: dateStr1 as! Date)
                let date = dateFormatter.date(from:strDate1)!
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day], from: date)
                let finalDate = calendar.date(from:components)
                
                datePicker.date = finalDate!
                if let delegate = self.delegate {
                    delegate.didSelectedDateString(dateString: strDate1)
                }
            }
        }
        let should = (touch.view == gestureRecognizer.view)
        return should
    }
    
}
