//
//  VNPay.swift
//  LanTranDuc
//
//  Created by Lan Tran Duc on 2/28/19.
//  Copyright © 2019 lantranduc@gmail.com. All rights reserved.
//

import Foundation
import SwiftHash
import SafariServices
import WebKit
import UIKit
import SnapKit

protocol VNPayDelegate : AnyObject {
    func vnpay(_ vnpay: VNPay, present vnPayViewController: VNPayViewController)
    func vnpay(_ vnpay: VNPay, dismiss viewController:VNPayViewController, withTransactionResult transactionIsSuccess: Bool)
}

enum VNPayCommand: String {
    case pay
}

enum VNPayOrderType : String {
    case billPayment = "billpayment"
}

class VNPayOrderInfo {
    
    var version: String! = "2.0.0"
    var tmnCode: String!
    var amount: String!
    var command: VNPayCommand = .pay
    var createDate: Date! //yyyyMMddHHmmss
    var currCode: String {
        get {
            return "VND"
        }
    }
    var ipAddress: String!
    var locale : String {
        get {
            return "vn"
        }
    }
    var orderInfo: String!
    var orderType: VNPayOrderType = .billPayment
    var returnURL: String! = NetworkManager.vnpayCallBack
    var ref: String!
    
    convenience init( amount: String, orderInfo: String, ref: String ) {
        self.init()
        self.amount = amount
        self.orderInfo = orderInfo
        self.ref = ref
        self.createDate = Date()
        self.ipAddress = ""
        do {
            var ip = try String(contentsOf: URL(string: "https://icanhazip.com")!, encoding: String.Encoding.utf8)

            ip = ip.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.ipAddress = ip
        } catch {

        }
        
    }
    
    func dateToDateString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        return dateFormatter.string(from: self.createDate)
    }
    
    func toDictionary() -> Dictionary<String, String> {
        
        let dictionary: Dictionary<String, String> = [
            "vnp_Version": self.version,
            "vnp_TmnCode": self.tmnCode,
            "vnp_Amount": self.amount,
            "vnp_Command": self.command.rawValue,
            "vnp_CreateDate": self.dateToDateString(),
            "vnp_CurrCode": self.currCode,
            "vnp_IpAddr": self.ipAddress,
            "vnp_Locale": self.locale,
            "vnp_OrderInfo": self.orderInfo,
            "vnp_OrderType": self.orderType.rawValue,
            "vnp_ReturnUrl": self.returnURL,
            "vnp_TxnRef": self.ref
        ]
        
        return dictionary
    }
    
    func toHashData() -> String {
        var hashString = ""
        
        
        let data = self.toDictionary()
        var keys: Array<String> = Array(data.keys)
        keys.sort { (left, right) -> Bool in
            return left.compare(right) != .orderedDescending
        }
        
        for key in keys {
            if key.count > 0 {
                if let value = data[key], value.count > 0 {
                    if hashString.count > 0 {
                        hashString = hashString + "&"
                    }
                    hashString = hashString + "\(key)=\(value)"
                }
            }
        }
        
        return hashString
    }
    
}

class VNPay {
    
    let vnp_TmnCode = "MLATEC03"
    let vnp_HashSecret = "XEEYZCPBZZDLSWIPWNJAWKKLNXLSHWAY"
    
    weak var delegate: VNPayDelegate?
    var vnpayOrderInfo: VNPayOrderInfo!
    var tnmCode: String!
    var hashSecret: String!
    private var vnpayVC: VNPayViewController?
    var vnpayViewController: VNPayViewController? {
        get {
            return self.vnpayVC
        }
    }
    
    private var safari: SFSafariViewController?
    
    var safariViewController: SFSafariViewController? {
        get {
            return self.safari
        }
    }
    
    func vnpay( makeOrderForData data:VNPayOrderInfo) -> Bool {
        data.tmnCode = vnp_TmnCode
        self.vnpayOrderInfo = data
        
        let hashString = self.vnpayOrderInfo.toHashData()
        let vnpayEndPoint = NetworkManager.vnpayURL + "?" + hashString.urlEncoded() + "&vnp_SecureHashType=MD5&vnp_SecureHash=\(MD5(vnp_HashSecret + hashString))"
        if let url = URL(string: vnpayEndPoint) {
            if UIApplication.shared.canOpenURL(url) {
                if let delegate = self.delegate {
                    self.vnpayVC = VNPayViewController()
                    self.vnpayVC!.url = url
                    self.vnpayVC!.vnpay = self
                    delegate.vnpay(self, present: self.vnpayVC!)
                    return true
                }
            }
        }
        return false
    }
    
    func dismiss(vnpay viewController: VNPayViewController, withTransactionResult transactionIsSuccess:Bool) -> Void {
        if let delegate = self.delegate {
            delegate.vnpay(self, dismiss: viewController, withTransactionResult: transactionIsSuccess)
        }
    }
}

class VNPayViewController: BaseViewController {
    
    var mWebview: WKWebView!
    var url: URL!
    var vnpay: VNPay!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let config = WKWebViewConfiguration()
        mWebview = WKWebView(frame: .zero, configuration: config)
        self.view.addSubview(mWebview)
        mWebview.translatesAutoresizingMaskIntoConstraints = false
        mWebview.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview()
            maker.leading.equalToSuperview()
            maker.trailing.equalToSuperview()
        }
        mWebview.uiDelegate = self
        mWebview.navigationDelegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadUrl(url)
    }
    
    
    func loadUrl(_ url: URL) -> Void {
        mWebview.load(URLRequest(url: url))
    }
    
    
}

extension VNPayViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        if let urlString = webView.url?.absoluteString {
            print("Bat dau load URL : " + urlString)
            if urlString.contains("/success.php") {
                webView.stopLoading()
                self.vnpay.dismiss(vnpay: self, withTransactionResult: true)
            } else if urlString.contains("/fail.php") {
                webView.stopLoading()
                self.vnpay.dismiss(vnpay: self, withTransactionResult: false)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        if let urlString = webView.url?.absoluteString {
            print("Bat dau load Redirect URL : " + urlString)
            if urlString.contains("/success.php") {
                webView.stopLoading()
                self.vnpay.dismiss(vnpay: self, withTransactionResult: true)
            } else if urlString.contains("/fail.php") {
                webView.stopLoading()
                self.vnpay.dismiss(vnpay: self, withTransactionResult: false)
            }
        }
    }
    
}

extension VNPayViewController: WKUIDelegate {
    
    
}
