import UIKit
import UserNotifications
import SwiftyJSON

class NotificationManager: UIViewController, UNUserNotificationCenterDelegate {

    var currentweek: Float = 0
    var weeks: Int?
    var dataSettings: [JSON] = []
    var checkrequest = 0
    
    func logoutUser() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.removeAllDeliveredNotifications()
//            center.getPendingNotificationRequests(completionHandler: { (notificationRequests) in
//                for item in notificationRequests {
//                    center.removePendingNotificationRequests(withIdentifiers: [item.identifier])
//                }
//            })
        }
    }
    
    func scheduleNotification() {
        if #available(iOS 10.0, *) {
            NetworkManager.shareInstance.apiGetAppSetting() {
                (data, message, isSuccess) in
                if isSuccess {
                    if (message != "Data not found.") {
                    let data = data as? JSON
                    self.dataSettings = data!.arrayValue

                    DatabaseManager.sharedManager.getUserInfo { (user, isTemp, isSuccess) in
                        let center = UNUserNotificationCenter.current()
                        let content = UNMutableNotificationContent()
                        
                        if !isTemp , isSuccess, self.dataSettings[0]["reminders"].boolValue == true {
                            DatabaseManager.sharedManager.getPregnancys { (pregnancy, isTempData, isSuccess, rawData) in
                                if let pregnancy = pregnancy {
                                    var components = Calendar.current.dateComponents([ .day], from: Date(), to:pregnancy.due_date.addingTimeInterval(86400))

                                    if ( !isTemp && isSuccess) {
                                        if var day = components.day {
                                            if (day < 0 ) {
                                                day = 0
                                            } else {
                                                day = 280 - day
                                            }

                                            self.weeks = day / 7
                                            if self.weeks == 0 {
                                                self.weeks = 1
                                            } else if (pregnancy.show_week == 1){
                                                self.weeks = day / 7 + 1
                                            }
                                        } else {
                                            self.weeks = 1
                                        }

                                        if let user = user {
                                            content.title = "Xin chào " + user.first_name
                                            if var week = self.weeks {
                                                
                                                if week == 0 {
                                                    week = 1
                                                } else if week > 42 {
                                                    week = 42
                                                }
                                                
                                                
                                                content.body = "Bạn đang ở tuần thai thứ \(week). Momtour có một vài lưu ý để giúp bạn và bé luôn khỏe mạnh trong tuần thai này!"
                                                content.categoryIdentifier = "alarm"
                                                content.userInfo = ["customData": "fizzbuzz"]
                                                content.sound = UNNotificationSound.default

                                                let notifyId = "user_\(user.id)_week_\(week)"

                                                //                                        let date = Calendar.current.date(byAdding: .day, value: 7, to: Date())

                                                let dateComponents = Calendar.current.dateComponents([.year,.month,.day,.hour, .minute, .second], from: Date().addingTimeInterval(86400))

                                                if var day = components.day {
                                                    day = 280 - day

                                                    if day % 7 == 0 {
                                                        center.getPendingNotificationRequests(completionHandler: { (notificationRequests) in

                                                            center.removeAllDeliveredNotifications()

                                                            let trigger = UNCalendarNotificationTrigger.init(dateMatching: dateComponents, repeats: false)

                                                            let request = UNNotificationRequest(identifier: notifyId, content: content, trigger: trigger)

                                                            center.add(request)

                                                            let result = notificationRequests.filter({ (notification) -> Bool in
                                                                return (notification.identifier == notifyId)
                                                            })

                                                            if result.count > 0 {
                                                                // dang ton tai notification cua user X tai week Y
                                                                // delete toan bo notification week < Y

                                                                // kiem tra xem ngay hom nay co phai la ngay dau tien hay khong
                                                                // kiem tra xem trigger data cua notification dang ton tai co dung so voi duedate moi nhat hay khong
                                                                //neu dung thi de toan bo notification tu tuan Y den het,
                                                                //sai thi xoa toan bo, tinh toan lai trigger date roi add lai notification




                                                            } else {
                                                                //ko ton tai

                                                                //                                                        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
                                                                //
                                                                //                                                        let request = UNNotificationRequest(identifier: notifyId, content: content, trigger: trigger)
                                                                //
                                                                //                                                        center.add(request)
                                                            }
                                                        })
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        }
                    }
//                    if (message == "Data not found.") {
//                        DatabaseManager.sharedManager.logout {
//               
//                        }
//                    }
                }
            }
        }
    }
}

