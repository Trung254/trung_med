//
//  AuthManager.swift
//  Pregnancy
//
//  Created by Ngoc Dung on 09/01/2019.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn

final class AuthManager {
    static let shared = AuthManager()
    private let authTokenKey = "authTokenKey"
    private let authPhoneNumberKey = "authPhoneNumberKey"
    private let authPasswordKey = "authPasswordKey"
    private let authGuestAccountKey = "authGuestAccountKey"
    private let authGuestPasswordKey = "authGuestAccountPasswordKey"
    private let userDefault : UserDefaults
    private var timer:Timer?
    
    private init() {
        self.userDefault = UserDefaults.standard
    }
    
//    var currentUser : accountUser? = accountUser(isSignup: true,phone: "",socialTypeId: 0,firstName: "",lastName: "",youAreThe: "",location: "", startDate: "",dueDate: "", dataGender: 1, weekPregnancy: "")
    
    var authToken : String? {
        set {
            if newValue == nil {
                userDefault.removeObject(forKey: authTokenKey)
            } else {
                userDefault.setValue(newValue, forKey: authTokenKey)
            }
            userDefault.synchronize()
        }
        get {
            return userDefault.string(forKey: authTokenKey)
//            return DatabaseManager.sharedManager.getLocalToken()
        }
    }
    
    func resetToken() {
        authToken = nil
    }
    
    var authPhoneNumber : String? {
        set {
            if newValue == nil {
                userDefault.removeObject(forKey: authPhoneNumberKey)
            } else {
                userDefault.setValue(newValue, forKey: authPhoneNumberKey)
            }
        }
        get {
            return userDefault.string(forKey: authPhoneNumberKey)
        }
    }
//
    var authPassword : String? {
        set {
            if newValue == nil {
                userDefault.removeObject(forKey: authPasswordKey)
            } else {
                userDefault.setValue(newValue, forKey: authPasswordKey)
            }
        }
        get {
            return userDefault.string(forKey: authPasswordKey)
        }
    }
    
    var guestAccount: String? {
        set {
            if newValue == nil {
                userDefault.removeObject(forKey: authGuestAccountKey)
            } else {
                userDefault.setValue(newValue, forKey: authGuestAccountKey)
            }
            userDefault.synchronize()
        }
        get {
            return userDefault.string(forKey: authGuestAccountKey)
        }
    }
    
    var guestPassword: String? {
        set {
            if newValue == nil {
                userDefault.removeObject(forKey: authGuestPasswordKey)
            } else {
                userDefault.setValue(newValue, forKey: authGuestPasswordKey)
            }
            userDefault.synchronize()
        }
        get {
            return userDefault.string(forKey: authGuestPasswordKey)
        }
    }
    
    func stopTimer() -> Void {
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    func startTimer() -> Void {
        self.stopTimer()
        
        self.timer = Timer.scheduledTimer(timeInterval: 3*60*60, target: self, selector: #selector(self.renewToken), userInfo: nil, repeats: false)
    }
    
    
    @objc func renewToken() -> Void {
        
        if (DatabaseManager.sharedManager.isLogin()) {
            var phone: String?
            var password: String?
            
            var socialToken: String?
            var socialId: SocialProvider?
            
            if (DatabaseManager.sharedManager.isGuest()) {
                if let  guestPhone = self.guestAccount {
                    phone = guestPhone
                }
                if let guestPass = self.guestPassword {
                    password = guestPass
                }
            } else {
                if let user = DatabaseManager.sharedManager.getLocalUserInfo() {
                    if (user.social_type_id == "1" ||
                        user.social_type_id == "2") {
                        socialId = user.social_type_id == "1" ? SocialProvider.facebook : SocialProvider.google
                        if (socialId == .facebook) {
                            socialToken = FBSDKAccessToken.current()?.tokenString
                        } else {
                            if let user = GIDSignIn.sharedInstance()?.currentUser {
                                if let auth = user.authentication {
                                    if let accessToken = auth.accessToken {
                                        socialToken = accessToken
                                    }
                                }
                            }
                        }
                    } else {
                        if let  guestPhone = self.guestAccount {
                            phone = guestPhone
                        }
                        if let guestPass = self.guestPassword {
                            password = guestPass
                        }
                    }
                }
                
            }
            
            if let phone = phone, let password = password {
                NetworkManager.shareInstance.apiToken(username: phone, password: password, provider: nil, accessToken: nil) { (isSuccess) in
                    if (!isSuccess) {
                        DatabaseManager.sharedManager.logout {
                            
                        }
                        return
                    }
                    self.startTimer()
                }
                
            } else if let socialToken = socialToken, let socialId = socialId {
                NetworkManager.shareInstance.apiToken(username: phone, password: password, provider: socialId, accessToken: socialToken) { (isSuccess) in
                    if (!isSuccess) {
                        return
                    }
                    self.startTimer()
                }
            } else {
            }
            
        }
    }
}
